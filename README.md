This is an implementation of (a generalization of) the minimal
function graphs described in the two following research articles.

> Data Flow Analysis of Applicative Programs Using Minimal Function Graphs
>
> Neil D. Jones and Alan Mycroft
>
> Proceedings of the 13th ACM SIGACT-SIGPLAN symposium on Principles of programming languages - POPL '86
>
> ACM Press, 1986

> Jones, N. D. & Rosendahl, M.
>
> Higher-Order Minimal Function Graphs
>
> Journal of Functional and Logic Programming, 1997

Author: Benoît Montagu <benoit.montagu@inria.fr>

Copyright Inria © 2022
