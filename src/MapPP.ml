module Make (X : sig
  type t

  val compare : t -> t -> int
  val pp : Format.formatter -> t -> unit
end) : sig
  include Map.S with type key = X.t and type 'a t = 'a Map.Make(X).t

  val pp : (Format.formatter -> 'a -> unit) -> Format.formatter -> 'a t -> unit
end = struct
  include Map.Make (X)

  let pp pp_value fmt m =
    let open Format in
    fprintf fmt "@[<v>%a@]"
      (pp_print_seq ~pp_sep:pp_print_space (fun fmt (vs, v) ->
           fprintf fmt "@[%a ->@ %a@]" X.pp vs pp_value v))
      (to_seq m)
end
