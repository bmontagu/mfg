(**
  Author: Benoît Montagu <benoit.montagu@inria.fr>
  Copyright Inria © 2022
*)

open Sigs
open FoCommon

module type DOMAIN = sig
  type v
  type c
  type d
  type f
  type x
  type a
  type phi

  val fetch : int -> v list -> d
  val basic : a -> d list -> d
  val apply : f -> phi -> d list -> d
  val cond : d -> d -> d -> d
  val init : c -> phi -> phi
  val iterate : phi -> (f * (v list -> d)) list -> phi
  val fix : delay_widening:int -> (phi -> phi) -> phi
end

module type INTERP = sig
  type x
  type v
  type c
  type d
  type phi
  type expr
  type program

  val expr : (x -> v list -> d) -> expr -> phi -> v list -> d
  val program : delay_widening:int -> program -> c -> phi
end

module type MINIMAL_MAP = sig
  type 'a t
  type key

  val empty : 'a t
  val singleton : key -> 'a -> 'a t
  val find_opt : key -> 'a t -> 'a option
  val map : ('a -> 'b) -> 'a t -> 'b t
  val union : (key -> 'a -> 'a -> 'a option) -> 'a t -> 'a t -> 'a t

  val merge :
    (key -> 'a option -> 'b option -> 'c option) -> 'a t -> 'b t -> 'c t

  val for_all : (key -> 'a -> bool) -> 'a t -> bool
  val fold : (key -> 'a -> 'b -> 'b) -> 'a t -> 'b -> 'b
  val pp : (Format.formatter -> 'a -> unit) -> Format.formatter -> 'a t -> unit
end

module MFG
    (X : ORDERED_TYPE)
    (A : TYPE)
    (F : ORDERED_TYPE)
    (FMap : MINIMAL_MAP with type key = F.t) (V : sig
      include ORDERED_TYPE
      include PP_TYPE with type t := t

      val as_bool : t -> Common.abs_bool
      val bot : t
      val is_bot : t -> bool
      val join : t -> t -> t
    end)
    (Graph : GRAPH with type x := V.t list and type y := V.t) (I : sig
      type phi = Graph.t FMap.t

      val basic : A.t -> V.t list -> V.t
      val apply : F.t -> phi -> V.t list -> V.t
    end) : sig
  module D : sig
    type v = V.t
    type f = F.t

    module Phi : sig
      type t = I.phi

      val bot : t
      val singleton : F.t -> Graph.t -> t
      val join : t -> t -> t

      include PP_TYPE with type t := t
    end

    type c = Phi.t

    include
      DOMAIN
        with type f := f
         and type x := X.t
         and type a := A.t
         and type v := v
         and type c := c
         and type d = v * c
         and type phi = Phi.t
  end

  include
    INTERP
      with type expr = (X.t, A.t, F.t) expr
       and type program = (X.t, A.t, F.t) program
       and type x := X.t
       and type v := D.v
       and type c := D.c
       and type d := D.d
       and type phi := D.phi
end
