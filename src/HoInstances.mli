(**
  Author: Benoît Montagu <benoit.montagu@inria.fr>
  Copyright Inria © 2022
*)

open Sigs
open Common
open HoCommon

module AbstractGraph
    (_ : functor
      (X : sig
         type t

         val compare : t -> t -> int
         val pp : Format.formatter -> t -> unit
         val is_bot : t -> bool
         val leq : t -> t -> bool
         val join : t -> t -> t
         val widen : int -> t -> t -> t
         val meet : t -> t -> t
       end)
      (Y : sig
         type t

         val bot : t
         val leq : t -> t -> bool
         val join : t -> t -> t
         val widen : int -> t -> t -> t
         val pp : Format.formatter -> t -> unit
       end)
      ->
  HoCommon.GRAPH with type x := X.t and type y := Y.t) (X : sig
  include ORDERED_TYPE
  include PP_TYPE with type t := t

  val is_bot : t -> bool
  val leq : t -> t -> bool
  val join : t -> t -> t
  val widen : int -> t -> t -> t
  val meet : t -> t -> t
end) (Y : sig
  include PP_TYPE

  val bot : t
  val join : t -> t -> t
  val widen : int -> t -> t -> t
  val leq : t -> t -> bool
end) : GRAPH with type x := X.t list and type y := Y.t

module MakeAbstract (V : sig
  include ORDERED_TYPE
  include PP_TYPE with type t := t

  val as_bool : t -> abs_bool
  val bot : t
  val is_bot : t -> bool
  val join : t -> t -> t

  val app_map_reduce :
    map:(F.t -> int -> t list -> 'a) ->
    join:('a -> 'a -> 'a) ->
    bot:'a ->
    top:'a ->
    t ->
    'a

  val inject_val : Concrete.V.t -> t
end)
(_ : GRAPH with type x := V.t list and type y := V.t)
(_ : functor
  (G : GRAPH with type x := V.t list and type y := V.t)
  -> HoCommon.INTERP with type v := V.t and type phi = G.t FMap.t option) : sig
  val run :
    delay_widening:int ->
    (string, prim, string) HoCommon.program ->
    (string * HoCommon.Concrete.V.t list) list ->
    unit
end

module Concrete : sig
  val run :
    delay_widening:int ->
    (string, prim, string) HoCommon.program ->
    (string * HoCommon.Concrete.V.t list) list ->
    unit
end
