(**
  Author: Benoît Montagu <benoit.montagu@inria.fr>
  Copyright Inria © 2022
*)

{
open Parser
type range = Lexing.position * Lexing.position
type err = UnknownChar of char | EndOfFile
exception Error of range * err
}

let digit = ['0'-'9']
let num = digit+
let alpha = ['a'-'z'] | ['A'-'Z']
let alpha_num = alpha | num
let ident = alpha (alpha_num | '_' | '\'')*
let newline = '\n' | "\r\n"
let whitespace = (' ' | '\t')+

rule lex = parse
| "//" { one_line_comment lexbuf }
| "/*" { multi_line_comment lexbuf }
| whitespace { lex lexbuf }
| newline { Lexing.new_line lexbuf; lex lexbuf }
| '_' { UNDERSCORE }
| '+' { PLUS }
| '-' { MINUS }
| '*' { TIMES }
| '=' { EQ }
| "<=" { LE }
| '<' { LT }
| ">=" { GE }
| '>' { GT }
| '(' { LPAR }
| ')' { RPAR }
| ',' { COMMA }
| "init" { INIT }
| "true" { BOOL true }
| "false" { BOOL false }
| "fun" { FUN }
| "if" { IF }
| "then" { THEN }
| "else" { ELSE }
| num as i { INT (int_of_string i)}
| ident as x { VAR x }
| eof { EOF }
| _ as s
{ let range = Lexing.(lexeme_start_p lexbuf, lexeme_end_p lexbuf) in
  raise (Error (range, UnknownChar s))
}

and one_line_comment = parse
| newline { Lexing.new_line lexbuf; lex lexbuf }
| eof { EOF }
| _ { one_line_comment lexbuf }

and multi_line_comment = parse
| "*/" { lex lexbuf }
| newline { Lexing.new_line lexbuf; multi_line_comment lexbuf }
| eof
{ let range = Lexing.(lexeme_start_p lexbuf, lexeme_end_p lexbuf) in
  raise (Error (range, EndOfFile)) }
| _ { multi_line_comment lexbuf }

{ }
