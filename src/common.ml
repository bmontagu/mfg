(**
  Author: Benoît Montagu <benoit.montagu@inria.fr>
  Copyright Inria © 2022
*)

open Sigs

type abs_bool = NoBool | Bool of bool | AnyBool

let abs_bool_join b1 b2 =
  match (b1, b2) with
  | NoBool, b | b, NoBool -> b
  | AnyBool, _ | _, AnyBool -> AnyBool
  | (Bool b1 as b), Bool b2 -> if b1 = b2 then b else AnyBool

type prim =
  | Bool of bool (* arity 0 *)
  | Int of int (* arity 0 *)
  | Plus (* arity 2 *)
  | Minus (* arity 2 *)
  | Mult (* arity 2 *)
  | Eq (* arity 2 *)
  | Lt (* arity 2 *)
  | Le (* arity 2 *)
  | Gt (* arity 2 *)
  | Ge (* arity 2 *)

let rec list_join join l1 l2 =
  match (l1, l2) with
  | [], l | l, [] -> l
  | v1 :: l1, v2 :: l2 -> join v1 v2 :: list_join join l1 l2

module ListOf (X : sig
  include ORDERED_TYPE
  include PP_TYPE with type t := t
end) : sig
  include ORDERED_TYPE with type t = X.t list
  include PP_TYPE with type t := t
end = struct
  type t = X.t list [@@deriving ord]

  let pp fmt (l : X.t list) =
    let open Format in
    match l with
    | [ v ] -> X.pp fmt v
    | _ ->
        fprintf fmt "@[(%a)@]"
          (pp_print_list ~pp_sep:(fun fmt () -> fprintf fmt ",@ ") X.pp)
          l
end
