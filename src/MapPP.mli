module Make (X : sig
  type t

  val compare : t -> t -> int
  val pp : Format.formatter -> t -> unit
end) : sig
  include Map.S with type key = X.t and type 'a t = 'a Map.Make(X).t

  val pp : (Format.formatter -> 'a -> unit) -> Format.formatter -> 'a t -> unit
end
