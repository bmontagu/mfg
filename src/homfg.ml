(**
  Author: Benoît Montagu <benoit.montagu@inria.fr>
  Copyright Inria © 2022
*)

(**
   Retranscription in OCaml of the following research paper:

   Jones, N. D. & Rosendahl, M.
   Higher-Order Minimal Function Graphs
   Journal of Functional and Logic Programming, 1997
*)

open HoCommon
open Sigs
open Common

module type DOMAIN = sig
  type v
  type c
  type d
  type f
  type x
  type a
  type phi

  val bot : d
  val is_bot : v -> bool
  val fetch : int -> v list -> d
  val fun_ : f -> int -> v list -> d
  val basic : a -> d list -> d
  val apply : phi -> d -> d -> d
  val cond : d -> d -> d -> d
  val init : c -> phi -> phi
  val iterate : phi -> (f * (v list -> d)) list -> phi
  val fix : delay_widening:int -> (phi -> phi) -> phi
end

module type INTERP = sig
  type f
  type x
  type v
  type c
  type d
  type phi
  type expr
  type program

  val expr : (x -> v list -> d) -> (f -> d) -> expr -> phi -> v list -> d
  val program : delay_widening:int -> program -> c -> phi
end

module Interp
    (X : ORDERED_TYPE)
    (A : TYPE)
    (F : ORDERED_TYPE)
    (D : DOMAIN with type f := F.t and type x := X.t and type a := A.t) :
  INTERP
    with type expr = (X.t, A.t, F.t) expr
     and type program = (X.t, A.t, F.t) program
     and type f := F.t
     and type x := X.t
     and type v := D.v
     and type c := D.c
     and type d := D.d
     and type phi := D.phi = struct
  type nonrec expr = (X.t, A.t, F.t) expr
  type nonrec program = (X.t, A.t, F.t) program

  let rec expr (fetch : X.t -> D.v list -> D.d) (fun_ : F.t -> D.d) (e : expr)
      (phi : D.phi) (vs : D.v list) : D.d =
    match e with
    | Var x -> fetch x vs
    | Fun f -> fun_ f
    | Cons (a, es) ->
        D.basic a (List.map (fun e -> expr fetch fun_ e phi vs) es)
    | Call (e1, e2) ->
        D.apply phi (expr fetch fun_ e1 phi vs) (expr fetch fun_ e2 phi vs)
    | IfThenElse (e1, e2, e3) ->
        D.cond
          (expr fetch fun_ e1 phi vs)
          (expr fetch fun_ e2 phi vs)
          (expr fetch fun_ e3 phi vs)

  module E = Map.Make (X)
  module FMap = Map.Make (F)

  let program ~delay_widening (prog : program) (c : D.c) : D.phi =
    let resolver =
      List.fold_left
        (fun resolver (f, xs, _e) ->
          let env =
            let _n, env =
              List.fold_left
                (fun (i, m) x -> (i + 1, E.add x i m))
                (0, E.empty) xs
            in
            env
          in
          FMap.add f env resolver)
        FMap.empty prog
    in
    let resolve f =
      let env = FMap.find f resolver in
      fun x ->
        try E.find x env
        with Not_found -> failwith "Variable not found in environment"
    in
    let arities =
      List.fold_left
        (fun arities (f, xs, _e) -> FMap.add f (List.length xs) arities)
        FMap.empty prog
    in
    let fun_ f = D.fun_ f (FMap.find f arities) [] in
    D.fix ~delay_widening (fun phi ->
        D.init c
        @@ D.iterate phi
             (List.map
                (fun (f, xs, e) ->
                  let fetch =
                    let env = resolve f in
                    fun x vs -> D.fetch (env x) vs
                  in
                  let n = List.length xs in
                  ( f,
                    fun vs ->
                      (* Here we differ from the paper on higher-order
                         function graphs: we create a closure when the
                         application is partial *)
                      if List.exists D.is_bot vs then
                        D.fun_ f n (List.filter (Fun.negate D.is_bot) vs)
                      else expr fetch fun_ e phi vs ))
                prog))
end

module type MINIMAL_MAP = sig
  type 'a t
  type key

  val empty : 'a t
  val singleton : key -> 'a -> 'a t
  val find_opt : key -> 'a t -> 'a option
  val map : ('a -> 'b) -> 'a t -> 'b t
  val union : (key -> 'a -> 'a -> 'a option) -> 'a t -> 'a t -> 'a t

  val merge :
    (key -> 'a option -> 'b option -> 'c option) -> 'a t -> 'b t -> 'c t

  val for_all : (key -> 'a -> bool) -> 'a t -> bool
  val fold : (key -> 'a -> 'b -> 'b) -> 'a t -> 'b -> 'b
  val pp : (Format.formatter -> 'a -> unit) -> Format.formatter -> 'a t -> unit
end

module MFGDomain
    (X : TYPE)
    (A : TYPE)
    (F : TYPE)
    (FMap : MINIMAL_MAP with type key = F.t) (V : sig
      include ORDERED_TYPE
      include PP_TYPE with type t := t

      val as_bool : t -> abs_bool
      val bot : t
      val is_bot : t -> bool
      val join : t -> t -> t

      val app_map_reduce :
        map:(F.t -> int -> t list -> 'a) ->
        join:('a -> 'a -> 'a) ->
        bot:'a ->
        top:'a ->
        t ->
        'a
    end)
    (Graph : GRAPH with type x := V.t list and type y := V.t) (Args : sig
      type t

      val singleton : V.t list -> t
      val join : t -> t -> t
      val map : (V.t list -> V.t) -> t -> Graph.t
    end) (I : sig
      type phi = Graph.t FMap.t option

      val fun_ : F.t -> int -> V.t list -> V.t
      val basic : A.t -> V.t list -> V.t
      val apply : phi -> V.t -> V.t -> V.t
    end) : sig
  type v = V.t
  type f = F.t

  module C : sig
    type t = Args.t FMap.t option

    val bot : t
    val join : t -> t -> t
    val singleton : F.t -> v list -> t
  end

  type c = C.t

  module Phi : sig
    type t = I.phi

    include PP_TYPE with type t := t
  end

  include
    DOMAIN
      with type f := f
       and type x := X.t
       and type a := A.t
       and type v := v
       and type c := c
       and type d = v * c
       and type phi = Phi.t
end = struct
  type v = V.t
  type f = F.t

  module VS = ListOf (V)

  module C : sig
    type t = Args.t FMap.t option

    val bot : t
    val top : t
    val join : t -> t -> t
    val singleton : F.t -> v list -> t
  end = struct
    type t = Args.t FMap.t option

    let bot = Some FMap.empty
    let top = None

    let join o1 o2 =
      match (o1, o2) with
      | None, _ | _, None -> None
      | Some f1, Some f2 ->
          Some
            (FMap.union
               (fun _f args1 args2 -> Some (Args.join args1 args2))
               f1 f2)

    let singleton f vs = Some (FMap.singleton f (Args.singleton vs))
  end

  type c = C.t
  type d = v * c

  let bot = (V.bot, C.bot)
  let is_bot = V.is_bot

  module Phi = struct
    type t = Graph.t FMap.t option

    let bot = Some FMap.empty
    let top = None
    let singleton f graph = Some (FMap.singleton f graph)

    let join_gen join ophi1 ophi2 =
      match (ophi1, ophi2) with
      | None, _ | _, None -> None
      | Some phi1, Some phi2 ->
          Some (FMap.union (fun f g1 g2 -> Some (join f g1 g2)) phi1 phi2)

    let join ophi1 ophi2 = join_gen (fun _f -> Graph.join) ophi1 ophi2

    let widen ~delay_widening creation_times n ophi1 ophi2 =
      join_gen
        (fun f ->
          let m = Option.get @@ FMap.find_opt f creation_times in
          if n - m < delay_widening then Graph.join
          else Graph.widen (n - m - delay_widening))
        ophi1 ophi2

    (* [new_entries phi1 phi2] creates a map whose keys are the keys
       of [phi2] that are not in [phi1]. The values in the map are the
       unit value. *)
    let new_entries phi1 phi2 =
      FMap.merge
        (fun _k o1 o2 ->
          match (o1, o2) with
          | None, None -> assert false
          | Some _, None -> Some ()
          | Some _, Some _ | None, Some _ -> None)
        phi1 phi2

    let leq ophi1 ophi2 =
      (* whether [ophi1] is smaller than [ophi2], and the new
         entries in [ophi1], that are not present in [ophi2] *)
      match (ophi1, ophi2) with
      | _, None -> (true, FMap.empty)
      | None, Some _ -> (false, FMap.empty)
      | Some phi1, Some phi2 ->
          let is_leq =
            FMap.for_all
              (fun f g1 ->
                match FMap.find_opt f phi2 with
                | Some g2 -> Graph.leq g1 g2
                | None -> Graph.leq g1 Graph.bot)
              phi1
          in
          (is_leq, new_entries phi1 phi2)

    let map_reduce map red bot top ophi =
      match ophi with
      | None -> top
      | Some phi -> FMap.fold (fun f e acc -> red (map f e) acc) phi bot

    let pp fmt =
      let open Format in
      function
      | None -> pp_print_string fmt "⊤"
      | Some phi -> fprintf fmt "@[<v>%a@]" (FMap.pp Graph.pp) phi
  end

  type phi = Phi.t

  let fetch i vs = (List.nth vs i, C.bot)
  let fun_ f n vs = (I.fun_ f n vs, C.bot)

  let split ds =
    List.fold_right
      (fun (v, c) (is_bot, vs, cs) ->
        (is_bot || V.is_bot v, v :: vs, C.join c cs))
      ds (false, [], C.bot)

  let basic a ds =
    let is_bot, vs, cs = split ds in
    if is_bot then (V.bot, cs) else (I.basic a vs, cs)

  let apply (phi : phi) (v1, c1) (v2, c2) =
    let c12 = C.join c1 c2 in
    if V.is_bot v1 || V.is_bot v2 then (V.bot, c12)
    else
      let v = I.apply phi v1 v2
      and c =
        V.app_map_reduce
          ~map:(fun f n vs ->
            (* XXX: here we differ from the original article, where
               the computation was different in the two cases of the
               concrete semantics and the abstract one *)
            let m = List.length vs in
            C.singleton f (vs @ [ v2 ] @ ExtList.repeat (n - (m + 1)) V.bot))
          ~join:C.join ~bot:c12 ~top:C.top v1
      in
      (v, c)

  let cond (v1, cs1) (v2, cs2) (v3, cs3) =
    match V.as_bool v1 with
    | NoBool -> (V.bot, cs1)
    | Bool b -> if b then (v2, C.join cs1 cs2) else (v3, C.join cs1 cs3)
    | AnyBool -> (V.join v2 v3, C.join cs1 (C.join cs2 cs3))

  let save_calls ocs = Option.map (FMap.map (Args.map (fun _ -> V.bot))) ocs
  let init cs phis = Phi.join (save_calls cs) phis
  let call f vs v = Phi.singleton f (Graph.singleton vs v)

  let iterate (phi : phi) (es : (F.t * (v list -> d)) list) : phi =
    Phi.map_reduce
      (fun f graph ->
        let e = List.assoc f es in
        Graph.map_reduce
          (fun input _output ->
            let result, calls = e input in
            Phi.join (call f input result) (save_calls calls))
          Phi.join Phi.bot graph)
      Phi.join Phi.bot Phi.top phi

  (* [merge_entries time old new_] extends [old] with the new entries
     in [new_], giving them the value [time]. *)
  let merge_entries time old new_ =
    FMap.merge
      (fun _k o1 o2 ->
        match (o1, o2) with
        | None, None -> assert false
        | Some _, _ -> o1
        | None, Some () -> Some time)
      old new_

  let fix ~delay_widening f =
    let rec iter x n creation_times =
      if n <> 0 then (
        Format.printf "Iteration #%i:@." n;
        Format.printf "@[%a@]@.@." Phi.pp x);
      let y = f x in
      let stable, new_fields = Phi.leq y x in
      if stable then (
        Format.printf "Fixed point reached in %i iterations.@." n;
        x)
      else
        iter
          (Phi.widen ~delay_widening creation_times n x y)
          (n + 1)
          (merge_entries n creation_times new_fields)
    in
    iter Phi.bot 0 FMap.empty
end

module Make
    (X : ORDERED_TYPE)
    (A : TYPE)
    (F : ORDERED_TYPE)
    (FMap : MINIMAL_MAP with type key = F.t) (V : sig
      include ORDERED_TYPE
      include PP_TYPE with type t := t

      val as_bool : t -> abs_bool
      val bot : t
      val is_bot : t -> bool
      val join : t -> t -> t

      val app_map_reduce :
        map:(F.t -> int -> t list -> 'a) ->
        join:('a -> 'a -> 'a) ->
        bot:'a ->
        top:'a ->
        t ->
        'a
    end)
    (Graph : GRAPH with type x := V.t list and type y := V.t) (Args : sig
      type t

      val singleton : V.t list -> t
      val join : t -> t -> t
      val map : (V.t list -> V.t) -> t -> Graph.t
    end) (I : sig
      type phi = Graph.t FMap.t option

      val fun_ : F.t -> int -> V.t list -> V.t
      val basic : A.t -> V.t list -> V.t
      val apply : phi -> V.t -> V.t -> V.t
    end) =
struct
  module D = MFGDomain (X) (A) (F) (FMap) (V) (Graph) (Args) (I)
  include Interp (X) (A) (F) (D)
end
