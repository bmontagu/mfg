(**
  Author: Benoît Montagu <benoit.montagu@inria.fr>
  Copyright Inria © 2022
*)

(**
   Retranscription in OCaml of the following research paper:

   Data Flow Analysis of Applicative Programs Using Minimal Function Graphs
   Neil D. Jones and Alan Mycroft
   Proceedings of the 13th ACM SIGACT-SIGPLAN symposium on Principles of programming languages - POPL '86
   ACM Press, 1986
*)

open Sigs
open Common
open FoCommon

module type DOMAIN = sig
  type v
  type c
  type d
  type f
  type x
  type a
  type phi

  val fetch : int -> v list -> d
  val basic : a -> d list -> d
  val apply : f -> phi -> d list -> d
  val cond : d -> d -> d -> d
  val init : c -> phi -> phi
  val iterate : phi -> (f * (v list -> d)) list -> phi
  val fix : delay_widening:int -> (phi -> phi) -> phi
end

module type INTERP = sig
  type x
  type v
  type c
  type d
  type phi
  type expr
  type program

  val expr : (x -> v list -> d) -> expr -> phi -> v list -> d
  val program : delay_widening:int -> program -> c -> phi
end

module Interp
    (X : ORDERED_TYPE)
    (A : TYPE)
    (F : ORDERED_TYPE)
    (D : DOMAIN with type f := F.t and type x := X.t and type a := A.t) :
  INTERP
    with type expr = (X.t, A.t, F.t) expr
     and type program = (X.t, A.t, F.t) program
     and type x := X.t
     and type v := D.v
     and type c := D.c
     and type d := D.d
     and type phi := D.phi = struct
  type nonrec expr = (X.t, A.t, F.t) expr
  type nonrec program = (X.t, A.t, F.t) program

  let rec expr (fetch : X.t -> D.v list -> D.d) (e : expr) (phi : D.phi)
      (vs : D.v list) : D.d =
    match e with
    | Var x -> fetch x vs
    | Cons (a, es) -> D.basic a (List.map (fun e -> expr fetch e phi vs) es)
    | Call (f, es) -> D.apply f phi (List.map (fun e -> expr fetch e phi vs) es)
    | IfThenElse (e1, e2, e3) ->
        D.cond (expr fetch e1 phi vs) (expr fetch e2 phi vs)
          (expr fetch e3 phi vs)

  module E = Map.Make (X)
  module FMap = Map.Make (F)

  let program ~delay_widening (prog : program) (c : D.c) : D.phi =
    let resolver =
      List.fold_left
        (fun resolver (f, xs, _e) ->
          let env =
            let _n, env =
              List.fold_left
                (fun (i, m) x -> (i + 1, E.add x i m))
                (0, E.empty) xs
            in
            env
          in
          FMap.add f env resolver)
        FMap.empty prog
    in
    let resolve f =
      let env = FMap.find f resolver in
      fun x ->
        try E.find x env
        with Not_found -> failwith "Variable not found in environment"
    in
    D.fix ~delay_widening (fun phi ->
        D.init c
        @@ D.iterate phi
             (List.map
                (fun (f, _xs, e) ->
                  let fetch =
                    let env = resolve f in
                    fun x vs -> D.fetch (env x) vs
                  in
                  (f, expr fetch e phi))
                prog))
end

module type MINIMAL_MAP = sig
  type 'a t
  type key

  val empty : 'a t
  val singleton : key -> 'a -> 'a t
  val find_opt : key -> 'a t -> 'a option
  val map : ('a -> 'b) -> 'a t -> 'b t
  val union : (key -> 'a -> 'a -> 'a option) -> 'a t -> 'a t -> 'a t

  val merge :
    (key -> 'a option -> 'b option -> 'c option) -> 'a t -> 'b t -> 'c t

  val for_all : (key -> 'a -> bool) -> 'a t -> bool
  val fold : (key -> 'a -> 'b -> 'b) -> 'a t -> 'b -> 'b
  val pp : (Format.formatter -> 'a -> unit) -> Format.formatter -> 'a t -> unit
end

module MFGDomain
    (X : TYPE)
    (A : TYPE)
    (F : TYPE)
    (FMap : MINIMAL_MAP with type key = F.t) (V : sig
      include ORDERED_TYPE
      include PP_TYPE with type t := t

      val as_bool : t -> abs_bool
      val bot : t
      val is_bot : t -> bool
      val join : t -> t -> t
    end)
    (Graph : GRAPH with type x := V.t list and type y := V.t) (I : sig
      type phi = Graph.t FMap.t

      val basic : A.t -> V.t list -> V.t
      val apply : F.t -> phi -> V.t list -> V.t
    end) : sig
  type v = V.t
  type f = F.t

  module Phi : sig
    type t = I.phi

    val bot : t
    val singleton : F.t -> Graph.t -> t
    val join : t -> t -> t

    include PP_TYPE with type t := t
  end

  type c = Phi.t

  include
    DOMAIN
      with type f := f
       and type x := X.t
       and type a := A.t
       and type v := v
       and type c := c
       and type d = v * c
       and type phi = Phi.t
end = struct
  type v = V.t
  type f = F.t

  module Phi = struct
    type t = Graph.t FMap.t

    let bot = FMap.empty
    let singleton = FMap.singleton

    let join_gen join phi1 phi2 =
      FMap.union (fun f g1 g2 -> Some (join f g1 g2)) phi1 phi2

    let join phi1 phi2 = join_gen (fun _f -> Graph.join) phi1 phi2

    let widen ~delay_widening creation_times n phi1 phi2 =
      join_gen
        (fun f ->
          let m = Option.get @@ FMap.find_opt f creation_times in
          if n - m < delay_widening then Graph.join
          else Graph.widen (n - m - delay_widening))
        phi1 phi2

    (* [new_entries phi1 phi2] creates a map whose keys are the keys
       of [phi2] that are not in [phi1]. The values in the map are the
       unit value. *)
    let new_entries phi1 phi2 =
      FMap.merge
        (fun _k o1 o2 ->
          match (o1, o2) with
          | None, None -> assert false
          | Some _, None -> Some ()
          | Some _, Some _ | None, Some _ -> None)
        phi1 phi2

    let leq phi1 phi2 =
      (* whether [ophi1] is smaller than [ophi2], and the new
         entries in [ophi1], that are not present in [ophi2] *)
      let is_leq =
        FMap.for_all
          (fun f g1 ->
            match FMap.find_opt f phi2 with
            | Some g2 -> Graph.leq g1 g2
            | None -> Graph.leq g1 Graph.bot)
          phi1
      in
      (is_leq, new_entries phi1 phi2)

    let map_reduce map red bot phi =
      FMap.fold (fun f e acc -> red (map f e) acc) phi bot

    let pp fmt phi = Format.fprintf fmt "@[<v>%a@]" (FMap.pp Graph.pp) phi
  end

  type c = Phi.t
  type d = v * c
  type phi = Phi.t

  let fetch i vs = (List.nth vs i, Phi.bot)

  let split ds =
    List.fold_right
      (fun (v, c) (is_bot, vs, cs) ->
        (is_bot || V.is_bot v, v :: vs, Phi.join c cs))
      ds (false, [], Phi.bot)

  let basic a ds =
    let is_bot, vs, cs = split ds in
    if is_bot then (V.bot, cs) else (I.basic a vs, cs)

  let apply (f : F.t) (phi : phi) ds =
    let is_bot, vs, cs = split ds in
    if is_bot then (V.bot, cs)
    else
      ( I.apply f phi vs,
        Phi.join (Phi.singleton f (Graph.singleton vs V.bot)) cs )

  let cond (v1, cs1) (v2, cs2) (v3, cs3) =
    match V.as_bool v1 with
    | NoBool -> (V.bot, cs1)
    | Bool b -> if b then (v2, Phi.join cs1 cs2) else (v3, Phi.join cs1 cs3)
    | AnyBool -> (V.join v2 v3, Phi.join cs1 (Phi.join cs2 cs3))

  let init cs phis = Phi.join cs phis

  let iterate (phi : phi) (es : (F.t * (v list -> d)) list) : phi =
    Phi.map_reduce
      (fun f graph ->
        let e = List.assoc f es in
        Graph.map_reduce
          (fun input _output ->
            let result, calls = e input in
            Phi.join (Phi.singleton f (Graph.singleton input result)) calls)
          Phi.join Phi.bot graph)
      Phi.join Phi.bot phi

  (* [merge_entries time old new_] extends [old] with the new entries
     in [new_], giving them the value [time]. *)
  let merge_entries time old new_ =
    FMap.merge
      (fun _k o1 o2 ->
        match (o1, o2) with
        | None, None -> assert false
        | Some _, _ -> o1
        | None, Some () -> Some time)
      old new_

  let fix ~delay_widening f =
    let rec iter x n creation_times =
      if n <> 0 then (
        Format.printf "Iteration #%i:@." n;
        Format.printf "@[%a@]@.@." Phi.pp x);
      let y = f x in
      let stable, new_fields = Phi.leq y x in
      if stable then (
        Format.printf "Fixed point reached in %i iterations.@." n;
        x)
      else
        iter
          (Phi.widen ~delay_widening creation_times n x y)
          (n + 1)
          (merge_entries n creation_times new_fields)
    in
    iter Phi.bot 0 FMap.empty
end

module MFG
    (X : ORDERED_TYPE)
    (A : TYPE)
    (F : ORDERED_TYPE)
    (FMap : MINIMAL_MAP with type key = F.t) (V : sig
      include ORDERED_TYPE
      include PP_TYPE with type t := t

      val as_bool : t -> abs_bool
      val bot : t
      val is_bot : t -> bool
      val join : t -> t -> t
    end)
    (Graph : GRAPH with type x := V.t list and type y := V.t) (I : sig
      type phi = Graph.t FMap.t

      val basic : A.t -> V.t list -> V.t
      val apply : F.t -> phi -> V.t list -> V.t
    end) =
struct
  module D = MFGDomain (X) (A) (F) (FMap) (V) (Graph) (I)
  include Interp (X) (A) (F) (D)
end
