(**
  Author: Benoît Montagu <benoit.montagu@inria.fr>
  Copyright Inria © 2022
*)

type range = Lexing.position * Lexing.position
type 'a located = { data : 'a; location : range }
type binop = PLUS | MINUS | TIMES | EQ | LE | LT | GE | GT
type value = VBool of bool | VInt of int

type expr =
  | Bool of bool
  | Int of int
  | Var of string located
  | Binop of expr * binop * expr
  | App of (expr * expr) located
  | IfThenElse of expr * expr * expr
  | Tuple of expr list located

type decl = string located * string option located list * expr
type init = string located * value list
type prog = decl list * init list

module PP = struct
  let binop fmt =
    let open Format in
    function
    | PLUS -> pp_print_char fmt '+'
    | MINUS -> pp_print_char fmt '-'
    | TIMES -> pp_print_char fmt '*'
    | EQ -> pp_print_char fmt '='
    | LE -> pp_print_string fmt "<="
    | LT -> pp_print_char fmt '<'
    | GE -> pp_print_string fmt ">="
    | GT -> pp_print_char fmt '>'

  let rec expr fmt =
    let open Format in
    function
    | Bool b -> pp_print_bool fmt b
    | Int i -> pp_print_int fmt i
    | Var x -> pp_print_string fmt x.data
    | Binop (e1, op, e2) ->
        fprintf fmt "@[(%a@ %a@ %a)@]" expr e1 binop op expr e2
    | App { data = e1, e2; _ } -> fprintf fmt "@[(%a@ %a)@]" expr e1 expr e2
    | IfThenElse (e1, e2, e3) ->
        fprintf fmt "@[(if %a@ then %a@ else %a)@]" expr e1 expr e2 expr e3
    | Tuple { data = [ e ]; _ } -> expr fmt e
    | Tuple { data = es; _ } ->
        fprintf fmt "@[(%a)@]"
          (pp_print_list ~pp_sep:(fun fmt () -> fprintf fmt ",@ ") expr)
          es

  let arg fmt { data = ox; _ } =
    let open Format in
    match ox with
    | None -> pp_print_char fmt '_'
    | Some x -> pp_print_string fmt x

  let args ~fo fmt l =
    let open Format in
    if fo then
      fprintf fmt "@[(%a)@]"
        (pp_print_list ~pp_sep:(fun fmt () -> fprintf fmt ",@ ") arg)
        l
    else fprintf fmt "@[%a@]" (pp_print_list ~pp_sep:pp_print_space arg) l

  let decl ~fo fmt ((f, xs, e) : decl) =
    Format.fprintf fmt "@[fun %s %a =@ %a@]" f.data (args ~fo) xs expr e

  let value fmt =
    let open Format in
    function VBool b -> pp_print_bool fmt b | VInt i -> pp_print_int fmt i

  let values ~fo fmt l =
    let open Format in
    if fo then
      fprintf fmt "@[(%a)@]"
        (pp_print_list ~pp_sep:(fun fmt () -> fprintf fmt ",@ ") value)
        l
    else fprintf fmt "@[%a@]" (pp_print_list ~pp_sep:pp_print_space value) l

  let init ~fo fmt ((f, vs) : init) =
    Format.fprintf fmt "init %s %a" f.data (values ~fo) vs

  let prog ~fo fmt ((decls, inits) : prog) =
    let open Format in
    List.iter (fun d -> fprintf fmt "@[%a@]@." (decl ~fo) d) decls;
    List.iter (fun i -> fprintf fmt "@[%a@]@." (init ~fo) i) inits
end

type error =
  | DuplicateParameter of range * string
  | UnknownVariableOrFunction of range * string
  | UnknownFunction of range * string
  | FunctionDeclaredTwice of range * string
  | BadApplication of range
  | BadApplicationArity of range * string * int * int
  | UnexpectedTuple of range

exception IllFormedProgram of error

let handle_error f x =
  try f x
  with IllFormedProgram err ->
    let open Format in
    (match err with
    | DuplicateParameter (rng, x) ->
        eprintf "%sDuplicate parameter: %s" (MenhirLib.LexerUtil.range rng) x
    | BadApplication rng ->
        eprintf "%sIllegal application" (MenhirLib.LexerUtil.range rng)
    | UnexpectedTuple rng ->
        eprintf "%sUnexpected tuple expression" (MenhirLib.LexerUtil.range rng)
    | UnknownVariableOrFunction (rng, x) ->
        eprintf "%sUnknown variable or function %s"
          (MenhirLib.LexerUtil.range rng)
          x
    | UnknownFunction (rng, x) ->
        eprintf "%sUnknown function %s" (MenhirLib.LexerUtil.range rng) x
    | FunctionDeclaredTwice (rng, f) ->
        eprintf "%sFunction declared more than once %s"
          (MenhirLib.LexerUtil.range rng)
          f
    | BadApplicationArity (rng, f, n, m) ->
        eprintf
          "%sBad application: function %s expects %i parameters, but was \
           provided %i arguments"
          (MenhirLib.LexerUtil.range rng)
          f n m);
    eprintf "@.";
    exit 1

let fail err = raise (IllFormedProgram err)

type var_or_fun = Var | Fun of int

module SMap = Map.Make (String)
module SSet = Set.Make (String)

let check_no_dup xs =
  List.fold_left
    (fun (n, s) ox ->
      match ox.data with
      | Some x ->
          if SSet.mem x s then fail @@ DuplicateParameter (ox.location, x)
          else (n + 1, SSet.add x s)
      | None -> (n + 1, s))
    (0, SSet.empty) xs

let get fs xs x =
  if SSet.mem x.data xs then Var
  else
    match SMap.find_opt x.data fs with
    | Some (n, _) -> Fun n
    | None -> fail @@ UnknownVariableOrFunction (x.location, x.data)

let arities (decls : decl list) =
  List.fold_left
    (fun arities (f, args, _body) ->
      SMap.update f.data
        (function
          | None -> Some (check_no_dup args)
          | Some _ -> fail @@ FunctionDeclaredTwice (f.location, f.data))
        arities)
    SMap.empty decls

module FO = struct
  type outer = expr
  type prim = Common.prim
  type expr = (string, prim, string) FoCommon.expr

  let bool b = FoCommon.(Cons (Common.Bool b, []))
  let int n = FoCommon.(Cons (Common.Int n, []))

  let binop (op : binop) e1 e2 =
    let op : prim =
      match op with
      | PLUS -> Plus
      | MINUS -> Minus
      | TIMES -> Mult
      | EQ -> Eq
      | LE -> Le
      | LT -> Lt
      | GE -> Ge
      | GT -> Lt
    in
    FoCommon.(Cons (op, [ e1; e2 ]))

  let rec expr fs xs : outer -> expr = function
    | Bool b -> bool b
    | Int n -> int n
    | Var x -> (
        match get fs xs x with
        | Var -> Var x.data
        | Fun n -> fail @@ BadApplicationArity (x.location, x.data, n, 0))
    | Binop (e1, op, e2) -> binop op (expr fs xs e1) (expr fs xs e2)
    | App { data = Var x, Tuple es; _ } ->
        let n =
          match get fs xs x with
          | Var -> fail @@ UnknownFunction (x.location, x.data)
          | Fun n -> n
        in
        let es = es.data in
        let m = List.length es in
        if n <> m then fail @@ BadApplicationArity (x.location, x.data, n, m)
        else Call (x.data, List.map (expr fs xs) es)
    | App { data = _; location } -> fail @@ BadApplication location
    | IfThenElse (e1, e2, e3) ->
        IfThenElse (expr fs xs e1, expr fs xs e2, expr fs xs e3)
    | Tuple { data = [ e ]; _ } -> expr fs xs e
    | Tuple l -> fail @@ UnexpectedTuple l.location

  let value : value -> FoCommon.Concrete.V.t = function
    | VBool b -> Bool b
    | VInt n -> Int n

  let prog (decls, inits) =
    let a = arities decls in
    let decls =
      List.map
        (fun (f, xs, body) ->
          let _, vars = SMap.find f.data a in
          ( f.data,
            List.map (fun x -> match x.data with Some x -> x | None -> "_") xs,
            expr a vars body ))
        decls
    in
    let inits =
      List.map
        (fun (f, args) ->
          let n, _ =
            try SMap.find f.data a
            with Not_found -> fail @@ UnknownFunction (f.location, f.data)
          in
          let m = List.length args in
          if m <> n then fail @@ BadApplicationArity (f.location, f.data, n, m)
          else (f.data, List.map value args))
        inits
    in
    (decls, inits)
end

module HO = struct
  type outer = expr
  type prim = Common.prim
  type expr = (string, prim, string) HoCommon.expr

  let bool b = HoCommon.(Cons (Common.Bool b, []))
  let int n = HoCommon.(Cons (Common.Int n, []))

  let binop (op : binop) e1 e2 =
    let op : prim =
      match op with
      | PLUS -> Plus
      | MINUS -> Minus
      | TIMES -> Mult
      | EQ -> Eq
      | LE -> Le
      | LT -> Lt
      | GE -> Ge
      | GT -> Gt
    in
    HoCommon.(Cons (op, [ e1; e2 ]))

  let rec expr fs xs : outer -> expr = function
    | Bool b -> bool b
    | Int n -> int n
    | Var x -> (
        match get fs xs x with Var -> Var x.data | Fun _n -> Fun x.data)
    | Binop (e1, op, e2) -> binop op (expr fs xs e1) (expr fs xs e2)
    | App { data = e1, e2; _ } -> Call (expr fs xs e1, expr fs xs e2)
    | IfThenElse (e1, e2, e3) ->
        IfThenElse (expr fs xs e1, expr fs xs e2, expr fs xs e3)
    | Tuple { data = [ e ]; _ } -> expr fs xs e
    | Tuple l -> fail @@ UnexpectedTuple l.location

  let value : value -> HoCommon.Concrete.V.t = function
    | VBool b -> Bool b
    | VInt n -> Int n

  let prog (decls, inits) =
    let a = arities decls in
    let decls =
      List.map
        (fun (f, xs, body) ->
          let _, vars = SMap.find f.data a in
          ( f.data,
            List.map (fun x -> match x.data with Some x -> x | None -> "_") xs,
            expr a vars body ))
        decls
    in
    let inits =
      List.map
        (fun (f, args) ->
          let n, _ =
            try SMap.find f.data a
            with Not_found -> fail @@ UnknownFunction (f.location, f.data)
          in
          let m = List.length args in
          if m <> n then fail @@ BadApplicationArity (f.location, f.data, n, m)
          else (f.data, List.map value args))
        inits
    in
    (decls, inits)
end
