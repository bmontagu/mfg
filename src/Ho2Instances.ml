(**
  Author: Benoît Montagu <benoit.montagu@inria.fr>
  Copyright Inria © 2022
*)

open Sigs
open Common
open Homfg2

module AbstractGraph
    (MakeGraph : functor
      (X : sig
         type t

         val compare : t -> t -> int
         val pp : Format.formatter -> t -> unit
         val is_bot : t -> bool
         val leq : t -> t -> bool
         val join : t -> t -> t
         val widen : int -> t -> t -> t
         val meet : t -> t -> t
       end)
      (Y : sig
         type t

         val bot : t
         val leq : t -> t -> bool
         val join : t -> t -> t
         val widen : int -> t -> t -> t
         val pp : Format.formatter -> t -> unit
       end)
      ->
  HoCommon.GRAPH with type x := X.t and type y := Y.t) (X : sig
  include ORDERED_TYPE
  include PP_TYPE with type t := t

  val is_bot : t -> bool
  val leq : t -> t -> bool
  val join : t -> t -> t
  val widen : int -> t -> t -> t
  val meet : t -> t -> t
end) (Y : sig
  include PP_TYPE

  val bot : t
  val join : t -> t -> t
  val widen : int -> t -> t -> t
  val leq : t -> t -> bool
end) : HoCommon.GRAPH with type x := X.t list and type y := Y.t = struct
  module XS = struct
    include ListOf (X)

    let bot = []
    let is_bot = ExtList.is_empty
    let len = List.length

    let leq xs1 xs2 =
      is_bot xs1
      ||
      let n1 = len xs1 and n2 = len xs2 in
      n1 < n2 || (n1 = n2 && List.for_all2 X.leq xs1 xs2)

    let join_gen join xs1 xs2 =
      if is_bot xs1 then xs2
      else if is_bot xs2 then xs1
      else list_join join xs1 xs2

    let join xs1 xs2 = join_gen X.join xs1 xs2
    let widen n xs1 xs2 = join_gen (X.widen n) xs1 xs2

    let meet xs1 xs2 =
      if is_bot xs1 || is_bot xs2 || len xs1 <> len xs2 then bot
      else if List.for_all X.is_bot xs1 then xs2
      else if List.for_all X.is_bot xs2 then xs1
      else
        let xs12 = List.map2 X.meet xs1 xs2 in
        if List.exists X.is_bot xs12 then bot else xs12
  end

  include
    HoCommon.AbstractGraph (XS) (Y) (MakeGraph (XS) (Y))
      (struct
        let partial_applications_in_graph_domain = true
      end)
end

module MakeAbstract (V : sig
  include ORDERED_TYPE
  include PP_TYPE with type t := t

  val as_bool : t -> abs_bool
  val bot : t
  val is_bot : t -> bool
  val join : t -> t -> t

  val app_map_reduce :
    map:(HoCommon.F.t -> int -> t list -> 'a) ->
    join:('a -> 'a -> 'a) ->
    bot:'a ->
    top:'a ->
    t ->
    'a

  val inject_val : HoCommon.Concrete.V.t -> t
end)
(G : HoCommon.GRAPH with type x := V.t list and type y := V.t)
(MakeI : functor
  (G : HoCommon.GRAPH with type x := V.t list and type y := V.t)
  ->
  HoCommon.INTERP with type v := V.t and type phi = G.t HoCommon.FMap.t option) : sig
  val run :
    delay_widening:int ->
    (string, prim, string) HoCommon.program ->
    (string * HoCommon.Concrete.V.t list) list ->
    unit
end = struct
  module I = MakeI (G)

  module MFG =
    Make (HoCommon.X) (HoCommon.A) (HoCommon.F) (HoCommon.FMap) (V) (G) (I)

  let convert_c l : MFG.D.c =
    List.fold_left
      (fun acc (f, args) ->
        let g = G.singleton ((List.map V.inject_val) args) V.bot in
        MFG.D.Phi.(join (singleton f g) acc))
      MFG.D.Phi.bot l

  let run ~delay_widening prog init =
    let phi = MFG.program ~delay_widening prog (convert_c init) in
    Format.printf "%a@." MFG.D.Phi.pp phi
end

module Concrete = struct
  open HoCommon.Concrete

  module I = MakeI (struct
    let partial_applications_in_graph_domain = true
  end)

  module MFG =
    Make (HoCommon.X) (HoCommon.A) (HoCommon.F) (HoCommon.FMap) (V) (G) (I)

  let convert_c l : MFG.D.c =
    List.fold_left
      (fun acc (f, args) ->
        MFG.D.Phi.(join (singleton f (G.singleton args V.bot)) acc))
      MFG.D.Phi.bot l

  let run ~delay_widening prog init =
    let phi = MFG.program ~delay_widening prog (convert_c init) in
    Format.printf "%a@." MFG.D.Phi.pp phi
end
