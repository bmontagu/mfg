(**
  Author: Benoît Montagu <benoit.montagu@inria.fr>
  Copyright Inria © 2022
*)

open Sigs
open Common
open FoCommon

module MakeAbstract (V : sig
  include ORDERED_TYPE
  include PP_TYPE with type t := t

  val inject_val : Concrete.V.t -> t
  val as_bool : t -> Common.abs_bool
  val bot : t
  val is_bot : t -> bool
  val join : t -> t -> t
end)
(_ : GRAPH with type x := V.t list and type y := V.t)
(_ : functor
  (G : GRAPH with type x := V.t list and type y := V.t)
  -> sig
  type phi = G.t FMap.t

  val basic : A.t -> V.t list -> V.t
  val apply : F.t -> phi -> V.t list -> V.t
end) : sig
  val run :
    delay_widening:int ->
    (string, Common.prim, string) FoCommon.program ->
    (string * FoCommon.Concrete.V.t list) list ->
    unit
end

module Concrete : sig
  val run :
    delay_widening:int ->
    (string, prim, string) FoCommon.program ->
    (string * FoCommon.Concrete.V.t list) list ->
    unit
end
