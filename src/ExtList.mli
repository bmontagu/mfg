(**
  Author: Benoît Montagu <benoit.montagu@inria.fr>
  Copyright Inria © 2022
*)

val is_empty : 'a list -> bool
(** [is_empty l] returns [true] iff [l] is the empty list *)

val longer_than : int -> 'a list -> bool
(** [longer_than n l] returns [true] iff [length l >= n] *)

val shorter : strict:bool -> 'a list -> 'b list -> bool
(** [shorter ~strict:false l1 l2] returns [true] iff [length l1 <= length l2].
    [shorter ~strict:true l1 l2] returns [true] iff [length l1 < length l2].
*)

val same_length : 'a list -> 'b list -> bool
(** [same_length l1 l2] returns [true] iff the lists [l1] and [l2] have the same length *)

val count : ('a -> bool) -> 'a list -> int
(** [count p l] returns the number of elements of the list [l] that
    satisfy the predicate [p] *)

val count_first : ('a -> bool) -> 'a list -> int
(** [count_first p l] returns the largest [n] such that [n = length l1]
and [l = l1 @ l2] and every element of [l1] satisfies [p] *)

val repeat : int -> 'a -> 'a list
(** [repeat n v] is the list of length [n] that contains only the value [v]
    @raise [Invalid_argument] when [n < 0] *)

val drop : int -> 'a list -> 'a list
(** [drop n l] removes the [n] first elements of the list [l].
    @raise [Invalid_argument] when the condition [0 <= n <= length l]
      is not satisfied *)

val drop2 : int -> 'a list -> 'b list -> 'a list * 'b list
(** [drop2 n l1 l2] removes the [n] first elements of the lists [l1] and [l2].
    @raise [Invalid_argument] when the condition [0 <= n <= min
      (length l1) (length l2)] is not satisfied *)

val exists_before : int -> ('a -> bool) -> 'a list -> bool
(** [exists_before n p l] returns [true] iff there exists a value [v]
    in [l] located at some index [i] such that [i < n] and such that
    [p v = true].
    @raise [Invalid_argument] when the condition [0 <= n] is not
      satisfied or when [n <= length l] is not satisfied whenever no
      element in [l] satisfies [p] *)

val exists_before2 : int -> ('a -> 'b -> bool) -> 'a list -> 'b list -> bool
(** [exists_before2 n p l1 l2] returns [true] iff there exists a value
    [v1] in [l1] and a value [v2] in [l2] both located at some index
    [i] such that [i < n] and such that [p v1 v2 = true].
    @raise [Invalid_argument] when the condition [0 <= n] is not
      satisfied or when [n <= min (length l1) (length l2)] is not
      satisfied whenever no element in [l1] or in [l2] satisfies
      [p] *)

val exists_in_range : int -> int -> ('a -> bool) -> 'a list -> bool
(** [exists_in_range start len p l] returns [true] iff there exists a
    value [v] in [l] located at some index [i] such that
    [start <= i < start + len] and such that [p v = true].
    @raise [Invalid_argument] when the condition [0 <= start <= length l]
      is not satisfied or when [start + len <= length l] is not
      satisfied whenever no element in [l] satisfies [p] *)

val exists_in_range2 :
  int -> int -> ('a -> 'b -> bool) -> 'a list -> 'b list -> bool
(** [exists_in_range2 start len p l1 l2] returns [true] iff there
    exists a value [v1] in [l1] and a value [v2] in [l2] both located
    at some index [i] such that [start <= i < start + len] and such
    that [p v1 v2 = true].
    @raise [Invalid_argument] when the condition
      [0 <= start <= min (length l1) (length l2)] is not
      satisfied or when [start + len <= min (length l1) (length l2)] is not
      satisfied whenever no element in [l1] or in [l2] satisfies
      [p] *)

val for_all_before : int -> ('a -> bool) -> 'a list -> bool
(** [for_all_before n p l] returns [true] iff for every value [v] in
    [l] located at some index [i] such that [i < n], the condition
    [p v = true] holds.
    @raise [Invalid_argument] when the condition [0 <= n] is not
      satisfied or when [n <= length l] is not satisfied whenever every
      element in [l] satisfies [p] *)

val for_all_before2 : int -> ('a -> 'b -> bool) -> 'a list -> 'b list -> bool
(** [for_all_before2 n p l1 l2] returns [true] iff for every value
    [v1] in [l1] and a value [v2] in [l2] both located at some index
    [i] such that [i < n], the condition [p v1 v2 = true] holds.
    @raise [Invalid_argument] when the condition [0 <= n] is not
      satisfied or when [n <= min (length l1) (length l2)] is not
      satisfied whenever every element in [l1] or in [l2] satisfies
      [p] *)

val for_all_in_range : int -> int -> ('a -> bool) -> 'a list -> bool
(** [for_all_in_range start len p l] returns [true] iff for every
    value [v] in [l] located at some index [i] such that [start <= i <
    start + len] and such that [p v = true].
    @raise [Invalid_argument] when the condition [0 <= start <= length l]
      is not satisfied or when [start + len <= length l] is not
      satisfied whenever every element in [l] satisfies [p] *)

val for_all_in_range2 :
  int -> int -> ('a -> 'b -> bool) -> 'a list -> 'b list -> bool
(** [for_all_in_range2 start len p l1 l2] returns [true] iff for
    every value [v1] in [l1] and a value [v2] in [l2] both located
    at some index [i] such that [start <= i < start + len], the condition
    [p v1 v2 = true] holds.
    @raise [Invalid_argument] when the condition
      [0 <= start <= min (length l1) (length l2)] is not
      satisfied or when [start + len <= min (length l1) (length l2)] is not
      satisfied whenever every element in [l1] or in [l2] satisfies
      [p] *)
