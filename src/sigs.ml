(**
  Author: Benoît Montagu <benoit.montagu@inria.fr>
  Copyright Inria © 2022
*)

module type TYPE = sig
  type t
end

module type ORDERED_TYPE = Map.OrderedType

module type PP_TYPE = sig
  type t

  val pp : Format.formatter -> t -> unit
end
