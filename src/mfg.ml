(**
  Author: Benoît Montagu <benoit.montagu@inria.fr>
  Copyright Inria © 2022
*)

let handle_parse_error error_buf parse lexbuf =
  let open Format in
  try Unix.handle_unix_error parse lexbuf with
  | Lexer.Error (r, err) ->
      (match err with
      | UnknownChar c ->
          eprintf "%sLexing error: unexpected token %c@."
            (MenhirLib.LexerUtil.range r)
            c
      | Lexer.EndOfFile ->
          eprintf "%sLexing error: unexpected end of file@."
            (MenhirLib.LexerUtil.range r));
      exit 1
  | Parser.Error ->
      let rng = MenhirLib.ErrorReports.last error_buf in
      eprintf "%sParse error: unexpected token %s@."
        (MenhirLib.LexerUtil.range rng)
        (Lexing.lexeme lexbuf);
      exit 1

let parse file =
  let error_buf, lexer = MenhirLib.ErrorReports.wrap Lexer.lex in
  file |> open_in |> Lexing.from_channel
  |> MenhirLib.LexerUtil.init file
  |> handle_parse_error error_buf (Parser.prog lexer)

type cmd = FO | FO2 | HO | HO2
type mode = Concrete | TypeAnalysis | ConstantAnalysis | IntervalAnalysis
type partitioning = Basic | Elementary

type options = {
  cmd : cmd;
  mode : mode;
  partitioning : partitioning;
  print : bool;
  delay_widening : int;
}

let print options ast =
  if options.print then
    let fo = match options.cmd with FO | FO2 -> true | HO | HO2 -> false in
    Format.printf "%a@." (Ast.PP.prog ~fo) ast

let run_cmd options ast =
  print options ast;
  let delay_widening = options.delay_widening in
  match options.cmd with
  | FO -> (
      let prog, init = Ast.handle_error Ast.FO.prog ast in
      let module Inst = FoInstances in
      match options.mode with
      | Concrete -> Inst.Concrete.run ~delay_widening prog init
      | TypeAnalysis ->
          let module A = FoCommon.AbstractType in
          let module V = A.V in
          let module SIG = struct
            module type S =
              FoCommon.GRAPH with type x := V.t list and type y := V.t
          end in
          let module G = (val match options.partitioning with
                              | Basic -> (module FoCommon.AbstractGraph (V) (V))
                              | Elementary ->
                                  (module FoCommon
                                          .AbstractGraphElementaryPartitioning
                                            (V)
                                            (V)) : SIG.S)
          in
          let module M = Inst.MakeAbstract (V) (G) (A.MakeI) in
          M.run ~delay_widening prog init
      | ConstantAnalysis ->
          let module A = FoCommon.AbstractConst in
          let module V = A.V in
          let module SIG = struct
            module type S =
              FoCommon.GRAPH with type x := V.t list and type y := V.t
          end in
          let module G = (val match options.partitioning with
                              | Basic -> (module FoCommon.AbstractGraph (V) (V))
                              | Elementary ->
                                  (module FoCommon
                                          .AbstractGraphElementaryPartitioning
                                            (V)
                                            (V)) : SIG.S)
          in
          let module M = Inst.MakeAbstract (V) (G) (A.MakeI) in
          M.run ~delay_widening prog init
      | IntervalAnalysis ->
          let module A = FoCommon.AbstractInterval in
          let module V = A.V in
          let module SIG = struct
            module type S =
              FoCommon.GRAPH with type x := V.t list and type y := V.t
          end in
          let module G = (val match options.partitioning with
                              | Basic -> (module FoCommon.AbstractGraph (V) (V))
                              | Elementary ->
                                  (module FoCommon
                                          .AbstractGraphElementaryPartitioning
                                            (V)
                                            (V)) : SIG.S)
          in
          let module M = Inst.MakeAbstract (V) (G) (A.MakeI) in
          M.run ~delay_widening prog init)
  | FO2 -> (
      let prog, init = Ast.handle_error Ast.FO.prog ast in
      let module Inst = Fo2Instances in
      match options.mode with
      | Concrete -> Inst.Concrete.run ~delay_widening prog init
      | TypeAnalysis ->
          let module A = FoCommon.AbstractType in
          let module V = A.V in
          let module SIG = struct
            module type S =
              FoCommon.GRAPH with type x := V.t list and type y := V.t
          end in
          let module G = (val match options.partitioning with
                              | Basic -> (module FoCommon.AbstractGraph (V) (V))
                              | Elementary ->
                                  (module FoCommon
                                          .AbstractGraphElementaryPartitioning
                                            (V)
                                            (V)) : SIG.S)
          in
          let module M = Inst.MakeAbstract (V) (G) (A.MakeI) in
          M.run ~delay_widening prog init
      | ConstantAnalysis ->
          let module A = FoCommon.AbstractConst in
          let module V = A.V in
          let module SIG = struct
            module type S =
              FoCommon.GRAPH with type x := V.t list and type y := V.t
          end in
          let module G = (val match options.partitioning with
                              | Basic -> (module FoCommon.AbstractGraph (V) (V))
                              | Elementary ->
                                  (module FoCommon
                                          .AbstractGraphElementaryPartitioning
                                            (V)
                                            (V)) : SIG.S)
          in
          let module M = Inst.MakeAbstract (V) (G) (A.MakeI) in
          M.run ~delay_widening prog init
      | IntervalAnalysis ->
          let module A = FoCommon.AbstractInterval in
          let module V = A.V in
          let module SIG = struct
            module type S =
              FoCommon.GRAPH with type x := V.t list and type y := V.t
          end in
          let module G = (val match options.partitioning with
                              | Basic -> (module FoCommon.AbstractGraph (V) (V))
                              | Elementary ->
                                  (module FoCommon
                                          .AbstractGraphElementaryPartitioning
                                            (V)
                                            (V)) : SIG.S)
          in
          let module M = Inst.MakeAbstract (V) (G) (A.MakeI) in
          M.run ~delay_widening prog init)
  | HO -> (
      let prog, init = Ast.handle_error Ast.HO.prog ast in
      let module Inst = HoInstances in
      match options.mode with
      | Concrete -> Inst.Concrete.run ~delay_widening prog init
      | TypeAnalysis ->
          let module A = HoCommon.AbstractType in
          let module V = A.V in
          let module SIG = struct
            module type S =
              HoCommon.GRAPH with type x := V.t list and type y := V.t
          end in
          let module G = (val match options.partitioning with
                              | Basic ->
                                  (module Inst.AbstractGraph
                                            (Functional_partitioning.Basic.Make)
                                            (V)
                                            (V))
                              | Elementary ->
                                  (module Inst.AbstractGraph
                                            (Functional_partitioning.Elementary
                                             .Make
                                               (MapPP.Make))
                                               (V)
                                            (V)) : SIG.S)
          in
          let module M = Inst.MakeAbstract (V) (G) (A.MakeI) in
          M.run ~delay_widening prog init
      | ConstantAnalysis ->
          let module A = HoCommon.AbstractConst in
          let module V = A.V in
          let module SIG = struct
            module type S =
              HoCommon.GRAPH with type x := V.t list and type y := V.t
          end in
          let module G = (val match options.partitioning with
                              | Basic ->
                                  (module Inst.AbstractGraph
                                            (Functional_partitioning.Basic.Make)
                                            (V)
                                            (V))
                              | Elementary ->
                                  (module Inst.AbstractGraph
                                            (Functional_partitioning.Elementary
                                             .Make
                                               (MapPP.Make))
                                               (V)
                                            (V)) : SIG.S)
          in
          let module M = Inst.MakeAbstract (V) (G) (A.MakeI) in
          M.run ~delay_widening prog init
      | IntervalAnalysis ->
          let module A = HoCommon.AbstractInterval in
          let module V = A.V in
          let module SIG = struct
            module type S =
              HoCommon.GRAPH with type x := V.t list and type y := V.t
          end in
          let module G = (val match options.partitioning with
                              | Basic ->
                                  (module Inst.AbstractGraph
                                            (Functional_partitioning.Basic.Make)
                                            (V)
                                            (V))
                              | Elementary ->
                                  (module Inst.AbstractGraph
                                            (Functional_partitioning.Elementary
                                             .Make
                                               (MapPP.Make))
                                               (V)
                                            (V)) : SIG.S)
          in
          let module M = Inst.MakeAbstract (V) (G) (A.MakeI) in
          M.run ~delay_widening prog init)
  | HO2 -> (
      let prog, init = Ast.handle_error Ast.HO.prog ast in
      let module Inst = Ho2Instances in
      match options.mode with
      | Concrete -> Inst.Concrete.run ~delay_widening prog init
      | TypeAnalysis ->
          let module A = HoCommon.AbstractType in
          let module V = A.V in
          let module SIG = struct
            module type S =
              HoCommon.GRAPH with type x := V.t list and type y := V.t
          end in
          let module G = (val match options.partitioning with
                              | Basic ->
                                  (module Inst.AbstractGraph
                                            (Functional_partitioning.Basic.Make)
                                            (V)
                                            (V))
                              | Elementary ->
                                  (module Inst.AbstractGraph
                                            (Functional_partitioning.Elementary
                                             .Make
                                               (MapPP.Make))
                                               (V)
                                            (V)) : SIG.S)
          in
          let module M = Inst.MakeAbstract (V) (G) (A.MakeI) in
          M.run ~delay_widening prog init
      | ConstantAnalysis ->
          let module A = HoCommon.AbstractConst in
          let module V = A.V in
          let module SIG = struct
            module type S =
              HoCommon.GRAPH with type x := V.t list and type y := V.t
          end in
          let module G = (val match options.partitioning with
                              | Basic ->
                                  (module Inst.AbstractGraph
                                            (Functional_partitioning.Basic.Make)
                                            (V)
                                            (V))
                              | Elementary ->
                                  (module Inst.AbstractGraph
                                            (Functional_partitioning.Elementary
                                             .Make
                                               (MapPP.Make))
                                               (V)
                                            (V)) : SIG.S)
          in
          let module M = Inst.MakeAbstract (V) (G) (A.MakeI) in
          M.run ~delay_widening prog init
      | IntervalAnalysis ->
          let module A = HoCommon.AbstractInterval in
          let module V = A.V in
          let module SIG = struct
            module type S =
              HoCommon.GRAPH with type x := V.t list and type y := V.t
          end in
          let module G = (val match options.partitioning with
                              | Basic ->
                                  (module Inst.AbstractGraph
                                            (Functional_partitioning.Basic.Make)
                                            (V)
                                            (V))
                              | Elementary ->
                                  (module Inst.AbstractGraph
                                            (Functional_partitioning.Elementary
                                             .Make
                                               (MapPP.Make))
                                               (V)
                                            (V)) : SIG.S)
          in
          let module M = Inst.MakeAbstract (V) (G) (A.MakeI) in
          M.run ~delay_widening prog init)

let run options file = file |> parse |> run_cmd options |> ignore

let file_t =
  let open Cmdliner.Arg in
  required
  @@ pos 0 (some file) None
  @@ info [] ~docv:"FILE" ~doc:"File to analyze."

let cmd_t =
  let open Cmdliner.Arg in
  required
  @@ vflag None
       [
         (Some FO, info [ "fo" ] ~doc:"Version for first-order programs");
         ( Some FO2,
           info [ "fo2" ]
             ~doc:
               "Version for first-order programs, with no specific structure \
                for sets of calls." );
         (Some HO, info [ "ho" ] ~doc:"Version for higher-order programs.");
         ( Some HO2,
           info [ "ho2" ]
             ~doc:
               "Version for higher-order programs, with no specific structure \
                for sets of calls." );
       ]

let mode_t =
  let open Cmdliner.Arg in
  value
  @@ vflag Concrete
       [
         ( Concrete,
           info [ "concrete" ]
             ~doc:
               "Concrete minimal function graph (may not terminate if the \
                graph is infinite)." );
         ( TypeAnalysis,
           info [ "type-analysis" ]
             ~doc:
               "Abstract minimal function graph, where abstract values are \
                types." );
         ( ConstantAnalysis,
           info [ "constant-analysis" ]
             ~doc:
               "Abstract minimal function graph, where abstract values for \
                integers are singletons or any integer." );
         ( IntervalAnalysis,
           info [ "interval-analysis" ]
             ~doc:
               "Abstract minimal function graph, where abstract values for \
                integers are intervals." );
       ]

let partitioning_t =
  let open Cmdliner.Arg in
  value
  @@ opt (enum [ ("basic", Basic); ("elementary", Elementary) ]) Basic
  @@ info [ "partitioning" ]
       ~doc:
         "Which partitioning scheme should be used. $(b,basic) enables the \
          functional partitioning, i.e., one abstract input and one abstract \
          output only. $(b,elementary) enables functional partitioning, i.e., \
          a list of pairs (abstract input, abstract output), with the \
          elementary semantics solely based on unions."

let print_t =
  let open Cmdliner.Arg in
  value @@ flag @@ info [ "print" ] ~doc:"Print the source program."

let delay_widening_t =
  let open Cmdliner.Arg in
  value @@ opt int 0
  @@ info [ "delay-widening" ]
       ~doc:
         "Integer that indicates how many iterations must be done before \
          widening is performed."

let options_t =
  let open Cmdliner.Term in
  const (fun cmd mode partitioning print delay_widening ->
      { cmd; mode; partitioning; print; delay_widening })
  $ cmd_t $ mode_t $ partitioning_t $ print_t $ delay_widening_t

let mfg_t =
  let open Cmdliner.Term in
  const run $ options_t $ file_t

let () =
  exit
    Cmdliner.Cmd.(
      eval
      @@ v
           (info "mfg"
              ~exits:
                Exit.(info 1 ~doc:"on lexing or parsing error." :: defaults)
              ~doc:"Implementation of minimal function graphs")
           mfg_t)
