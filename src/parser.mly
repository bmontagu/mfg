(**
  Author: Benoît Montagu <benoit.montagu@inria.fr>
  Copyright Inria © 2022
*)

%{
open Ast
%}

%token LPAR RPAR COMMA FUN IF THEN ELSE EQ PLUS MINUS TIMES LT LE GT GE UNDERSCORE
%token INIT
%token EOF

%token<string> VAR
%token<int> INT
%token<bool> BOOL

%nonassoc IF ELSE
%right EQ LT LE GT GE
%left PLUS MINUS
%left TIMES
%nonassoc VAR INT BOOL LPAR
%left APP

%start<prog> prog

%%

prog:
| decls = decls
  inits = inits
  EOF
{ (decls, inits) }

params(arg):
| params = delimited(LPAR, separated_nonempty_list(COMMA, arg), RPAR)
{ params }

%inline binop:
| PLUS
{ PLUS }
| MINUS
{ MINUS }
| TIMES
{ TIMES }
| EQ
{ EQ }
| LT
{ LT }
| GT
{ GT }
| LE
{ LE }
| GE
{ GE }

expr:
| i = INT
{ Int i }
| b = BOOL
{ Bool b }
| x = located(VAR)
{ Var x }
| e1 = expr op = binop e2 = expr
{ Binop(e1, op, e2) }
| e1 = expr e2 = expr              %prec APP
{ App { data = (e1, e2); location = $loc } }
| IF e1 = expr THEN e2 = expr ELSE e3 = expr
{ IfThenElse(e1, e2, e3) }
| l = located(params(expr))
{ Tuple l }

arg:
| x = VAR
{ Some x }
| UNDERSCORE
{ None }

decl:
| FUN f = located(VAR) args = params(located(arg)) EQ e = expr
| FUN f = located(VAR) args = located(arg)+ EQ e = expr
{ (f, args, e) }

decls:
| l = decl+
{ l }

value:
| i = INT
{ VInt i }
| MINUS i = INT
{ VInt (-i) }
| b = BOOL
{ VBool b }

init:
| INIT f = located(VAR) args = params(value)
| INIT f = located(VAR) args = value+
{ (f, args) }

inits:
| l = init+
{ l }

located(a):
| a = a
{ { data = a; location = $loc(a) } }
