(**
  Author: Benoît Montagu <benoit.montagu@inria.fr>
  Copyright Inria © 2022
*)

(** [is_empty l] returns [true] iff [l] is the empty list *)
let is_empty = function [] -> true | _ -> false

(** [longer_than n l] returns [true] iff [length l >= n] *)
let rec longer_than n = function
  | [] -> n >= 0
  | _ :: l -> longer_than (n - 1) l

(** [shorter ~strict:false l1 l2] returns [true] iff [length l1 <= length l2].
    [shorter ~strict:true l1 l2] returns [true] iff [length l1 < length l2].
*)
let rec shorter ~strict l1 l2 =
  match (l1, l2) with
  | [], _ :: _ -> true
  | [], [] -> not strict
  | _ :: _, [] -> false
  | _ :: l1, _ :: l2 -> shorter ~strict l1 l2

(** [same_length l1 l2] returns [true] iff the lists [l1] and [l2] have the same length *)
let rec same_length l1 l2 =
  match (l1, l2) with
  | [], [] -> true
  | [], _ | _, [] -> false
  | _ :: q1, _ :: q2 -> same_length q1 q2

(** [count p l] returns the number of elements of the list [l] that
    satisfy the predicate [p] *)
let count p l =
  let rec count l n =
    match l with [] -> n | x :: xs -> count xs (if p x then n + 1 else n)
  in
  count l 0

(** [count_first p l] returns the largest [n] such that [n = length l1]
and [l = l1 @ l2] and every element of [l1] satisfies [p] *)
let count_first p l =
  let rec count l n =
    match l with [] -> n | x :: xs -> if p x then count xs (n + 1) else n
  in
  count l 0

(** [repeat n v] is the list of length [n] that contains only the value [v]
    @raise [Invalid_argument] when [n < 0] *)
let repeat n v =
  if n < 0 then raise (Invalid_argument "repeat: n < 0")
  else
    let rec loop n acc = if n <= 0 then acc else loop (n - 1) (v :: acc) in
    loop n []

(** [drop n l] removes the [n] first elements of the list [l].
    @raise [Invalid_argument] when the condition [0 <= n <= length l]
      is not satisfied *)
let drop n l =
  if n < 0 then raise (Invalid_argument "drop: n < 0")
  else
    let rec drop n l =
      if n = 0 then l
      else
        match l with
        | [] -> raise (Invalid_argument "drop: n > length l")
        | _ :: xs -> drop (n - 1) xs
    in
    drop n l

(** [drop2 n l1 l2] removes the [n] first elements of the lists [l1] and [l2].
    @raise [Invalid_argument] when the condition [0 <= n <= min
      (length l1) (length l2)] is not satisfied *)
let drop2 n l1 l2 =
  if n < 0 then raise (Invalid_argument "drop2: n < 0")
  else
    let rec drop2 n l1 l2 =
      if n = 0 then (l1, l2)
      else
        match (l1, l2) with
        | [], _ -> raise (Invalid_argument "drop2: n > length l1")
        | _, [] -> raise (Invalid_argument "drop2: n > length l2")
        | _ :: xs1, _ :: xs2 -> drop2 (n - 1) xs1 xs2
    in
    drop2 n l1 l2

(** [exists_before n p l] returns [true] iff there exists a value [v]
    in [l] located at some index [i] such that [i < n] and such that
    [p v = true].
    @raise [Invalid_argument] when the condition [0 <= n] is not
      satisfied or when [n <= length l] is not satisfied whenever no
      element in [l] satisfies [p] *)
let exists_before n p l =
  if n < 0 then raise (Invalid_argument "exists_before: n < 0")
  else
    let rec exists_before n l =
      if n = 0 then false
      else
        match l with
        | [] -> raise (Invalid_argument "exists_before: n > length l")
        | x :: xs -> p x || exists_before (n - 1) xs
    in
    exists_before n l

(** [exists_before2 n p l1 l2] returns [true] iff there exists a value
    [v1] in [l1] and a value [v2] in [l2] both located at some index
    [i] such that [i < n] and such that [p v1 v2 = true].
    @raise [Invalid_argument] when the condition [0 <= n] is not
      satisfied or when [n <= min (length l1) (length l2)] is not
      satisfied whenever no element in [l1] or in [l2] satisfies
      [p] *)
let exists_before2 n p l1 l2 =
  if n < 0 then raise (Invalid_argument "exists_before2: n < 0")
  else
    let rec exists_before2 n l1 l2 =
      if n = 0 then false
      else
        match (l1, l2) with
        | [], _ -> raise (Invalid_argument "exists_before2: n > length l1")
        | _, [] -> raise (Invalid_argument "exists_before2: n > length l2")
        | x1 :: xs1, x2 :: xs2 -> p x1 x2 || exists_before2 (n - 1) xs1 xs2
    in
    exists_before2 n l1 l2

(** [exists_in_range start len p l] returns [true] iff there exists a
    value [v] in [l] located at some index [i] such that
    [start <= i < start + len] and such that [p v = true].
    @raise [Invalid_argument] when the condition [0 <= start <= length l]
      is not satisfied or when [start + len <= length l] is not
      satisfied whenever no element in [l] satisfies [p] *)
let exists_in_range start len p l = exists_before len p (drop start l)

(** [exists_in_range2 start len p l1 l2] returns [true] iff there
    exists a value [v1] in [l1] and a value [v2] in [l2] both located
    at some index [i] such that [start <= i < start + len] and such
    that [p v1 v2 = true].
    @raise [Invalid_argument] when the condition
      [0 <= start <= min (length l1) (length l2)] is not
      satisfied or when [start + len <= min (length l1) (length l2)] is not
      satisfied whenever no element in [l1] or in [l2] satisfies
      [p] *)
let exists_in_range2 start len p l1 l2 =
  let l1, l2 = drop2 start l1 l2 in
  exists_before2 len p l1 l2

(** [for_all_before n p l] returns [true] iff for every value [v] in
    [l] located at some index [i] such that [i < n], the condition
    [p v = true] holds.
    @raise [Invalid_argument] when the condition [0 <= n] is not
      satisfied or when [n <= length l] is not satisfied whenever every
      element in [l] satisfies [p] *)
let for_all_before n p l =
  if n < 0 then raise (Invalid_argument "for_all_before: n < 0")
  else
    let rec for_all_before n l =
      if n = 0 then true
      else
        match l with
        | [] -> raise (Invalid_argument "for_all_before: n > length l")
        | x :: xs -> p x && for_all_before (n - 1) xs
    in
    for_all_before n l

(** [for_all_before2 n p l1 l2] returns [true] iff for every value
    [v1] in [l1] and a value [v2] in [l2] both located at some index
    [i] such that [i < n], the condition [p v1 v2 = true] holds.
    @raise [Invalid_argument] when the condition [0 <= n] is not
      satisfied or when [n <= min (length l1) (length l2)] is not
      satisfied whenever every element in [l1] or in [l2] satisfies
      [p] *)
let for_all_before2 n p l1 l2 =
  if n < 0 then raise (Invalid_argument "for_all_before2: n < 0")
  else
    let rec for_all_before2 n l1 l2 =
      if n = 0 then true
      else
        match (l1, l2) with
        | [], _ -> raise (Invalid_argument "for_all_before2: n > length l1")
        | _, [] -> raise (Invalid_argument "for_all_before2: n > length l1")
        | x1 :: xs1, x2 :: xs2 -> p x1 x2 && for_all_before2 (n - 1) xs1 xs2
    in
    for_all_before2 n l1 l2

(** [for_all_in_range start len p l] returns [true] iff for every
    value [v] in [l] located at some index [i] such that [start <= i <
    start + len] and such that [p v = true].
    @raise [Invalid_argument] when the condition [0 <= start <= length l]
      is not satisfied or when [start + len <= length l] is not
      satisfied whenever every element in [l] satisfies [p] *)
let for_all_in_range start len p l = for_all_before len p (drop start l)

(** [for_all_in_range2 start len p l1 l2] returns [true] iff for
    every value [v1] in [l1] and a value [v2] in [l2] both located
    at some index [i] such that [start <= i < start + len], the condition
    [p v1 v2 = true] holds.
    @raise [Invalid_argument] when the condition
      [0 <= start <= min (length l1) (length l2)] is not
      satisfied or when [start + len <= min (length l1) (length l2)] is not
      satisfied whenever every element in [l1] or in [l2] satisfies
      [p] *)
let for_all_in_range2 start len p l1 l2 =
  let l1, l2 = drop2 start l1 l2 in
  for_all_before2 len p l1 l2
