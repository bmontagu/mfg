module Make (X : sig
  type t

  val pp : Format.formatter -> t -> unit
  val join : t -> t -> t
  val widen : int -> t -> t -> t
  val leq : t -> t -> bool
end) (Y : sig
  type t

  val pp : Format.formatter -> t -> unit
  val join : t -> t -> t
  val widen : int -> t -> t -> t
  val leq : t -> t -> bool
end) : sig
  type t

  val bot : t
  val leq : t -> t -> bool
  val singleton : X.t -> Y.t -> t
  val join : t -> t -> t
  val widen : int -> t -> t -> t
  val find_opt : X.t -> t -> Y.t option
  val map_reduce : (X.t -> Y.t -> 'a) -> ('a -> 'a -> 'a) -> 'a -> t -> 'a
  val pp : Format.formatter -> t -> unit
end
