module Make
    (MapMake : functor
      (X : sig
         type t

         val compare : t -> t -> int
         val pp : Format.formatter -> t -> unit
       end)
      -> sig
  type key = X.t
  type 'a t

  val empty : 'a t
  val is_empty : 'a t -> bool
  val singleton : key -> 'a -> 'a t
  val add : key -> 'a -> 'a t -> 'a t
  val fold : (key -> 'a -> 'b -> 'b) -> 'a t -> 'b -> 'b
  val for_all : (key -> 'a -> bool) -> 'a t -> bool
  val exists : (key -> 'a -> bool) -> 'a t -> bool
  val pp : (Format.formatter -> 'a -> unit) -> Format.formatter -> 'a t -> unit
end) (X : sig
  type t

  val compare : t -> t -> int
  val pp : Format.formatter -> t -> unit
  val is_bot : t -> bool
  val leq : t -> t -> bool
  val join : t -> t -> t
  val widen : int -> t -> t -> t
end) (Y : sig
  type t

  val bot : t
  val leq : t -> t -> bool
  val join : t -> t -> t
  val widen : int -> t -> t -> t
  val pp : Format.formatter -> t -> unit
end) : sig
  type t

  val bot : t
  val leq : t -> t -> bool
  val singleton : X.t -> Y.t -> t
  val join : t -> t -> t
  val widen : int -> t -> t -> t
  val find_opt : X.t -> t -> Y.t option
  val map_reduce : (X.t -> Y.t -> 'a) -> ('a -> 'a -> 'a) -> 'a -> t -> 'a
  val pp : Format.formatter -> t -> unit
end = struct
  module XMap = MapMake (X)

  (* Maps whose keys are elements of [X.t], whose domain is
     non-redundant and does not contain the bottom element *)
  type t = Y.t XMap.t

  let bot = XMap.empty

  let leq m1 m2 =
    XMap.for_all
      (fun x1 y1 -> XMap.exists (fun x2 y2 -> X.leq x1 x2 && Y.leq y1 y2) m2)
      m1

  let singleton x y = if X.is_bot x then bot else XMap.singleton x y

  (* Precondition: [m] has a non-redondant domain.
     Postcondition: the output also has a non-redundant domain.
     The output is of the form [Some _]
     iff there was some [x'] in the domain of [m] such that [x <= x'].
  *)
  let add_below y_join x y m =
    if X.is_bot x then Some m
    else
      let found, m' =
        XMap.fold
          (fun x' y' (found, m_acc) ->
            if X.leq x x' then (true, XMap.add x' (y_join y' y) m_acc)
            else (found, XMap.add x' y' m_acc))
          m (false, bot)
      in
      if found then Some m' else None

  (* Precondition: [m] has a non-redondant domain.
     Postcondition: the output also has a non-redundant domain.
     The output is of the form [Some _]
     iff there was some [x'] in the domain of [m] such that [x' <= x].
  *)
  let add_above y_join x y m =
    let found, m', y'' =
      XMap.fold
        (fun x' y' (found, m_acc, y_acc) ->
          if X.leq x' x then (true, m_acc, Y.join y_acc y')
          else (found, XMap.add x' y' m_acc, y_acc))
        m (false, bot, Y.bot)
    in
    if found then Some (XMap.add x (y_join y'' y) m') else None

  (* Precondition: [m] has a non-redondant domain.
     Postcondition: the output also has a non-redundant domain. *)
  let add y_join x y m =
    (* try to insert from below first *)
    match add_below y_join x y m with
    | Some m' -> m'
    | None -> (
        (* if it failed, try to insert from above *)
        match add_above y_join x y m with
        | Some m' -> m'
        | None ->
            (* otherwise, just add the new entry*)
            XMap.add x y m)

  let join m1 m2 = XMap.fold (add Y.join) m2 m1

  let add_widen_collapse n x y m =
    let ox', y' =
      XMap.fold
        (fun x' y' (xacc, yacc) ->
          let xacc =
            match xacc with
            | None -> Some x'
            | Some xacc -> Some (X.join xacc x')
          and yacc = Y.join yacc y' in
          (xacc, yacc))
        m (None, Y.bot)
    in
    let x' = match ox' with None -> x | Some x' -> X.widen n x' x in
    singleton x' (Y.widen n y' y)

  let add_widen n x y m =
    if X.is_bot x then m
    else if XMap.is_empty m then singleton x y
    else
      match add_below (Y.widen n) x y m with
      | Some m' -> m'
      | None -> add_widen_collapse n x y m

  let widen0 n m1 m2 = XMap.fold (add_widen n) m2 m1

  let widen n m1 m2 =
    if n <= 2 then join m1 m2
    else if n <= 3 then widen0 n m1 m2
    else if n <= 6 then join m1 m2
    else widen0 n m1 m2

  let map_reduce map reduce init m =
    XMap.fold (fun x y acc -> reduce (map x y) acc) m init

  let find_opt x m =
    map_reduce
      (fun x' y' -> if X.leq x x' then Some y' else None)
      (fun o1 o2 ->
        match (o1, o2) with
        | None, None -> None
        | None, (Some _ as o) | (Some _ as o), None -> o
        | Some v1, Some v2 -> Some (Y.join v1 v2))
      None m

  let pp fmt m = XMap.pp Y.pp fmt m
end
