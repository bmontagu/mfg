module Make (X : sig
  type t

  val pp : Format.formatter -> t -> unit
  val join : t -> t -> t
  val widen : int -> t -> t -> t
  val leq : t -> t -> bool
end) (Y : sig
  type t

  val pp : Format.formatter -> t -> unit
  val join : t -> t -> t
  val widen : int -> t -> t -> t
  val leq : t -> t -> bool
end) : sig
  type t

  val bot : t
  val leq : t -> t -> bool
  val singleton : X.t -> Y.t -> t
  val join : t -> t -> t
  val widen : int -> t -> t -> t
  val find_opt : X.t -> t -> Y.t option
  val map_reduce : (X.t -> Y.t -> 'a) -> ('a -> 'a -> 'a) -> 'a -> t -> 'a
  val pp : Format.formatter -> t -> unit
end = struct
  type t = Bot | G of X.t * Y.t

  let bot = Bot

  let join_gen x_join y_join og1 og2 =
    match (og1, og2) with
    | Bot, g | g, Bot -> g
    | G (x1, y1), G (x2, y2) ->
        let x12 = x_join x1 x2 and y12 = y_join y1 y2 in
        G (x12, y12)

  let join og1 og2 = join_gen X.join Y.join og1 og2
  let widen n og1 og2 = join_gen (X.widen n) (Y.widen n) og1 og2

  let leq og1 og2 =
    match (og1, og2) with
    | Bot, _ -> true
    | _, Bot -> false
    | G (x1, y1), G (x2, y2) -> X.leq x1 x2 && Y.leq y1 y2

  let singleton x y = G (x, y)

  let find_opt x = function
    | Bot -> None
    | G (x', y) -> if X.leq x x' then Some y else None

  let to_seq = function
    | Bot -> Seq.empty
    | G (x, y) -> Seq.cons (x, y) Seq.empty

  let map_reduce map reduce init = function
    | Bot -> init
    | G (x, y) -> reduce (map x y) init

  let pp fmt graph =
    let open Format in
    fprintf fmt "@[<v>%a@]"
      (pp_print_seq ~pp_sep:pp_print_space (fun fmt (vs, v) ->
           fprintf fmt "@[%a ->@ %a@]" X.pp vs Y.pp v))
      (to_seq graph)
end
