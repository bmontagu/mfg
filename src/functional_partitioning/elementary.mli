module Make
    (_ : functor
      (X : sig
         type t

         val compare : t -> t -> int
         val pp : Format.formatter -> t -> unit
       end)
      -> sig
  type key = X.t
  type 'a t

  val empty : 'a t
  val is_empty : 'a t -> bool
  val singleton : key -> 'a -> 'a t
  val add : key -> 'a -> 'a t -> 'a t
  val fold : (key -> 'a -> 'b -> 'b) -> 'a t -> 'b -> 'b
  val for_all : (key -> 'a -> bool) -> 'a t -> bool
  val exists : (key -> 'a -> bool) -> 'a t -> bool
  val pp : (Format.formatter -> 'a -> unit) -> Format.formatter -> 'a t -> unit
end) (X : sig
  type t

  val compare : t -> t -> int
  val pp : Format.formatter -> t -> unit
  val is_bot : t -> bool
  val leq : t -> t -> bool
  val join : t -> t -> t
  val widen : int -> t -> t -> t
end) (Y : sig
  type t

  val bot : t
  val leq : t -> t -> bool
  val join : t -> t -> t
  val widen : int -> t -> t -> t
  val pp : Format.formatter -> t -> unit
end) : sig
  type t

  val bot : t
  val leq : t -> t -> bool
  val singleton : X.t -> Y.t -> t
  val join : t -> t -> t
  val widen : int -> t -> t -> t
  val find_opt : X.t -> t -> Y.t option
  val map_reduce : (X.t -> Y.t -> 'a) -> ('a -> 'a -> 'a) -> 'a -> t -> 'a
  val pp : Format.formatter -> t -> unit
end
