(**
  Author: Benoît Montagu <benoit.montagu@inria.fr>
  Copyright Inria © 2022
*)

module IntBar = struct
  type t =
    | Neg (* { n | n <= 0 } *)
    | Int of int (* {n} *)
    | Pos (* { n | n >= 0 } *)
  [@@deriving ord]

  let pp fmt =
    let open Format in
    function
    | Neg -> pp_print_string fmt "-∞"
    | Pos -> pp_print_string fmt "+∞"
    | Int i -> pp_print_int fmt i

  let leq i1 i2 =
    match (i1, i2) with
    | Neg, _ | _, Pos -> true
    | Int n1, Int n2 -> n1 <= n2
    | _ -> false

  let min i1 i2 =
    match (i1, i2) with
    | Pos, i | i, Pos -> i
    | Neg, _ | _, Neg -> Neg
    | Int n1, Int n2 -> Int (Int.min n1 n2)

  let max i1 i2 =
    match (i1, i2) with
    | Neg, i | i, Neg -> i
    | Pos, _ | _, Pos -> Pos
    | Int n1, Int n2 -> Int (Int.max n1 n2)

  let plus i1 i2 =
    match (i1, i2) with
    | Neg, Pos | Pos, Neg -> None
    | Int n1, Int n2 -> Some (Int (n1 + n2))
    | Pos, _ | _, Pos -> Some Pos
    | Neg, _ | _, Neg -> Some Neg

  let minus i1 i2 =
    match (i1, i2) with
    | Neg, Neg | Pos, Pos -> None
    | Int n1, Int n2 -> Some (Int (n1 - n2))
    | Pos, _ | _, Neg -> Some Pos
    | Neg, _ | _, Pos -> Some Neg

  let mult i1 i2 =
    match (i1, i2) with
    | (Int 0 as z), _ | _, (Int 0 as z) -> z
    | Int n, Neg | Neg, Int n -> if n >= 0 then Neg else Pos
    | Int n, Pos | Pos, Int n -> if n >= 0 then Pos else Neg
    | Int n1, Int n2 -> Int (n1 * n2)
    | Neg, Neg | Pos, Pos -> Pos
    | Neg, Pos | Pos, Neg -> Neg
end

type t = IntBar.t * IntBar.t [@@deriving ord]

let pp fmt (l, u) =
  match (l, u) with
  | (IntBar.Neg | Int _), (IntBar.Int _ | Pos) ->
      Format.fprintf fmt "[%a,%a]" IntBar.pp l IntBar.pp u
  | Pos, Neg -> Format.pp_print_string fmt "∅"
  | (Neg | Int _), Neg | Pos, (Int _ | Pos) -> assert false

let bot : t = (Pos, Neg)
let is_bot : t -> bool = function Pos, Neg -> true | _ -> false
let top : t = (Neg, Pos)

let inject i : t =
  let j = IntBar.Int i in
  (j, j)

let leq ((l1, u1) as i1) (l2, u2) =
  is_bot i1 || (IntBar.leq l2 l1 && IntBar.leq u1 u2)

let join ((l1, u1) as i1) ((l2, u2) as i2) =
  if is_bot i1 then i2
  else if is_bot i2 then i1
  else (IntBar.min l1 l2, IntBar.max u1 u2)

let widen ((l1, u1) as i1) ((l2, u2) as i2) =
  if is_bot i1 then i2
  else if is_bot i2 then i1
  else
    let open IntBar in
    let l = if leq l1 l2 then l1 else Neg
    and u = if leq u2 u1 then u1 else Pos in
    (l, u)

let meet ((l1, u1) as i1) ((l2, u2) as i2) =
  if is_bot i1 || is_bot i2 then bot
  else
    let l = IntBar.max l1 l2 and u = IntBar.min u1 u2 in
    match (l, u) with
    | Neg, Neg | Pos, Pos | Pos, Neg | Pos, Int _ | Int _, Neg -> bot
    | (Neg, Pos | Neg, Int _ | Int _, Pos) as p -> p
    | (Int n1, Int n2) as p -> if n1 <= n2 then p else bot

let plus (l1, u1) (l2, u2) =
  let open IntBar in
  match (plus l1 l2, plus u1 u2) with
  | None, _ | _, None -> top
  | Some l, Some u -> (l, u)

let minus (l1, u1) (l2, u2) =
  let open IntBar in
  match (minus l1 l2, minus u1 u2) with
  | None, _ | _, None -> top
  | Some l, Some u -> (l, u)

let mult (l1, u1) (l2, u2) =
  let open IntBar in
  let l1l2 = mult l1 l2
  and u1l2 = mult u1 l2
  and l1u2 = mult l1 u2
  and u1u2 = mult u1 u2 in
  (min (min l1l2 l1u2) (min u1l2 u1u2), max (max l1l2 l1u2) (max u1l2 u1u2))

let le (l1, u1) (l2, u2) =
  let open IntBar in
  if leq u1 l2 then Some true
  else if leq u2 l1 && (not @@ leq l1 u2) then Some false
  else None

let eq i1 i2 =
  match le i1 i2 with
  | Some true -> le i2 i1
  | (Some false | None) as res -> res

let lt i1 i2 = Option.map not @@ le i2 i1
let ge i1 i2 = le i2 i1
let gt i1 i2 = lt i2 i1
