(**
  Author: Benoît Montagu <benoit.montagu@inria.fr>
  Copyright Inria © 2022
*)

(** This modules implements intervals of integers. Warning: it is not
   safe with respect to overflows. *)

type t

val compare : t -> t -> int
val pp : Format.formatter -> t -> unit
val bot : t
val is_bot : t -> bool
val top : t
val inject : int -> t
val leq : t -> t -> bool
val join : t -> t -> t
val widen : t -> t -> t
val meet : t -> t -> t
val plus : t -> t -> t
val minus : t -> t -> t
val mult : t -> t -> t
val eq : t -> t -> bool option
val le : t -> t -> bool option
val lt : t -> t -> bool option
val ge : t -> t -> bool option
val gt : t -> t -> bool option
