(**
  Author: Benoît Montagu <benoit.montagu@inria.fr>
  Copyright Inria © 2022
*)

open Sigs

type abs_bool = NoBool | Bool of bool | AnyBool

val abs_bool_join : abs_bool -> abs_bool -> abs_bool

type prim =
  | Bool of bool (* arity 0 *)
  | Int of int (* arity 0 *)
  | Plus (* arity 2 *)
  | Minus (* arity 2 *)
  | Mult (* arity 2 *)
  | Eq (* arity 2 *)
  | Lt (* arity 2 *)
  | Le (* arity 2 *)
  | Gt (* arity 2 *)
  | Ge (* arity 2 *)

val list_join : ('a -> 'a -> 'a) -> 'a list -> 'a list -> 'a list

module ListOf (X : sig
  include ORDERED_TYPE
  include PP_TYPE with type t := t
end) : sig
  include ORDERED_TYPE with type t = X.t list
  include PP_TYPE with type t := t
end
