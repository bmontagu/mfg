(**
  Author: Benoît Montagu <benoit.montagu@inria.fr>
  Copyright Inria © 2022
*)

open Sigs
open Common
open Homfg

module Args (V : sig
  include ORDERED_TYPE
  include PP_TYPE with type t := t
end) (G : sig
  type t

  val bot : t
  val join : t -> t -> t
  val singleton : V.t list -> V.t -> t
end) =
struct
  module VS = ListOf (V)
  module S = Set.Make (VS)

  type t = S.t

  let singleton = S.singleton
  let join = S.union

  let map f args =
    S.fold (fun arg acc -> G.join (G.singleton arg (f arg)) acc) args G.bot
end

module AbstractGraph
    (MakeGraph : functor
      (X : sig
         type t

         val compare : t -> t -> int
         val pp : Format.formatter -> t -> unit
         val is_bot : t -> bool
         val leq : t -> t -> bool
         val join : t -> t -> t
         val widen : int -> t -> t -> t
         val meet : t -> t -> t
       end)
      (Y : sig
         type t

         val bot : t
         val leq : t -> t -> bool
         val join : t -> t -> t
         val widen : int -> t -> t -> t
         val pp : Format.formatter -> t -> unit
       end)
      ->
  HoCommon.GRAPH with type x := X.t and type y := Y.t) (X : sig
  include ORDERED_TYPE
  include PP_TYPE with type t := t

  val is_bot : t -> bool
  val leq : t -> t -> bool
  val join : t -> t -> t
  val widen : int -> t -> t -> t
  val meet : t -> t -> t
end) (Y : sig
  include PP_TYPE

  val bot : t
  val join : t -> t -> t
  val widen : int -> t -> t -> t
  val leq : t -> t -> bool
end) : HoCommon.GRAPH with type x := X.t list and type y := Y.t = struct
  module XS = struct
    include ListOf (X)

    let bot = []
    let is_bot = ExtList.is_empty

    let is_semantic_bot xs =
      ExtList.is_empty xs
      ||
      let n = ExtList.count_first (Fun.negate X.is_bot) xs in
      (n = 0 && (not @@ List.for_all X.is_bot xs))
      || (not @@ List.for_all X.is_bot (ExtList.drop n xs))

    let len xs =
      let nb_bot = ExtList.count_first (Fun.negate X.is_bot) xs in
      if nb_bot = 0 then List.length xs else nb_bot

    let leq xs1 xs2 =
      is_semantic_bot xs1
      ||
      let n1 = len xs1 and n2 = len xs2 in
      n1 < n2 || (n1 = n2 && ExtList.for_all_before2 n1 X.leq xs1 xs2)

    let join_gen join xs1 xs2 =
      if is_bot xs1 then xs2
      else if is_bot xs2 then xs1
      else list_join join xs1 xs2

    let join xs1 xs2 = join_gen X.join xs1 xs2
    let widen n xs1 xs2 = join_gen (X.widen n) xs1 xs2

    let meet xs1 xs2 =
      if is_semantic_bot xs1 || is_semantic_bot xs2 || len xs1 <> len xs2 then
        bot
      else if List.for_all X.is_bot xs1 then xs2
      else if List.for_all X.is_bot xs2 then xs1
      else
        let xs12 = List.map2 X.meet xs1 xs2 in
        if is_semantic_bot xs12 || ExtList.exists_before (len xs1) X.is_bot xs12
        then bot
        else xs12
  end

  include
    HoCommon.AbstractGraph (XS) (Y) (MakeGraph (XS) (Y))
      (struct
        let partial_applications_in_graph_domain = false
      end)
end

module AbstractArgs (V : sig
  include ORDERED_TYPE
  include PP_TYPE with type t := t

  val join : t -> t -> t
end) (G : sig
  type t

  val bot : t
  val singleton : V.t list -> V.t -> t
end) =
struct
  module VS = ListOf (V)

  type t = VS.t option

  let singleton x = Some x

  let join ol1 ol2 =
    match (ol1, ol2) with
    | None, ol | ol, None -> ol
    | Some l1, Some l2 ->
        assert (List.length l1 = List.length l2);
        Some (list_join V.join l1 l2)

  let map f = function None -> G.bot | Some args -> G.singleton args (f args)
end

module MakeAbstract (V : sig
  include ORDERED_TYPE
  include PP_TYPE with type t := t

  val as_bool : t -> abs_bool
  val bot : t
  val is_bot : t -> bool
  val join : t -> t -> t

  val app_map_reduce :
    map:(HoCommon.F.t -> int -> t list -> 'a) ->
    join:('a -> 'a -> 'a) ->
    bot:'a ->
    top:'a ->
    t ->
    'a

  val inject_val : HoCommon.Concrete.V.t -> t
end)
(G : HoCommon.GRAPH with type x := V.t list and type y := V.t)
(MakeI : functor
  (G : HoCommon.GRAPH with type x := V.t list and type y := V.t)
  ->
  HoCommon.INTERP with type v := V.t and type phi = G.t HoCommon.FMap.t option) : sig
  val run :
    delay_widening:int ->
    (string, prim, string) HoCommon.program ->
    (string * HoCommon.Concrete.V.t list) list ->
    unit
end = struct
  module C = AbstractArgs (V) (G)
  module I = MakeI (G)

  module MFG =
    Make (HoCommon.X) (HoCommon.A) (HoCommon.F) (HoCommon.FMap) (V) (G) (C) (I)

  let convert_c l : MFG.D.c =
    List.fold_left
      (fun acc (f, args) ->
        match C.singleton ((List.map V.inject_val) args) with
        | None -> acc
        | Some vs -> MFG.D.C.(join (singleton f vs) acc))
      MFG.D.C.bot l

  let run ~delay_widening prog init =
    let phi = MFG.program prog ~delay_widening (convert_c init) in
    Format.printf "%a@." MFG.D.Phi.pp phi
end

module Concrete = struct
  open HoCommon.Concrete
  module C = Args (V) (G)

  module I = MakeI (struct
    let partial_applications_in_graph_domain = false
  end)

  module MFG =
    Make (HoCommon.X) (HoCommon.A) (HoCommon.F) (HoCommon.FMap) (V) (G) (C) (I)

  let convert_c l : MFG.D.c =
    List.fold_left
      (fun acc (f, args) ->
        MFG.D.C.(join (Some (HoCommon.FMap.singleton f (C.singleton args))) acc))
      MFG.D.C.bot l

  let run ~delay_widening prog init =
    let phi = MFG.program prog ~delay_widening (convert_c init) in
    Format.printf "%a@." MFG.D.Phi.pp phi
end
