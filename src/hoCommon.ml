(**
  Author: Benoît Montagu <benoit.montagu@inria.fr>
  Copyright Inria © 2022
*)

open Sigs
open Common

type ('x, 'a, 'f) expr =
  | Var of 'x
  | Fun of 'f
  | Cons of 'a * ('x, 'a, 'f) expr list
  | Call of ('x, 'a, 'f) expr * ('x, 'a, 'f) expr
  | IfThenElse of ('x, 'a, 'f) expr * ('x, 'a, 'f) expr * ('x, 'a, 'f) expr

type ('x, 'a, 'f) decl = 'f * 'x list * ('x, 'a, 'f) expr
type ('x, 'a, 'f) program = ('x, 'a, 'f) decl list

module type GRAPH = sig
  type x
  type y
  type t

  val bot : t
  val join : t -> t -> t
  val widen : int -> t -> t -> t
  val leq : t -> t -> bool
  val singleton : x -> y -> t
  val find_opt : x -> t -> y option
  val map_reduce : (x -> y -> 'a) -> ('a -> 'a -> 'a) -> 'a -> t -> 'a

  include PP_TYPE with type t := t
end

module Graph (X : sig
  include ORDERED_TYPE
  include PP_TYPE with type t := t
end) (Y : sig
  type t

  val join : t -> t -> t
  val widen : int -> t -> t -> t
  val leq : t -> t -> bool

  include PP_TYPE with type t := t
end) : GRAPH with type x := X.t list and type y := Y.t = struct
  module XS = ListOf (X)
  module M = Map.Make (XS)

  type t = Y.t M.t

  let bot = M.empty
  let join_gen join g1 g2 = M.union (fun _args v1 v2 -> Some (join v1 v2)) g1 g2
  let join g1 g2 = join_gen Y.join g1 g2
  let widen n g1 g2 = join_gen (Y.widen n) g1 g2

  let leq g1 g2 =
    M.for_all
      (fun vs v1 ->
        match M.find_opt vs g2 with None -> false | Some v2 -> Y.leq v1 v2)
      g1

  let singleton = M.singleton
  let find_opt = M.find_opt
  let to_seq = M.to_seq

  let map_reduce map reduce init g =
    M.fold (fun x y acc -> reduce (map x y) acc) g init

  let pp fmt graph =
    let open Format in
    fprintf fmt "@[<v>%a@]"
      (pp_print_seq ~pp_sep:pp_print_space (fun fmt (vs, v) ->
           fprintf fmt "@[%a ->@ %a@]" XS.pp vs Y.pp v))
      (to_seq graph)
end

module AbstractGraph (XS : sig
  type t

  include ORDERED_TYPE with type t := t
  include PP_TYPE with type t := t

  val len : t -> int
  val bot : t
  val is_bot : t -> bool
  val join : t -> t -> t
end) (Y : sig
  include PP_TYPE

  val bot : t
end)
(G : GRAPH with type x := XS.t and type y := Y.t) (Options : sig
  val partial_applications_in_graph_domain : bool
end) : GRAPH with type x := XS.t and type y := Y.t = struct
  module IMap = Map.Make (Int)

  type t = G.t IMap.t

  let bot = IMap.empty
  let g_dom (g : G.t) : XS.t = G.map_reduce (fun x _y -> x) XS.join XS.bot g

  let join_gen g_join g1 g2 =
    let g12 =
      IMap.merge
        (fun i o1 o2 ->
          match (o1, o2) with
          | None, None -> assert false
          | (Some g as o), None | None, (Some g as o) ->
              assert (i >= 0);
              if Options.partial_applications_in_graph_domain then
                if i <= 1 then o
                else
                  match
                    (IMap.find_opt (i - 1) g1, IMap.find_opt (i - 1) g2)
                  with
                  | None, None -> assert false
                  | Some g', None | None, Some g' ->
                      let g'' =
                        G.map_reduce
                          (fun x' _ ->
                            G.map_reduce
                              (fun x y -> G.singleton (XS.join x x') y)
                              g_join G.bot g)
                          g_join G.bot g'
                      in
                      Some (g_join g'' g)
                  | Some g'1, Some g'2 ->
                      let g'' =
                        G.map_reduce
                          (fun x' _ ->
                            G.map_reduce
                              (fun x y -> G.singleton (XS.join x' x) y)
                              g_join G.bot g)
                          g_join G.bot (G.join g'1 g'2)
                      in
                      Some (g_join g'' g)
              else o
          | Some g1, Some g2 -> Some (g_join g1 g2))
        g1 g2
    in
    if Options.partial_applications_in_graph_domain then g12
    else
      IMap.mapi
        (fun i gi ->
          let g' =
            IMap.fold
              (fun j gj gacc ->
                if j < i then g_join gacc (G.singleton (g_dom gj) Y.bot)
                else gacc)
              g12 G.bot
          in
          G.join g' gi)
        g12

  let join g1 g2 = join_gen G.join g1 g2
  let widen n g1 g2 = join_gen (G.widen n) g1 g2

  let leq gs1 gs2 =
    IMap.for_all
      (fun i g1 ->
        match IMap.find_opt i gs2 with None -> false | Some g2 -> G.leq g1 g2)
      gs1

  let singleton x y =
    if XS.is_bot x then IMap.empty
    else IMap.singleton (XS.len x) (G.singleton x y)

  let find_opt x gs =
    match IMap.find_opt (XS.len x) gs with
    | None -> None
    | Some g -> G.find_opt x g

  let map_reduce map reduce init gs =
    IMap.fold (fun _i g acc -> G.map_reduce map reduce acc g) gs init

  let pp fmt graph =
    let open Format in
    if IMap.is_empty graph then pp_print_string fmt "⊥"
    else
      fprintf fmt "@[<v>%a@]"
        (pp_print_seq ~pp_sep:pp_print_space (fun fmt (_i, g) ->
             fprintf fmt "@[%a@]" G.pp g))
        (IMap.to_seq graph)
end

module X = String

module A = struct
  type t = prim
end

module F = String

module FMap = struct
  include Map.Make (F)

  let pp pp_elt fmt m =
    let open Format in
    fprintf fmt "@[<v>%a@]"
      (pp_print_seq ~pp_sep:pp_print_space (fun fmt (f, elt) ->
           fprintf fmt "@[<hv 2>%s:@ {@[%a@]}@]" f pp_elt elt))
      (to_seq m)
end

module type VALUE = sig
  type t

  val compare : t -> t -> int
  val bot : t
  val is_bot : t -> bool
  val join : t -> t -> t
  val widen : int -> t -> t -> t
  val meet : t -> t -> t
  val leq : t -> t -> bool
  val as_bool : t -> Common.abs_bool

  val app_map_reduce :
    map:(string -> int -> t list -> 'a) ->
    join:('a -> 'a -> 'a) ->
    bot:'a ->
    top:'a ->
    t ->
    'a

  val pp : Format.formatter -> t -> unit
end

module type INTERP = sig
  type v
  type phi

  val fun_ : F.t -> int -> v list -> v
  val basic : A.t -> v list -> v
  val apply : phi -> v -> v -> v
end

module Concrete = struct
  module V = struct
    type t =
      | Bot
      | Top
      | Int of int
      | Bool of bool
      | Clos of string * int * t list
    [@@deriving ord]

    let top = Top

    let rec pp fmt =
      let open Format in
      function
      | Bot -> pp_print_string fmt "⊥"
      | Top -> pp_print_string fmt "⊤"
      | Int n -> pp_print_int fmt n
      | Bool b -> pp_print_bool fmt b
      | Clos (f, _n, vs) ->
          fprintf fmt "%s [@[%a]@]" f
            (pp_print_list ~pp_sep:(fun fmt () -> fprintf fmt ",@ ") pp)
            vs

    let bot = Bot
    let is_bot = function Bot -> true | _ -> false

    let as_bool = function
      | Bot | Int _ | Clos _ -> NoBool
      | Bool b -> Bool b
      | Top -> AnyBool

    let rec leq v1 v2 =
      match (v1, v2) with
      | Bot, _ | _, Top -> true
      | Int n1, Int n2 -> n1 = n2
      | Bool b1, Bool b2 -> b1 = b2
      | Clos (f1, n1, l1), Clos (f2, n2, l2) ->
          F.equal f1 f2 && n1 = n2
          && List.length l1 = List.length l2
          && List.for_all2 leq l1 l2
      | _ -> false

    let rec equal v1 v2 =
      match (v1, v2) with
      | Bot, Bot | Top, Top -> true
      | Int n1, Int n2 -> n1 = n2
      | Bool b1, Bool b2 -> b1 = b2
      | Clos (f1, n1, l1), Clos (f2, n2, l2) ->
          F.equal f1 f2 && n1 = n2
          && List.length l1 = List.length l2
          && List.for_all2 equal l1 l2
      | _ -> false

    let rec join v1 v2 =
      match (v1, v2) with
      | Bot, v | v, Bot | (Top as v), _ | _, (Top as v) -> v
      | Int n1, Int n2 -> if n1 = n2 then v1 else Top
      | Bool b1, Bool b2 -> if b1 = b2 then v1 else Top
      | Clos (f1, n1, l1), Clos (f2, n2, l2) ->
          if F.equal f1 f2 && n1 = n2 then
            let l12 = list_join join l1 l2 in
            Clos (f1, n1, l12)
          else Top
      | Int _, Bool _
      | Bool _, Int _
      | Int _, Clos _
      | Clos _, Int _
      | Bool _, Clos _
      | Clos _, Bool _ ->
          Top

    let widen _n v1 v2 = join v1 v2

    let app_map_reduce ~map ~join ~bot ~top = function
      | Bot | Int _ | Bool _ -> bot
      | Top -> top
      | Clos (f, n, l) -> join (map f n l) bot

    let meet v1 v2 =
      match (v1, v2) with
      | Top, v | v, Top -> v
      | Bot, _ | _, Bot -> bot
      | Int n1, Int n2 -> if n1 = n2 then v1 else bot
      | Bool b1, Bool b2 -> if b1 = b2 then v1 else bot
      | Clos (f1, n1, l1), Clos (f2, n2, l2) ->
          if F.equal f1 f2 && n1 = n2 && List.for_all2 equal l1 l2 then v1
          else bot
      | Int _, (Bool _ | Clos _)
      | (Bool _ | Clos _), Int _
      | Bool _, Clos _
      | Clos _, Bool _ ->
          bot
  end

  module G = Graph (V) (V)

  module MakeI (Options : sig
    val partial_applications_in_graph_domain : bool
  end) =
  struct
    let int_op2_int f v1 v2 =
      match (v1, v2) with
      | V.Bot, _ | _, V.Bot -> V.Bot
      | V.Top, _ | _, V.Top -> Top
      | V.Bool _, _ | _, V.Bool _ -> Bot
      | V.Clos _, _ | _, V.Clos _ -> Bot
      | V.Int n1, V.Int n2 -> Int (f n1 n2)

    let int_op2_bool f v1 v2 =
      match (v1, v2) with
      | V.Bot, _ | _, V.Bot -> V.Bot
      | V.Top, _ | _, V.Top -> Top
      | V.Bool _, _ | _, V.Bool _ -> Bot
      | V.Clos _, _ | _, V.Clos _ -> Bot
      | V.Int n1, V.Int n2 -> Bool (f n1 n2)

    let fun_ f n vs = V.Clos (f, n, vs)

    let basic a vs =
      match (a, vs) with
      | Common.Bool b, [] -> V.Bool b
      | Bool _, _ -> V.bot
      | Int n, [] -> V.Int n
      | Int _, _ -> V.bot
      | Plus, [ v1; v2 ] -> int_op2_int ( + ) v1 v2
      | Plus, _ -> V.bot
      | Minus, [ v1; v2 ] -> int_op2_int ( - ) v1 v2
      | Minus, _ -> V.bot
      | Mult, [ v1; v2 ] -> int_op2_int ( * ) v1 v2
      | Mult, _ -> V.bot
      | Eq, [ v1; v2 ] -> int_op2_bool ( = ) v1 v2
      | Eq, _ -> V.bot
      | Lt, [ v1; v2 ] -> int_op2_bool ( < ) v1 v2
      | Lt, _ -> V.bot
      | Le, [ v1; v2 ] -> int_op2_bool ( <= ) v1 v2
      | Le, _ -> V.bot
      | Gt, [ v1; v2 ] -> int_op2_bool ( > ) v1 v2
      | Gt, _ -> V.bot
      | Ge, [ v1; v2 ] -> int_op2_bool ( >= ) v1 v2
      | Ge, _ -> V.bot

    type phi = G.t FMap.t option

    let get_phi phi f args =
      match FMap.find_opt f phi with
      | None -> V.bot
      | Some g -> ( match G.find_opt args g with None -> V.bot | Some v -> v)

    let apply ophi v1 v2 =
      if V.is_bot v2 then V.Bot
      else
        match v1 with
        | V.Top -> V.Top
        | Int _ | Bool _ | Bot -> Bot
        | Clos (f, n, l) -> (
            match ophi with
            | None -> V.top
            | Some phi ->
                (* Here we differ from the paper on higher-order
                   function graphs: we systematically read in the graph,
                   and never create a closure. The closure is created by
                   the evaluation function. *)
                let args =
                  if Options.partial_applications_in_graph_domain then
                    l @ [ v2 ]
                  else l @ [ v2 ] @ ExtList.repeat (n - 1 - List.length l) V.bot
                in
                get_phi phi f args)
  end
end

module AbstractGraphElementaryPartitioning (X : sig
  include ORDERED_TYPE
  include PP_TYPE with type t := t

  val is_bot : t -> bool
  val join : t -> t -> t
  val widen : int -> t -> t -> t
  val leq : t -> t -> bool
end) (Y : sig
  include PP_TYPE

  val bot : t
  val join : t -> t -> t
  val widen : int -> t -> t -> t
  val leq : t -> t -> bool
end) : GRAPH with type x := X.t list and type y := Y.t = struct
  module XS = struct
    type t = Bot | Top | Val of X.t list [@@deriving ord]

    let is_bot = function Bot -> true | _ -> false

    let leq v1 v2 =
      match (v1, v2) with
      | Bot, _ | _, Top -> true
      | Val l1, Val l2 -> ExtList.same_length l1 l2 && List.for_all2 X.leq l1 l2
      | Top, Bot | Val _, Bot | Top, Val _ -> false

    let join_gen join v1 v2 =
      match (v1, v2) with
      | Bot, v | v, Bot -> v
      | Top, _ | _, Top -> Top
      | Val l1, Val l2 ->
          if ExtList.same_length l1 l2 then Val (List.map2 join l1 l2) else Top

    let join v1 v2 = join_gen X.join v1 v2
    let widen n v1 v2 = join_gen (X.widen n) v1 v2

    let pp fmt =
      let open Format in
      function
      | Bot -> pp_print_string fmt "⊥"
      | Top -> pp_print_string fmt "⊤"
      | Val [ v ] -> X.pp fmt v
      | Val l ->
          fprintf fmt "@[(%a)@]"
            (pp_print_list ~pp_sep:(fun fmt () -> fprintf fmt ",@ ") X.pp)
            l
  end

  module P = Functional_partitioning.Elementary.Make (MapPP.Make) (XS) (Y)

  type t = P.t

  let pp = P.pp
  let bot = P.bot
  let leq = P.leq

  let singleton x y =
    if List.exists X.is_bot x then P.bot else P.singleton (Val x) y

  let join = P.join
  let widen = P.widen

  let map_reduce map reduce init m =
    P.map_reduce
      (fun x y ->
        match x with
        | Val l -> map l y
        | Bot -> assert false
        | Top -> assert false (* XXX *))
      reduce init m

  let find_opt x g =
    P.find_opt (if List.exists X.is_bot x then XS.Bot else Val x) g
end

module AbstractType = struct
  module V0 = struct
    type t = Bool | Int | Clos of F.t * int * int [@@deriving ord]

    let pp fmt =
      let open Format in
      function
      | Bool -> pp_print_string fmt "bool"
      | Int -> pp_print_string fmt "int"
      | Clos (f, _, m) ->
          fprintf fmt "%s [%a]" f
            (pp_print_list
               ~pp_sep:(fun fmt () -> pp_print_char fmt ',')
               (fun fmt () -> pp_print_char fmt '_'))
            (ExtList.repeat m ())

    let as_bool = function Int | Clos _ -> NoBool | Bool -> AnyBool
  end

  module V = struct
    module S = Set.Make (V0)

    type t = S.t option [@@deriving ord]

    let pp fmt =
      let open Format in
      function
      | None -> pp_print_string fmt "⊤"
      | Some s ->
          if S.is_empty s then pp_print_string fmt "⊥"
          else
            fprintf fmt "@[{%a@]}"
              (pp_print_seq ~pp_sep:(fun fmt () -> fprintf fmt ",@ ") V0.pp)
              (S.to_seq s)

    let bot = Some S.empty
    let top = None
    let is_bot = function None -> false | Some s -> S.is_empty s
    let inject v = Some (S.singleton v)

    let join o1 o2 =
      match (o1, o2) with
      | None, _ | _, None -> None
      | Some s1, Some s2 -> Some (S.union s1 s2)

    let widen _n o1 o2 = join o1 o2

    let leq o1 o2 =
      match (o1, o2) with
      | _, None -> true
      | None, Some _ -> false
      | Some s1, Some s2 -> S.subset s1 s2

    let as_bool = function
      | None -> AnyBool
      | Some s -> S.fold (fun v b -> abs_bool_join (V0.as_bool v) b) s NoBool

    let app_map_reduce ~map ~join ~bot:init ~top = function
      | None -> top
      | Some s ->
          S.fold
            (fun v acc ->
              match v with
              | Bool | Int -> acc
              | Clos (f, n, m) -> join (map f n (ExtList.repeat m bot)) acc)
              (* XXX: /!\ ExtList.repeat m bot *)
            s init

    let inject_val = function
      | Concrete.V.Bot -> bot
      | Top -> top
      | Int _ -> inject V0.Int
      | Bool _ -> inject V0.Bool
      | Clos (f, n, vs) -> inject (V0.Clos (f, n, List.length vs))

    let meet o1 o2 =
      match (o1, o2) with
      | None, o | o, None -> o
      | Some s1, Some s2 -> Some (S.inter s1 s2)
  end

  module MakeI (G : GRAPH with type x := V.t list and type y := V.t) = struct
    type phi = G.t FMap.t option

    let fun_ f n vs = V.inject (V0.Clos (f, n, List.length vs))

    let binop default f v1 v2 =
      if V.is_bot v1 || V.is_bot v2 then V.bot
      else
        match (v1, v2) with
        | None, _ | _, None -> default
        | Some vs1, Some vs2 ->
            V.S.fold
              (fun v1 acc ->
                V.S.fold (fun v2 acc -> V.join (f v1 v2) acc) vs2 acc)
              vs1 V.bot

    let int_op2_int =
      binop (V.inject V0.Int) (fun v1 v2 ->
          match (v1, v2) with
          | V0.Bool, _ | _, V0.Bool -> V.bot
          | Clos _, _ | _, Clos _ -> V.bot
          | Int, Int -> V.inject V0.Int)

    let int_op2_bool =
      binop (V.inject V0.Bool) (fun v1 v2 ->
          match (v1, v2) with
          | V0.Bool, _ | _, V0.Bool -> V.bot
          | Clos _, _ | _, Clos _ -> V.bot
          | Int, Int -> V.inject V0.Bool)

    let basic a vs =
      match (a, vs) with
      | Common.Bool _b, [] -> V.inject V0.Bool
      | Bool _, _ -> V.bot
      | Int _n, [] -> V.inject V0.Int
      | Int _, _ -> V.bot
      | Plus, [ v1; v2 ] -> int_op2_int v1 v2
      | Plus, _ -> V.bot
      | Minus, [ v1; v2 ] -> int_op2_int v1 v2
      | Minus, _ -> V.bot
      | Mult, [ v1; v2 ] -> int_op2_int v1 v2
      | Mult, _ -> V.bot
      | Eq, [ v1; v2 ] -> int_op2_bool v1 v2
      | Eq, _ -> V.bot
      | Lt, [ v1; v2 ] -> int_op2_bool v1 v2
      | Lt, _ -> V.bot
      | Le, [ v1; v2 ] -> int_op2_bool v1 v2
      | Le, _ -> V.bot
      | Gt, [ v1; v2 ] -> int_op2_bool v1 v2
      | Gt, _ -> V.bot
      | Ge, [ v1; v2 ] -> int_op2_bool v1 v2
      | Ge, _ -> V.bot

    let get_phi ophi f n =
      match ophi with
      | None -> V.top
      | Some phi -> (
          match FMap.find_opt f phi with
          | None -> V.bot
          | Some g -> (
              match G.find_opt (ExtList.repeat n V.bot) g with
              | Some v -> v
              | None -> V.bot))

    let apply ophi v1 v2 =
      if V.is_bot v2 then V.bot
      else
        match v1 with
        | None -> V.top
        | Some vs ->
            V.S.fold
              (fun v1 acc ->
                let out =
                  match v1 with
                  | Clos (f, n, m) ->
                      if m + 1 < n then V.inject (V0.Clos (f, n, m + 1))
                      else (
                        assert (m + 1 = n);
                        get_phi ophi f n)
                  | _ -> V.bot
                in
                V.join out acc)
              vs V.bot
  end
end

module AbstractConst = struct
  module V0 = struct
    type t = Bool of bool | Int of int option | Clos of F.t * int * int
    [@@deriving ord]

    let pp fmt =
      let open Format in
      function
      | Bool b -> pp_print_bool fmt b
      | Int None -> pp_print_string fmt "[-∞,+∞]"
      | Int (Some i) -> fprintf fmt "[%i,%i]" i i
      | Clos (f, _, m) ->
          fprintf fmt "%s [%a]" f
            (pp_print_list
               ~pp_sep:(fun fmt () -> pp_print_char fmt ',')
               (fun fmt () -> pp_print_char fmt '_'))
            (ExtList.repeat m ())

    let as_bool = function Int _ | Clos _ -> NoBool | Bool b -> Bool b
  end

  module V = struct
    module S = Set.Make (V0)

    type t = S.t option [@@deriving ord]

    let pp fmt =
      let open Format in
      function
      | None -> pp_print_string fmt "⊤"
      | Some s ->
          if S.is_empty s then pp_print_string fmt "⊥"
          else
            fprintf fmt "@[{%a@]}"
              (pp_print_seq ~pp_sep:(fun fmt () -> fprintf fmt ",@ ") V0.pp)
              (S.to_seq s)

    let bot = Some S.empty
    let top = None
    let is_bot = function None -> false | Some s -> S.is_empty s
    let inject v = Some (S.singleton v)

    let join o1 o2 =
      match (o1, o2) with
      | None, _ | _, None -> None
      | Some s1, Some s2 ->
          let s =
            S.fold
              (fun v s ->
                match v with
                | Int None ->
                    S.add v (S.filter (function Int _ -> false | _ -> true) s)
                | Int (Some i) -> (
                    match
                      S.find_first_opt (function Int _ -> true | _ -> false) s
                    with
                    | None -> S.add v s
                    | Some (Int None) -> s
                    | Some (Int (Some j)) ->
                        if i = j then s
                        else
                          S.add (V0.Int None)
                            (S.filter (function Int _ -> false | _ -> true) s)
                    | _ -> assert false)
                | _ -> S.add v s)
              s1 s2
          in
          Some s

    let widen _n o1 o2 = join o1 o2

    let leq o1 o2 =
      match (o1, o2) with
      | _, None -> true
      | None, Some _ -> false
      | Some s1, Some s2 ->
          S.for_all
            (function
              | (Bool _ | Int None | Clos _) as v1 -> S.mem v1 s2
              | Int (Some n1) ->
                  S.exists
                    (function
                      | Bool _ | Clos _ -> false
                      | Int None -> true
                      | Int (Some n2) -> n1 = n2)
                    s2)
            s1

    let as_bool = function
      | None -> AnyBool
      | Some s -> S.fold (fun v b -> abs_bool_join (V0.as_bool v) b) s NoBool

    let app_map_reduce ~map ~join ~bot:init ~top = function
      | None -> top
      | Some s ->
          S.fold
            (fun v acc ->
              match v with
              | Bool _ | Int _ -> acc
              | Clos (f, n, m) -> join (map f n (ExtList.repeat m bot)) acc)
              (* XXX: /!\ repeat m bot *)
            s init

    let inject_val = function
      | Concrete.V.Bot -> bot
      | Top -> top
      | Int n -> inject (V0.Int (Some n))
      | Bool b -> inject (V0.Bool b)
      | Clos (f, n, vs) -> inject (V0.Clos (f, n, List.length vs))

    let meet o1 o2 =
      match (o1, o2) with
      | None, o | o, None -> o
      | Some s1, Some s2 ->
          let s =
            S.fold
              (fun v1 acc ->
                S.fold
                  (fun v2 acc ->
                    match (v1, v2) with
                    | Bool b1, Bool b2 -> if b1 = b2 then S.add v1 acc else acc
                    | Int (Some n1), Int (Some n2) ->
                        if n1 = n2 then S.add v1 acc else acc
                    | (Int (Some _) as v), Int None
                    | Int None, (Int (Some _) as v) ->
                        S.add v acc
                    | Int None, Int None -> S.add v1 acc
                    | Clos (f1, n1, m1), Clos (f2, n2, m2) ->
                        if F.equal f1 f2 && n1 = n2 && m1 = m2 then S.add v1 acc
                        else acc
                    | Int _, (Bool _ | Clos _)
                    | (Bool _ | Clos _), Int _
                    | Bool _, Clos _
                    | Clos _, Bool _ ->
                        acc)
                  s2 acc)
              s1 S.empty
          in
          Some s
  end

  module MakeI (G : GRAPH with type x := V.t list and type y := V.t) = struct
    type phi = G.t FMap.t option

    let fun_ f n vs = V.inject (V0.Clos (f, n, List.length vs))

    let binop default f v1 v2 =
      if V.is_bot v1 || V.is_bot v2 then V.bot
      else
        match (v1, v2) with
        | None, _ | _, None -> default
        | Some vs1, Some vs2 ->
            V.S.fold
              (fun v1 acc ->
                V.S.fold (fun v2 acc -> V.join (f v1 v2) acc) vs2 acc)
              vs1 V.bot

    let int_op2_int f =
      binop (V.inject (V0.Int None)) (fun v1 v2 ->
          match (v1, v2) with
          | V0.Bool _, _ | _, V0.Bool _ -> V.bot
          | Clos _, _ | _, Clos _ -> V.bot
          | Int (Some i1), Int (Some i2) -> V.inject (V0.Int (Some (f i1 i2)))
          | Int _, Int None | Int None, Int _ -> V.inject (V0.Int None))

    let int_op2_bool f =
      let top_bool =
        V.(join (inject (V0.Bool true)) (inject (V0.Bool false)))
      in
      binop top_bool (fun v1 v2 ->
          match (v1, v2) with
          | V0.Bool _, _ | _, V0.Bool _ -> V.bot
          | Clos _, _ | _, Clos _ -> V.bot
          | Int (Some i1), Int (Some i2) -> V.inject (V0.Bool (f i1 i2))
          | Int None, Int _ | Int _, Int None -> top_bool)

    let basic a vs =
      match (a, vs) with
      | Common.Bool b, [] -> V.inject (V0.Bool b)
      | Bool _, _ -> V.bot
      | Int i, [] -> V.inject (V0.Int (Some i))
      | Int _, _ -> V.bot
      | Plus, [ v1; v2 ] -> int_op2_int ( + ) v1 v2
      | Plus, _ -> V.bot
      | Minus, [ v1; v2 ] -> int_op2_int ( - ) v1 v2
      | Minus, _ -> V.bot
      | Mult, [ v1; v2 ] -> int_op2_int ( * ) v1 v2
      | Mult, _ -> V.bot
      | Eq, [ v1; v2 ] -> int_op2_bool ( = ) v1 v2
      | Eq, _ -> V.bot
      | Lt, [ v1; v2 ] -> int_op2_bool ( < ) v1 v2
      | Lt, _ -> V.bot
      | Le, [ v1; v2 ] -> int_op2_bool ( <= ) v1 v2
      | Le, _ -> V.bot
      | Gt, [ v1; v2 ] -> int_op2_bool ( > ) v1 v2
      | Gt, _ -> V.bot
      | Ge, [ v1; v2 ] -> int_op2_bool ( >= ) v1 v2
      | Ge, _ -> V.bot

    let get_phi ophi f n =
      match ophi with
      | None -> V.top
      | Some phi -> (
          match FMap.find_opt f phi with
          | None -> V.bot
          | Some g -> (
              match G.find_opt (ExtList.repeat n V.bot) g with
              | Some v -> v
              | None -> V.bot))

    let apply ophi v1 v2 =
      if V.is_bot v2 then V.bot
      else
        match v1 with
        | None -> V.top
        | Some vs ->
            V.S.fold
              (fun v1 acc ->
                let out =
                  match v1 with
                  | Clos (f, n, m) ->
                      if m + 1 < n then V.inject (V0.Clos (f, n, m + 1))
                      else (
                        assert (m + 1 = n);
                        get_phi ophi f n)
                  | _ -> V.bot
                in
                V.join out acc)
              vs V.bot
  end
end

module AbstractInterval = struct
  module V0 = struct
    type t = Bool of bool | Int of IntervalDomain.t | Clos of F.t * int * int
    [@@deriving ord]

    let pp fmt =
      let open Format in
      function
      | Bool b -> pp_print_bool fmt b
      | Int i -> IntervalDomain.pp fmt i
      | Clos (f, _, m) ->
          fprintf fmt "%s [%a]" f
            (pp_print_list
               ~pp_sep:(fun fmt () -> pp_print_char fmt ',')
               (fun fmt () -> pp_print_char fmt '_'))
            (ExtList.repeat m ())

    let as_bool = function Int _ | Clos _ -> NoBool | Bool b -> Bool b
  end

  module V = struct
    module S = Set.Make (V0)

    type t = S.t option [@@deriving ord]

    let pp fmt =
      let open Format in
      function
      | None -> pp_print_string fmt "⊤"
      | Some s ->
          if S.is_empty s then pp_print_string fmt "⊥"
          else
            fprintf fmt "@[{%a@]}"
              (pp_print_seq ~pp_sep:(fun fmt () -> fprintf fmt ",@ ") V0.pp)
              (S.to_seq s)

    let bot = Some S.empty
    let top = None
    let is_bot = function None -> false | Some s -> S.is_empty s

    let inject v =
      match v with
      | V0.Int i when IntervalDomain.is_bot i -> bot
      | _ -> Some (S.singleton v)

    let join o1 o2 =
      match (o1, o2) with
      | None, _ | _, None -> None
      | Some s1, Some s2 ->
          let s =
            S.fold
              (fun v s ->
                match v with
                | Int i -> (
                    match
                      S.find_first_opt (function Int _ -> true | _ -> false) s
                    with
                    | None -> S.add v s
                    | Some (Int j) ->
                        S.add
                          (V0.Int (IntervalDomain.join i j))
                          (S.filter (function Int _ -> false | _ -> true) s)
                    | _ -> assert false)
                | _ -> S.add v s)
              s1 s2
          in
          Some s

    let widen o1 o2 =
      match (o1, o2) with
      | None, _ | _, None -> None
      | Some s1, Some s2 ->
          let s =
            S.fold
              (fun v s ->
                match v with
                | Int i -> (
                    match
                      S.find_first_opt (function Int _ -> true | _ -> false) s
                    with
                    | None -> S.add v s
                    | Some (Int j) ->
                        S.add
                          (V0.Int (IntervalDomain.widen i j))
                          (S.filter (function Int _ -> false | _ -> true) s)
                    | _ -> assert false)
                | _ -> S.add v s)
              s1 s2
          in
          Some s

    let widen _n o1 o2 = widen o1 o2

    let leq o1 o2 =
      match (o1, o2) with
      | _, None -> true
      | None, Some _ -> false
      | Some s1, Some s2 ->
          S.for_all
            (function
              | (Bool _ | Clos _) as v1 -> S.mem v1 s2
              | Int i1 ->
                  S.exists
                    (function
                      | Bool _ | Clos _ -> false
                      | Int i2 -> IntervalDomain.leq i1 i2)
                    s2)
            s1

    let as_bool = function
      | None -> AnyBool
      | Some s -> S.fold (fun v b -> abs_bool_join (V0.as_bool v) b) s NoBool

    let app_map_reduce ~map ~join ~bot:init ~top = function
      | None -> top
      | Some s ->
          S.fold
            (fun v acc ->
              match v with
              | Bool _ | Int _ -> acc
              | Clos (f, n, m) -> join (map f n (ExtList.repeat m bot)) acc)
              (* XXX: /!\ repeat m bot *)
            s init

    let inject_val = function
      | Concrete.V.Bot -> bot
      | Top -> top
      | Int n -> inject (V0.Int (IntervalDomain.inject n))
      | Bool b -> inject (V0.Bool b)
      | Clos (f, n, vs) -> inject (V0.Clos (f, n, List.length vs))

    let meet o1 o2 =
      match (o1, o2) with
      | None, o | o, None -> o
      | Some s1, Some s2 ->
          let s =
            S.fold
              (fun v1 acc ->
                S.fold
                  (fun v2 acc ->
                    match (v1, v2) with
                    | Bool b1, Bool b2 -> if b1 = b2 then S.add v1 acc else acc
                    | Int i1, Int i2 ->
                        let i = IntervalDomain.meet i1 i2 in
                        if IntervalDomain.is_bot i then acc
                        else S.add (Int i) acc
                    | Clos (f1, n1, m1), Clos (f2, n2, m2) ->
                        if F.equal f1 f2 && n1 = n2 && m1 = m2 then S.add v1 acc
                        else acc
                    | Int _, (Bool _ | Clos _)
                    | (Bool _ | Clos _), Int _
                    | Bool _, Clos _
                    | Clos _, Bool _ ->
                        acc)
                  s2 acc)
              s1 S.empty
          in
          Some s
  end

  module MakeI (G : GRAPH with type x := V.t list and type y := V.t) = struct
    type phi = G.t FMap.t option

    let fun_ f n vs = V.inject (V0.Clos (f, n, List.length vs))

    let binop default f v1 v2 =
      if V.is_bot v1 || V.is_bot v2 then V.bot
      else
        match (v1, v2) with
        | None, _ | _, None -> default
        | Some vs1, Some vs2 ->
            V.S.fold
              (fun v1 acc ->
                V.S.fold (fun v2 acc -> V.join (f v1 v2) acc) vs2 acc)
              vs1 V.bot

    let int_op2_int f =
      binop (V.inject (V0.Int IntervalDomain.top)) (fun v1 v2 ->
          match (v1, v2) with
          | V0.Bool _, _ | _, V0.Bool _ -> V.bot
          | Clos _, _ | _, Clos _ -> V.bot
          | Int i1, Int i2 -> V.inject (V0.Int (f i1 i2)))

    let int_op2_bool f =
      let top_bool =
        V.(join (inject (V0.Bool true)) (inject (V0.Bool false)))
      in
      binop top_bool (fun v1 v2 ->
          match (v1, v2) with
          | V0.Bool _, _ | _, V0.Bool _ -> V.bot
          | Clos _, _ | _, Clos _ -> V.bot
          | Int i1, Int i2 -> (
              match f i1 i2 with
              | Some b -> V.inject (V0.Bool b)
              | None -> top_bool))

    let basic a vs =
      match (a, vs) with
      | Common.Bool b, [] -> V.inject (V0.Bool b)
      | Bool _, _ -> V.bot
      | Int i, [] -> V.inject (V0.Int (IntervalDomain.inject i))
      | Int _, _ -> V.bot
      | Plus, [ v1; v2 ] -> int_op2_int IntervalDomain.plus v1 v2
      | Plus, _ -> V.bot
      | Minus, [ v1; v2 ] -> int_op2_int IntervalDomain.minus v1 v2
      | Minus, _ -> V.bot
      | Mult, [ v1; v2 ] -> int_op2_int IntervalDomain.mult v1 v2
      | Mult, _ -> V.bot
      | Eq, [ v1; v2 ] -> int_op2_bool IntervalDomain.eq v1 v2
      | Eq, _ -> V.bot
      | Lt, [ v1; v2 ] -> int_op2_bool IntervalDomain.lt v1 v2
      | Lt, _ -> V.bot
      | Le, [ v1; v2 ] -> int_op2_bool IntervalDomain.le v1 v2
      | Le, _ -> V.bot
      | Gt, [ v1; v2 ] -> int_op2_bool IntervalDomain.gt v1 v2
      | Gt, _ -> V.bot
      | Ge, [ v1; v2 ] -> int_op2_bool IntervalDomain.ge v1 v2
      | Ge, _ -> V.bot

    let get_phi ophi f n =
      match ophi with
      | None -> V.top
      | Some phi -> (
          match FMap.find_opt f phi with
          | None -> V.bot
          | Some g -> (
              match G.find_opt (ExtList.repeat n V.bot) g with
              | Some v -> v
              | None -> V.bot))

    let apply ophi v1 v2 =
      if V.is_bot v2 then V.bot
      else
        match v1 with
        | None -> V.top
        | Some vs ->
            V.S.fold
              (fun v1 acc ->
                let out =
                  match v1 with
                  | Clos (f, n, m) ->
                      if m + 1 < n then V.inject (V0.Clos (f, n, m + 1))
                      else (
                        assert (m + 1 = n);
                        get_phi ophi f n)
                  | _ -> V.bot
                in
                V.join out acc)
              vs V.bot
  end
end
