(**
  Author: Benoît Montagu <benoit.montagu@inria.fr>
  Copyright Inria © 2022
*)

open Sigs

type ('x, 'a, 'f) expr =
  | Var of 'x
  | Cons of 'a * ('x, 'a, 'f) expr list
  | Call of 'f * ('x, 'a, 'f) expr list
  | IfThenElse of ('x, 'a, 'f) expr * ('x, 'a, 'f) expr * ('x, 'a, 'f) expr

type ('x, 'a, 'f) decl = 'f * 'x list * ('x, 'a, 'f) expr
type ('x, 'a, 'f) program = ('x, 'a, 'f) decl list

module type GRAPH = sig
  type x
  type y
  type t

  val bot : t
  val join : t -> t -> t
  val widen : int -> t -> t -> t
  val leq : t -> t -> bool
  val singleton : x -> y -> t
  val find_opt : x -> t -> y option
  val map_reduce : (x -> y -> 'a) -> ('a -> 'a -> 'a) -> 'a -> t -> 'a

  include PP_TYPE with type t := t
end

module Graph (X : sig
  include ORDERED_TYPE
  include PP_TYPE with type t := t
end) (Y : sig
  type t

  val join : t -> t -> t
  val widen : int -> t -> t -> t
  val leq : t -> t -> bool

  include PP_TYPE with type t := t
end) : GRAPH with type x := X.t list and type y := Y.t

module AbstractGraph (X : sig
  type t

  val join : t -> t -> t
  val widen : int -> t -> t -> t
  val leq : t -> t -> bool

  include ORDERED_TYPE with type t := t
  include PP_TYPE with type t := t
end) (Y : sig
  type t

  val join : t -> t -> t
  val widen : int -> t -> t -> t
  val leq : t -> t -> bool

  include PP_TYPE with type t := t
end) : GRAPH with type x := X.t list and type y := Y.t

module AbstractGraphElementaryPartitioning (X : sig
  include ORDERED_TYPE
  include PP_TYPE with type t := t

  val is_bot : t -> bool
  val join : t -> t -> t
  val widen : int -> t -> t -> t
  val meet : t -> t -> t
  val leq : t -> t -> bool
end) (Y : sig
  include PP_TYPE

  val bot : t
  val join : t -> t -> t
  val widen : int -> t -> t -> t
  val leq : t -> t -> bool
end) : GRAPH with type x := X.t list and type y := Y.t

module X : ORDERED_TYPE with type t = string
module A : TYPE with type t = Common.prim
module F : ORDERED_TYPE with type t = string

module FMap : sig
  include Map.S with type key = F.t

  val pp : (Format.formatter -> 'a -> unit) -> Format.formatter -> 'a t -> unit
end

module type VALUE = sig
  type t

  val compare : t -> t -> int
  val bot : t
  val is_bot : t -> bool
  val join : t -> t -> t
  val widen : int -> t -> t -> t
  val meet : t -> t -> t
  val leq : t -> t -> bool
  val as_bool : t -> Common.abs_bool
  val pp : Format.formatter -> t -> unit
end

module type INTERP = sig
  type v
  type phi

  val basic : A.t -> v list -> v
  val apply : F.t -> phi -> v list -> v
end

module Concrete : sig
  module V : sig
    type t = Bot | Top | Int of int | Bool of bool

    include VALUE with type t := t
  end

  module MakeI (G : GRAPH with type x := V.t list and type y := V.t) :
    INTERP with type v := V.t and type phi = G.t FMap.t
end

module AbstractType : sig
  module V : sig
    include VALUE

    val inject_val : Concrete.V.t -> t
  end

  module MakeI (G : GRAPH with type x := V.t list and type y := V.t) :
    INTERP with type v := V.t and type phi = G.t FMap.t
end

module AbstractConst : sig
  module V : sig
    include VALUE

    val inject_val : Concrete.V.t -> t
  end

  module MakeI (G : GRAPH with type x := V.t list and type y := V.t) :
    INTERP with type v := V.t and type phi = G.t FMap.t
end

module AbstractInterval : sig
  module V : sig
    include VALUE

    val inject_val : Concrete.V.t -> t
  end

  module MakeI (G : GRAPH with type x := V.t list and type y := V.t) :
    INTERP with type v := V.t and type phi = G.t FMap.t
end
