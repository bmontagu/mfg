(**
  Author: Benoît Montagu <benoit.montagu@inria.fr>
  Copyright Inria © 2022
*)

open Sigs
open Common

type ('x, 'a, 'f) expr =
  | Var of 'x
  | Cons of 'a * ('x, 'a, 'f) expr list
  | Call of 'f * ('x, 'a, 'f) expr list
  | IfThenElse of ('x, 'a, 'f) expr * ('x, 'a, 'f) expr * ('x, 'a, 'f) expr

type ('x, 'a, 'f) decl = 'f * 'x list * ('x, 'a, 'f) expr
type ('x, 'a, 'f) program = ('x, 'a, 'f) decl list

module type GRAPH = sig
  type x
  type y
  type t

  val bot : t
  val join : t -> t -> t
  val widen : int -> t -> t -> t
  val leq : t -> t -> bool
  val singleton : x -> y -> t
  val find_opt : x -> t -> y option
  val map_reduce : (x -> y -> 'a) -> ('a -> 'a -> 'a) -> 'a -> t -> 'a

  include PP_TYPE with type t := t
end

module Graph (X : sig
  include ORDERED_TYPE
  include PP_TYPE with type t := t
end) (Y : sig
  type t

  val join : t -> t -> t
  val widen : int -> t -> t -> t
  val leq : t -> t -> bool

  include PP_TYPE with type t := t
end) : GRAPH with type x := X.t list and type y := Y.t = struct
  module XS = ListOf (X)
  module M = Map.Make (XS)

  type t = Y.t M.t

  let bot = M.empty
  let join_gen join g1 g2 = M.union (fun _args v1 v2 -> Some (join v1 v2)) g1 g2
  let join g1 g2 = join_gen Y.join g1 g2
  let widen n g1 g2 = join_gen (Y.widen n) g1 g2

  let leq g1 g2 =
    M.for_all
      (fun vs v1 ->
        match M.find_opt vs g2 with None -> false | Some v2 -> Y.leq v1 v2)
      g1

  let singleton = M.singleton
  let find_opt = M.find_opt

  let map_reduce map reduce init t =
    M.fold (fun x y acc -> reduce (map x y) acc) t init

  let pp fmt graph =
    let open Format in
    fprintf fmt "@[<v>%a@]"
      (pp_print_seq ~pp_sep:pp_print_space (fun fmt (vs, v) ->
           fprintf fmt "@[%a ->@ %a@]" XS.pp vs Y.pp v))
      (M.to_seq graph)
end

module AbstractGraph (X : sig
  include ORDERED_TYPE
  include PP_TYPE with type t := t

  val join : t -> t -> t
  val widen : int -> t -> t -> t
  val leq : t -> t -> bool
end) (Y : sig
  include PP_TYPE

  val join : t -> t -> t
  val widen : int -> t -> t -> t
  val leq : t -> t -> bool
end) : GRAPH with type x := X.t list and type y := Y.t = struct
  module XS = struct
    include ListOf (X)

    let join_gen x_join xs1 xs2 =
      assert (List.length xs1 = List.length xs2);
      list_join x_join xs1 xs2

    let leq xs1 xs2 =
      try List.for_all2 X.leq xs1 xs2 with Invalid_argument _ -> false

    let join xs1 xs2 = join_gen X.join xs1 xs2
    let widen n xs1 xs2 = join_gen (X.widen n) xs1 xs2
  end

  include Functional_partitioning.Basic.Make (XS) (Y)
end

module X = String

module A = struct
  type t = prim
end

module F = String

module FMap = struct
  include Map.Make (F)

  let pp pp_elt fmt m =
    let open Format in
    fprintf fmt "@[<v>%a@]"
      (pp_print_seq ~pp_sep:pp_print_space (fun fmt (f, elt) ->
           fprintf fmt "@[<hv 2>%s:@ {@[%a@]}@]" f pp_elt elt))
      (to_seq m)
end

module type VALUE = sig
  type t

  val compare : t -> t -> int
  val bot : t
  val is_bot : t -> bool
  val join : t -> t -> t
  val widen : int -> t -> t -> t
  val meet : t -> t -> t
  val leq : t -> t -> bool
  val as_bool : t -> Common.abs_bool
  val pp : Format.formatter -> t -> unit
end

module type INTERP = sig
  type v
  type phi

  val basic : A.t -> v list -> v
  val apply : F.t -> phi -> v list -> v
end

module Concrete = struct
  module V = struct
    type t = Bot | Top | Int of int | Bool of bool [@@deriving ord]

    let pp fmt =
      let open Format in
      function
      | Bot -> pp_print_string fmt "⊥"
      | Top -> pp_print_string fmt "⊤"
      | Int n -> pp_print_int fmt n
      | Bool b -> pp_print_bool fmt b

    let bot = Bot
    let is_bot = function Bot -> true | _ -> false

    let as_bool = function
      | Bot | Int _ -> NoBool
      | Bool b -> Bool b
      | Top -> AnyBool

    let leq v1 v2 =
      match (v1, v2) with
      | Bot, _ | _, Top -> true
      | Int n1, Int n2 -> n1 = n2
      | Bool b1, Bool b2 -> b1 = b2
      | _ -> false

    let join v1 v2 =
      match (v1, v2) with
      | Bot, v | v, Bot | (Top as v), _ | _, (Top as v) -> v
      | Int n1, Int n2 -> if n1 = n2 then v1 else Top
      | Bool b1, Bool b2 -> if b1 = b2 then v1 else Top
      | Int _, Bool _ | Bool _, Int _ -> Top

    let widen _ v1 v2 = join v1 v2

    let meet v1 v2 =
      match (v1, v2) with
      | Bot, _ | _, Bot -> Bot
      | Top, v | v, Top -> v
      | Int n1, Int n2 -> if n1 = n2 then v1 else Bot
      | Bool b1, Bool b2 -> if b1 = b2 then v1 else Bot
      | Int _, Bool _ | Bool _, Int _ -> Bot
  end

  module MakeI (G : GRAPH with type x := V.t list and type y := V.t) = struct
    type phi = G.t FMap.t

    let int_op2_int f v1 v2 =
      match (v1, v2) with
      | V.Bot, _ | _, V.Bot -> V.Bot
      | V.Top, _ | _, V.Top -> Top
      | V.Bool _, _ | _, V.Bool _ -> Bot
      | V.Int n1, V.Int n2 -> Int (f n1 n2)

    let int_op2_bool f v1 v2 =
      match (v1, v2) with
      | V.Bot, _ | _, V.Bot -> V.Bot
      | V.Top, _ | _, V.Top -> Top
      | V.Bool _, _ | _, V.Bool _ -> Bot
      | V.Int n1, V.Int n2 -> Bool (f n1 n2)

    let basic a vs =
      match (a, vs) with
      | Common.Bool b, [] -> V.Bool b
      | Bool _, _ -> V.bot
      | Int n, [] -> V.Int n
      | Int _, _ -> V.bot
      | Plus, [ v1; v2 ] -> int_op2_int ( + ) v1 v2
      | Plus, _ -> V.bot
      | Minus, [ v1; v2 ] -> int_op2_int ( - ) v1 v2
      | Minus, _ -> V.bot
      | Mult, [ v1; v2 ] -> int_op2_int ( * ) v1 v2
      | Mult, _ -> V.bot
      | Eq, [ v1; v2 ] -> int_op2_bool ( = ) v1 v2
      | Eq, _ -> V.bot
      | Lt, [ v1; v2 ] -> int_op2_bool ( < ) v1 v2
      | Lt, _ -> V.bot
      | Le, [ v1; v2 ] -> int_op2_bool ( <= ) v1 v2
      | Le, _ -> V.bot
      | Gt, [ v1; v2 ] -> int_op2_bool ( > ) v1 v2
      | Gt, _ -> V.bot
      | Ge, [ v1; v2 ] -> int_op2_bool ( >= ) v1 v2
      | Ge, _ -> V.bot

    let apply f phi vs =
      match FMap.find_opt f phi with
      | None -> V.bot
      | Some g -> ( match G.find_opt vs g with None -> V.bot | Some v -> v)
  end
end

module AbstractType = struct
  module V0 = struct
    type t = Bool | Int [@@deriving ord]

    let pp fmt =
      let open Format in
      function
      | Bool -> pp_print_string fmt "bool" | Int -> pp_print_string fmt "int"

    let as_bool = function Int -> NoBool | Bool -> AnyBool
  end

  module V = struct
    module S = Set.Make (V0)

    type t = S.t option [@@deriving ord]

    let pp fmt =
      let open Format in
      function
      | None -> pp_print_string fmt "⊤"
      | Some s ->
          if S.is_empty s then pp_print_string fmt "⊥"
          else
            fprintf fmt "@[{%a@]}"
              (pp_print_seq ~pp_sep:(fun fmt () -> fprintf fmt ",@ ") V0.pp)
              (S.to_seq s)

    let bot = Some S.empty
    let top = None
    let is_bot = function None -> false | Some s -> S.is_empty s
    let inject v = Some (S.singleton v)

    let join o1 o2 =
      match (o1, o2) with
      | None, _ | _, None -> None
      | Some s1, Some s2 -> Some (S.union s1 s2)

    let widen _ o1 o2 = join o1 o2

    let leq o1 o2 =
      match (o1, o2) with
      | _, None -> true
      | None, Some _ -> false
      | Some s1, Some s2 -> S.subset s1 s2

    let as_bool = function
      | None -> AnyBool
      | Some s -> S.fold (fun v b -> abs_bool_join (V0.as_bool v) b) s NoBool

    let inject_val = function
      | Concrete.V.Bot -> bot
      | Top -> top
      | Int _ -> inject V0.Int
      | Bool _ -> inject V0.Bool

    let meet o1 o2 =
      match (o1, o2) with
      | None, o | o, None -> o
      | Some s1, Some s2 -> Some (S.inter s1 s2)
  end

  module MakeI (G : GRAPH with type x := V.t list and type y := V.t) = struct
    type phi = G.t FMap.t

    let binop default f v1 v2 =
      if V.is_bot v1 || V.is_bot v2 then V.bot
      else
        match (v1, v2) with
        | None, _ | _, None -> default
        | Some vs1, Some vs2 ->
            V.S.fold
              (fun v1 acc ->
                V.S.fold (fun v2 acc -> V.join (f v1 v2) acc) vs2 acc)
              vs1 V.bot

    let int_op2_int =
      binop (V.inject V0.Int) (fun v1 v2 ->
          match (v1, v2) with
          | V0.Bool, _ | _, V0.Bool -> V.bot
          | Int, Int -> V.inject V0.Int)

    let int_op2_bool =
      binop (V.inject V0.Bool) (fun v1 v2 ->
          match (v1, v2) with
          | V0.Bool, _ | _, V0.Bool -> V.bot
          | Int, Int -> V.inject V0.Bool)

    let basic a vs =
      match (a, vs) with
      | Common.Bool _b, [] -> V.inject V0.Bool
      | Bool _, _ -> V.bot
      | Int _n, [] -> V.inject V0.Int
      | Int _, _ -> V.bot
      | Plus, [ v1; v2 ] -> int_op2_int v1 v2
      | Plus, _ -> V.bot
      | Minus, [ v1; v2 ] -> int_op2_int v1 v2
      | Minus, _ -> V.bot
      | Mult, [ v1; v2 ] -> int_op2_int v1 v2
      | Mult, _ -> V.bot
      | Eq, [ v1; v2 ] -> int_op2_bool v1 v2
      | Eq, _ -> V.bot
      | Lt, [ v1; v2 ] -> int_op2_bool v1 v2
      | Lt, _ -> V.bot
      | Le, [ v1; v2 ] -> int_op2_bool v1 v2
      | Le, _ -> V.bot
      | Gt, [ v1; v2 ] -> int_op2_bool v1 v2
      | Gt, _ -> V.bot
      | Ge, [ v1; v2 ] -> int_op2_bool v1 v2
      | Ge, _ -> V.bot

    let get_phi phi f vs =
      match FMap.find_opt f phi with
      | None -> V.bot
      | Some g -> ( match G.find_opt vs g with Some v -> v | None -> V.bot)

    let apply f phi vs =
      if List.exists V.is_bot vs then V.bot else get_phi phi f vs
  end
end

module AbstractConst = struct
  module V0 = struct
    type t = Bool of bool | Int of int option [@@deriving ord]

    let pp fmt =
      let open Format in
      function
      | Bool b -> pp_print_bool fmt b
      | Int None -> pp_print_string fmt "[-∞,+∞]"
      | Int (Some i) -> fprintf fmt "[%i,%i]" i i

    let as_bool = function Int _ -> NoBool | Bool b -> Bool b
  end

  module V = struct
    module S = Set.Make (V0)

    type t = S.t option [@@deriving ord]

    let pp fmt =
      let open Format in
      function
      | None -> pp_print_string fmt "⊤"
      | Some s ->
          if S.is_empty s then pp_print_string fmt "⊥"
          else
            fprintf fmt "@[{%a@]}"
              (pp_print_seq ~pp_sep:(fun fmt () -> fprintf fmt ",@ ") V0.pp)
              (S.to_seq s)

    let bot = Some S.empty
    let top = None
    let is_bot = function None -> false | Some s -> S.is_empty s
    let inject v = Some (S.singleton v)

    let join o1 o2 =
      match (o1, o2) with
      | None, _ | _, None -> None
      | Some s1, Some s2 ->
          let s =
            S.fold
              (fun v s ->
                match v with
                | Int None ->
                    S.add v (S.filter (function Int _ -> false | _ -> true) s)
                | Int (Some i) -> (
                    match
                      S.find_first_opt (function Int _ -> true | _ -> false) s
                    with
                    | None -> S.add v s
                    | Some (Int None) -> s
                    | Some (Int (Some j)) ->
                        if i = j then s
                        else
                          S.add (V0.Int None)
                            (S.filter (function Int _ -> false | _ -> true) s)
                    | _ -> assert false)
                | _ -> S.add v s)
              s1 s2
          in
          Some s

    let widen _ o1 o2 = join o1 o2

    let leq o1 o2 =
      match (o1, o2) with
      | _, None -> true
      | None, Some _ -> false
      | Some s1, Some s2 ->
          S.for_all
            (function
              | (Bool _ | Int None) as v1 -> S.mem v1 s2
              | Int (Some n1) ->
                  S.exists
                    (function
                      | Bool _ -> false
                      | Int None -> true
                      | Int (Some n2) -> n1 = n2)
                    s2)
            s1

    let as_bool = function
      | None -> AnyBool
      | Some s -> S.fold (fun v b -> abs_bool_join (V0.as_bool v) b) s NoBool

    let inject_val = function
      | Concrete.V.Bot -> bot
      | Top -> top
      | Int n -> inject (V0.Int (Some n))
      | Bool b -> inject (V0.Bool b)

    let meet o1 o2 =
      match (o1, o2) with
      | None, o | o, None -> o
      | Some s1, Some s2 ->
          let s =
            S.fold
              (fun v1 acc ->
                match v1 with
                | Bool _ -> if S.mem v1 s2 then S.add v1 acc else acc
                | Int (Some _) ->
                    if S.mem v1 s2 || S.mem (Int None) s2 then S.add v1 acc
                    else acc
                | Int None -> (
                    let s2' =
                      S.filter (function Int _ -> true | _ -> false) s2
                    in
                    match S.min_elt_opt s2' with
                    | Some v' -> S.add v' acc
                    | None -> acc))
              s1 S.empty
          in
          Some s
  end

  module MakeI (G : GRAPH with type x := V.t list and type y := V.t) = struct
    type phi = G.t FMap.t

    let binop default f v1 v2 =
      if V.is_bot v1 || V.is_bot v2 then V.bot
      else
        match (v1, v2) with
        | None, _ | _, None -> default
        | Some vs1, Some vs2 ->
            V.S.fold
              (fun v1 acc ->
                V.S.fold (fun v2 acc -> V.join (f v1 v2) acc) vs2 acc)
              vs1 V.bot

    let int_op2_int f =
      binop (V.inject (V0.Int None)) (fun v1 v2 ->
          match (v1, v2) with
          | V0.Bool _, _ | _, V0.Bool _ -> V.bot
          | Int (Some i1), Int (Some i2) -> V.inject (V0.Int (Some (f i1 i2)))
          | Int _, Int None | Int None, Int _ -> V.inject (V0.Int None))

    let int_op2_bool f =
      let top_bool =
        V.(join (inject (V0.Bool true)) (inject (V0.Bool false)))
      in
      binop top_bool (fun v1 v2 ->
          match (v1, v2) with
          | V0.Bool _, _ | _, V0.Bool _ -> V.bot
          | Int (Some i1), Int (Some i2) -> V.inject (V0.Bool (f i1 i2))
          | Int None, Int _ | Int _, Int None -> top_bool)

    let basic a vs =
      match (a, vs) with
      | Common.Bool b, [] -> V.inject (V0.Bool b)
      | Bool _, _ -> V.bot
      | Int i, [] -> V.inject (V0.Int (Some i))
      | Int _, _ -> V.bot
      | Plus, [ v1; v2 ] -> int_op2_int ( + ) v1 v2
      | Plus, _ -> V.bot
      | Minus, [ v1; v2 ] -> int_op2_int ( - ) v1 v2
      | Minus, _ -> V.bot
      | Mult, [ v1; v2 ] -> int_op2_int ( * ) v1 v2
      | Mult, _ -> V.bot
      | Eq, [ v1; v2 ] -> int_op2_bool ( = ) v1 v2
      | Eq, _ -> V.bot
      | Lt, [ v1; v2 ] -> int_op2_bool ( < ) v1 v2
      | Lt, _ -> V.bot
      | Le, [ v1; v2 ] -> int_op2_bool ( <= ) v1 v2
      | Le, _ -> V.bot
      | Gt, [ v1; v2 ] -> int_op2_bool ( > ) v1 v2
      | Gt, _ -> V.bot
      | Ge, [ v1; v2 ] -> int_op2_bool ( >= ) v1 v2
      | Ge, _ -> V.bot

    let get_phi phi f vs =
      match FMap.find_opt f phi with
      | None -> V.bot
      | Some g -> ( match G.find_opt vs g with Some v -> v | None -> V.bot)

    let apply f phi vs =
      if List.exists V.is_bot vs then V.bot else get_phi phi f vs
  end
end

module AbstractInterval = struct
  module V0 = struct
    type t = Bool of bool | Int of IntervalDomain.t [@@deriving ord]

    let pp fmt =
      let open Format in
      function
      | Bool b -> pp_print_bool fmt b | Int i -> IntervalDomain.pp fmt i

    let as_bool = function Int _ -> NoBool | Bool b -> Bool b
  end

  module V = struct
    module S = Set.Make (V0)

    type t = S.t option [@@deriving ord]

    let pp fmt =
      let open Format in
      function
      | None -> pp_print_string fmt "⊤"
      | Some s ->
          if S.is_empty s then pp_print_string fmt "⊥"
          else
            fprintf fmt "@[{%a@]}"
              (pp_print_seq ~pp_sep:(fun fmt () -> fprintf fmt ",@ ") V0.pp)
              (S.to_seq s)

    let bot = Some S.empty
    let top = None
    let is_bot = function None -> false | Some s -> S.is_empty s

    let inject v =
      match v with
      | V0.Int i when IntervalDomain.is_bot i -> bot
      | _ -> Some (S.singleton v)

    let join_gen join o1 o2 =
      match (o1, o2) with
      | None, _ | _, None -> None
      | Some s1, Some s2 ->
          let s =
            S.fold
              (fun v s ->
                match v with
                | Int i -> (
                    match
                      S.find_first_opt (function Int _ -> true | _ -> false) s
                    with
                    | None -> S.add v s
                    | Some (Int j) ->
                        S.add
                          (V0.Int (join i j))
                          (S.filter (function Int _ -> false | _ -> true) s)
                    | _ -> assert false)
                | _ -> S.add v s)
              s1 s2
          in
          Some s

    let join o1 o2 = join_gen IntervalDomain.join o1 o2
    let widen o1 o2 = join_gen IntervalDomain.widen o1 o2
    let widen _ o1 o2 = widen o1 o2

    let leq o1 o2 =
      match (o1, o2) with
      | _, None -> true
      | None, Some _ -> false
      | Some s1, Some s2 ->
          S.for_all
            (function
              | Bool _ as v1 -> S.mem v1 s2
              | Int i1 ->
                  S.exists
                    (function
                      | Bool _ -> false | Int i2 -> IntervalDomain.leq i1 i2)
                    s2)
            s1

    let as_bool = function
      | None -> AnyBool
      | Some s -> S.fold (fun v b -> abs_bool_join (V0.as_bool v) b) s NoBool

    let inject_val = function
      | Concrete.V.Bot -> bot
      | Top -> top
      | Int n -> inject (V0.Int (IntervalDomain.inject n))
      | Bool b -> inject (V0.Bool b)

    let meet o1 o2 =
      match (o1, o2) with
      | None, o | o, None -> o
      | Some s1, Some s2 ->
          let s =
            S.fold
              (fun v1 acc ->
                S.fold
                  (fun v2 acc ->
                    match (v1, v2) with
                    | Bool b1, Bool b2 -> if b1 = b2 then S.add v1 acc else acc
                    | Int i1, Int i2 ->
                        let i = IntervalDomain.meet i1 i2 in
                        if IntervalDomain.is_bot i then acc
                        else S.add (Int i) acc
                    | Bool _, Int _ | Int _, Bool _ -> acc)
                  s2 acc)
              s1 S.empty
          in
          Some s
  end

  module MakeI (G : GRAPH with type x := V.t list and type y := V.t) = struct
    type phi = G.t FMap.t

    let binop default f v1 v2 =
      if V.is_bot v1 || V.is_bot v2 then V.bot
      else
        match (v1, v2) with
        | None, _ | _, None -> default
        | Some vs1, Some vs2 ->
            V.S.fold
              (fun v1 acc ->
                V.S.fold (fun v2 acc -> V.join (f v1 v2) acc) vs2 acc)
              vs1 V.bot

    let int_op2_int f =
      binop (V.inject (V0.Int IntervalDomain.top)) (fun v1 v2 ->
          match (v1, v2) with
          | V0.Bool _, _ | _, V0.Bool _ -> V.bot
          | Int i1, Int i2 -> V.inject (V0.Int (f i1 i2)))

    let int_op2_bool f =
      let top_bool =
        V.(join (inject (V0.Bool true)) (inject (V0.Bool false)))
      in
      binop top_bool (fun v1 v2 ->
          match (v1, v2) with
          | V0.Bool _, _ | _, V0.Bool _ -> V.bot
          | Int i1, Int i2 -> (
              match f i1 i2 with
              | Some b -> V.inject (V0.Bool b)
              | None -> top_bool))

    let basic a vs =
      match (a, vs) with
      | Common.Bool b, [] -> V.inject (V0.Bool b)
      | Bool _, _ -> V.bot
      | Int i, [] -> V.inject (V0.Int (IntervalDomain.inject i))
      | Int _, _ -> V.bot
      | Plus, [ v1; v2 ] -> int_op2_int IntervalDomain.plus v1 v2
      | Plus, _ -> V.bot
      | Minus, [ v1; v2 ] -> int_op2_int IntervalDomain.minus v1 v2
      | Minus, _ -> V.bot
      | Mult, [ v1; v2 ] -> int_op2_int IntervalDomain.mult v1 v2
      | Mult, _ -> V.bot
      | Eq, [ v1; v2 ] -> int_op2_bool IntervalDomain.eq v1 v2
      | Eq, _ -> V.bot
      | Lt, [ v1; v2 ] -> int_op2_bool IntervalDomain.lt v1 v2
      | Lt, _ -> V.bot
      | Le, [ v1; v2 ] -> int_op2_bool IntervalDomain.le v1 v2
      | Le, _ -> V.bot
      | Gt, [ v1; v2 ] -> int_op2_bool IntervalDomain.gt v1 v2
      | Gt, _ -> V.bot
      | Ge, [ v1; v2 ] -> int_op2_bool IntervalDomain.ge v1 v2
      | Ge, _ -> V.bot

    let get_phi phi f vs =
      match FMap.find_opt f phi with
      | None -> V.bot
      | Some g -> ( match G.find_opt vs g with Some v -> v | None -> V.bot)

    let apply f phi vs =
      if List.exists V.is_bot vs then V.bot else get_phi phi f vs
  end
end

module AbstractGraphElementaryPartitioning (X : sig
  include ORDERED_TYPE
  include PP_TYPE with type t := t

  val is_bot : t -> bool
  val join : t -> t -> t
  val widen : int -> t -> t -> t
  val leq : t -> t -> bool
end) (Y : sig
  include PP_TYPE

  val bot : t
  val join : t -> t -> t
  val widen : int -> t -> t -> t
  val leq : t -> t -> bool
end) : GRAPH with type x := X.t list and type y := Y.t = struct
  module XS = struct
    include ListOf (X)

    let is_bot l = List.exists X.is_bot l
    let leq l1 l2 = List.for_all2 X.leq l1 l2
    let join l1 l2 = List.map2 X.join l1 l2
    let widen n l1 l2 = List.map2 (X.widen n) l1 l2
  end

  include Functional_partitioning.Elementary.Make (MapPP.Make) (XS) (Y)
end
