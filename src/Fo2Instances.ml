(**
  Author: Benoît Montagu <benoit.montagu@inria.fr>
  Copyright Inria © 2022
*)

open Sigs
open Fomfg2
open FoCommon

module Concrete = struct
  module V = Concrete.V
  module G = Graph (V) (V)
  module I = Concrete.MakeI (G)
  module MyMFG = MFG (X) (A) (F) (FMap) (V) (G) (I)

  let convert_c l : MyMFG.D.c =
    List.fold_left
      (fun acc (f, args) ->
        MyMFG.D.Phi.(join (FMap.singleton f (G.singleton args V.bot)) acc))
      MyMFG.D.Phi.bot l

  let run ~delay_widening prog init =
    let phi = MyMFG.program prog ~delay_widening (convert_c init) in
    Format.printf "%a@." MyMFG.D.Phi.pp phi
end

module MakeAbstract (V : sig
  include ORDERED_TYPE
  include PP_TYPE with type t := t

  val inject_val : Concrete.V.t -> t
  val as_bool : t -> Common.abs_bool
  val bot : t
  val is_bot : t -> bool
  val join : t -> t -> t
end)
(G : GRAPH with type x := V.t list and type y := V.t)
(MakeI : functor
  (G : GRAPH with type x := V.t list and type y := V.t)
  -> sig
  type phi = G.t FMap.t

  val basic : A.t -> V.t list -> V.t
  val apply : F.t -> phi -> V.t list -> V.t
end) : sig
  val run :
    delay_widening:int ->
    (string, Common.prim, string) FoCommon.program ->
    (string * FoCommon.Concrete.V.t list) list ->
    unit
end = struct
  module MyMFG = MFG (X) (A) (F) (FMap) (V) (G) (MakeI (G))

  let convert_c l : MyMFG.D.c =
    List.fold_left
      (fun acc (f, args) ->
        let g = G.singleton ((List.map V.inject_val) args) V.bot in
        MyMFG.D.Phi.(join (singleton f g) acc))
      MyMFG.D.Phi.bot l

  let run ~delay_widening prog init =
    let phi = MyMFG.program ~delay_widening prog (convert_c init) in
    Format.printf "%a@." MyMFG.D.Phi.pp phi
end
