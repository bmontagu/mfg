(**
  Author: Benoît Montagu <benoit.montagu@inria.fr>
  Copyright Inria © 2022
*)

open Sigs
open Common
open Fomfg
open FoCommon

module AbstractArgs (V : sig
  include ORDERED_TYPE
  include PP_TYPE with type t := t
end) : sig
  type t = V.t list option

  val singleton : V.t list -> t
end = struct
  module VS = ListOf (V)

  type t = VS.t option

  let singleton x = Some x
end

module MakeAbstract (V : sig
  include ORDERED_TYPE
  include PP_TYPE with type t := t

  val inject_val : Concrete.V.t -> t
  val as_bool : t -> Common.abs_bool
  val bot : t
  val is_bot : t -> bool
  val join : t -> t -> t
end)
(G : GRAPH with type x := V.t list and type y := V.t)
(MakeI : functor
  (G : GRAPH with type x := V.t list and type y := V.t)
  -> sig
  type phi = G.t FMap.t

  val basic : A.t -> V.t list -> V.t
  val apply : F.t -> phi -> V.t list -> V.t
end) : sig
  val run :
    delay_widening:int ->
    (string, Common.prim, string) FoCommon.program ->
    (string * FoCommon.Concrete.V.t list) list ->
    unit
end = struct
  module C = AbstractArgs (V)
  module I = MakeI (G)
  module MyMFG = MFG (X) (A) (F) (FMap) (V) (G) (I)

  let convert_c l : MyMFG.D.c =
    List.fold_left
      (fun acc (f, args) ->
        match C.singleton ((List.map V.inject_val) args) with
        | None -> acc
        | Some vs -> MyMFG.D.C.(join (singleton f vs) acc))
      MyMFG.D.C.bot l

  let run ~delay_widening prog init =
    let phi = MyMFG.program ~delay_widening prog (convert_c init) in
    Format.printf "%a@." MyMFG.D.Phi.pp phi
end

module Concrete = struct
  module V = Concrete.V
  module G = Graph (V) (V)
  module I = Concrete.MakeI (G)
  module MyMFG = MFG (X) (A) (F) (FMap) (V) (G) (I)

  let convert_c l : MyMFG.D.c =
    List.fold_left
      (fun acc (f, args) ->
        MyMFG.D.C.(join (FMap.singleton f (MyMFG.D.Args.singleton args)) acc))
      MyMFG.D.C.bot l

  let run ~delay_widening prog init =
    let phi = MyMFG.program ~delay_widening prog (convert_c init) in
    Format.printf "%a@." MyMFG.D.Phi.pp phi
end
