.PHONY:	build check clean test fmt

build:
	dune build @install

check:
	dune build @check

clean:
	dune clean

test:
	dune runtest --auto-promote --display=short

fmt:
	dune build @fmt --auto-promote
