(** @author: Benoît Montagu <benoit.montagu@inria.fr>
    Copyright Inria 2019
*)

module type CONFIG = sig
  val input_ext : string
  (** Extension of the input files. Must start with a period. *)

  val output_ext : string
  (** Extension for the output files. Must start with a period. *)

  val expected_ext : string
  (** Extention for the expected output files. Must start with a period. *)

  val exe : string
  (** Name of the executable to run for each test. *)

  val default_flags : string list
  (** Flags that should be given by default to the executable. *)

  val file_specific_flags : (string list * string list) list
  (** Flags that should be given to specific files. Files must be
     listed without their extensions. *)

  val excludes : string list
  (** Files in the following list are excluded. *)
end

module Make (Config : CONFIG) = struct
  open Config

  (** [flags_of file] gets the flags for the given [file] *)
  let flags_of file =
    let o_specific =
      List.find_opt
        (fun (files, _flags) -> List.mem file files)
        file_specific_flags
    in
    match o_specific with
    | Some (_files, flags) -> flags
    | None -> default_flags

  (** [print_flags chn] print the list of flags to the output channel
     [chn] *)
  let rec print_flags chn = function
    | [] -> ()
    | [flag] -> Printf.fprintf chn "%s" flag
    | flag :: flags ->
      Printf.fprintf chn "%s " flag;
      print_flags chn flags

  (** [print_rule chn file] prints the test rule for the file [file]
     to the output channel [chn] *)
  let print_rule chn file =
    Printf.fprintf
      chn
      "; Test rule for input file %s%s\n\
       (rule\n\
      \  (deps %s%s)\n\
      \  (target %s%s)\n\
      \  (action\n\
      \    (with-outputs-to %%{target} (run %s %a %%{deps}))))\n\
       (rule\n\
      \  (alias   runtest)\n\
      \  (action (diff %s%s %s%s)))\n"
      file
      input_ext
      file
      input_ext
      file
      output_ext
      exe
      print_flags
      (flags_of file)
      file
      expected_ext
      file
      output_ext

  (** [is_excluded s] returns [true] iff the string [s] belongs to the
     list [excludes] *)
  let is_excluded =
    let module StringSet = Set.Make (String) in
    let excludes =
      List.fold_left (fun acc s -> StringSet.add s acc) StringSet.empty excludes
    in
    fun s -> StringSet.mem s excludes

  (** [get_files dir] gets the files that are present in the directory
     [dir] and that end with the suffix input_ext, and removes that
     suffix from those files. The returned list is sorted according to
     the ordering [String.compare]. *)
  let get_files dir =
    let all_files = Sys.readdir dir in
    Array.fold_right
      (fun file acc ->
        let file = Filename.basename file in
        if String.equal (Filename.extension file) input_ext
        then
          let short = Filename.remove_extension file in
          if is_excluded short then acc else short :: acc
        else acc)
      all_files
      []
    |> List.sort String.compare

  (* we sort the result so that we get consistent output on different
     machines (Sys.readdir does not guarantee any ordering on the
     array of returned files) *)

  (** [generate_rules chn dir] generates the rules for all the files
     in the directory [dir], and prints them to the output channel
     [chn] *)
  let generate_rules chn dir =
    get_files dir
    |> List.iter (fun file -> Printf.fprintf chn "%a\n%!" print_rule file)
end
