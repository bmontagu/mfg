; Test rule for input file prog1.simpl
(rule
  (deps prog1.simpl)
  (target prog1.out-concrete)
  (action
    (with-outputs-to %{target} (run mfg --fo --concrete %{deps}))))
(rule
  (alias   runtest)
  (action (diff prog1.expected-concrete prog1.out-concrete)))

; Test rule for input file prog2.simpl
(rule
  (deps prog2.simpl)
  (target prog2.out-concrete)
  (action
    (with-outputs-to %{target} (run mfg --fo --concrete %{deps}))))
(rule
  (alias   runtest)
  (action (diff prog2.expected-concrete prog2.out-concrete)))

; Test rule for input file prog3.simpl
(rule
  (deps prog3.simpl)
  (target prog3.out-concrete)
  (action
    (with-outputs-to %{target} (run mfg --fo --concrete %{deps}))))
(rule
  (alias   runtest)
  (action (diff prog3.expected-concrete prog3.out-concrete)))

; Test rule for input file prog5.simpl
(rule
  (deps prog5.simpl)
  (target prog5.out-concrete)
  (action
    (with-outputs-to %{target} (run mfg --fo --concrete %{deps}))))
(rule
  (alias   runtest)
  (action (diff prog5.expected-concrete prog5.out-concrete)))

; Test rule for input file prog7.simpl
(rule
  (deps prog7.simpl)
  (target prog7.out-concrete)
  (action
    (with-outputs-to %{target} (run mfg --fo --concrete %{deps}))))
(rule
  (alias   runtest)
  (action (diff prog7.expected-concrete prog7.out-concrete)))

; Test rule for input file prog8.simpl
(rule
  (deps prog8.simpl)
  (target prog8.out-concrete)
  (action
    (with-outputs-to %{target} (run mfg --fo --concrete %{deps}))))
(rule
  (alias   runtest)
  (action (diff prog8.expected-concrete prog8.out-concrete)))

; Test rule for input file prog9.simpl
(rule
  (deps prog9.simpl)
  (target prog9.out-concrete)
  (action
    (with-outputs-to %{target} (run mfg --fo --concrete %{deps}))))
(rule
  (alias   runtest)
  (action (diff prog9.expected-concrete prog9.out-concrete)))

; Test rule for input file prog1.simpl
(rule
  (deps prog1.simpl)
  (target prog1.out-type)
  (action
    (with-outputs-to %{target} (run mfg --fo --type-analysis %{deps}))))
(rule
  (alias   runtest)
  (action (diff prog1.expected-type prog1.out-type)))

; Test rule for input file prog2.simpl
(rule
  (deps prog2.simpl)
  (target prog2.out-type)
  (action
    (with-outputs-to %{target} (run mfg --fo --type-analysis %{deps}))))
(rule
  (alias   runtest)
  (action (diff prog2.expected-type prog2.out-type)))

; Test rule for input file prog3.simpl
(rule
  (deps prog3.simpl)
  (target prog3.out-type)
  (action
    (with-outputs-to %{target} (run mfg --fo --type-analysis %{deps}))))
(rule
  (alias   runtest)
  (action (diff prog3.expected-type prog3.out-type)))

; Test rule for input file prog5.simpl
(rule
  (deps prog5.simpl)
  (target prog5.out-type)
  (action
    (with-outputs-to %{target} (run mfg --fo --type-analysis %{deps}))))
(rule
  (alias   runtest)
  (action (diff prog5.expected-type prog5.out-type)))

; Test rule for input file prog7.simpl
(rule
  (deps prog7.simpl)
  (target prog7.out-type)
  (action
    (with-outputs-to %{target} (run mfg --fo --type-analysis %{deps}))))
(rule
  (alias   runtest)
  (action (diff prog7.expected-type prog7.out-type)))

; Test rule for input file prog8.simpl
(rule
  (deps prog8.simpl)
  (target prog8.out-type)
  (action
    (with-outputs-to %{target} (run mfg --fo --type-analysis %{deps}))))
(rule
  (alias   runtest)
  (action (diff prog8.expected-type prog8.out-type)))

; Test rule for input file prog9.simpl
(rule
  (deps prog9.simpl)
  (target prog9.out-type)
  (action
    (with-outputs-to %{target} (run mfg --fo --type-analysis %{deps}))))
(rule
  (alias   runtest)
  (action (diff prog9.expected-type prog9.out-type)))

; Test rule for input file prog1.simpl
(rule
  (deps prog1.simpl)
  (target prog1.out-const)
  (action
    (with-outputs-to %{target} (run mfg --fo --constant-analysis %{deps}))))
(rule
  (alias   runtest)
  (action (diff prog1.expected-const prog1.out-const)))

; Test rule for input file prog2.simpl
(rule
  (deps prog2.simpl)
  (target prog2.out-const)
  (action
    (with-outputs-to %{target} (run mfg --fo --constant-analysis %{deps}))))
(rule
  (alias   runtest)
  (action (diff prog2.expected-const prog2.out-const)))

; Test rule for input file prog3.simpl
(rule
  (deps prog3.simpl)
  (target prog3.out-const)
  (action
    (with-outputs-to %{target} (run mfg --fo --constant-analysis %{deps}))))
(rule
  (alias   runtest)
  (action (diff prog3.expected-const prog3.out-const)))

; Test rule for input file prog5.simpl
(rule
  (deps prog5.simpl)
  (target prog5.out-const)
  (action
    (with-outputs-to %{target} (run mfg --fo --constant-analysis %{deps}))))
(rule
  (alias   runtest)
  (action (diff prog5.expected-const prog5.out-const)))

; Test rule for input file prog7.simpl
(rule
  (deps prog7.simpl)
  (target prog7.out-const)
  (action
    (with-outputs-to %{target} (run mfg --fo --constant-analysis %{deps}))))
(rule
  (alias   runtest)
  (action (diff prog7.expected-const prog7.out-const)))

; Test rule for input file prog8.simpl
(rule
  (deps prog8.simpl)
  (target prog8.out-const)
  (action
    (with-outputs-to %{target} (run mfg --fo --constant-analysis %{deps}))))
(rule
  (alias   runtest)
  (action (diff prog8.expected-const prog8.out-const)))

; Test rule for input file prog9.simpl
(rule
  (deps prog9.simpl)
  (target prog9.out-const)
  (action
    (with-outputs-to %{target} (run mfg --fo --constant-analysis %{deps}))))
(rule
  (alias   runtest)
  (action (diff prog9.expected-const prog9.out-const)))

; Test rule for input file prog1.simpl
(rule
  (deps prog1.simpl)
  (target prog1.out-interval)
  (action
    (with-outputs-to %{target} (run mfg --fo --interval-analysis --delay-widening=2 %{deps}))))
(rule
  (alias   runtest)
  (action (diff prog1.expected-interval prog1.out-interval)))

; Test rule for input file prog2.simpl
(rule
  (deps prog2.simpl)
  (target prog2.out-interval)
  (action
    (with-outputs-to %{target} (run mfg --fo --interval-analysis --delay-widening=2 %{deps}))))
(rule
  (alias   runtest)
  (action (diff prog2.expected-interval prog2.out-interval)))

; Test rule for input file prog3.simpl
(rule
  (deps prog3.simpl)
  (target prog3.out-interval)
  (action
    (with-outputs-to %{target} (run mfg --fo --interval-analysis --delay-widening=2 %{deps}))))
(rule
  (alias   runtest)
  (action (diff prog3.expected-interval prog3.out-interval)))

; Test rule for input file prog5.simpl
(rule
  (deps prog5.simpl)
  (target prog5.out-interval)
  (action
    (with-outputs-to %{target} (run mfg --fo --interval-analysis --delay-widening=2 %{deps}))))
(rule
  (alias   runtest)
  (action (diff prog5.expected-interval prog5.out-interval)))

; Test rule for input file prog7.simpl
(rule
  (deps prog7.simpl)
  (target prog7.out-interval)
  (action
    (with-outputs-to %{target} (run mfg --fo --interval-analysis --delay-widening=2 %{deps}))))
(rule
  (alias   runtest)
  (action (diff prog7.expected-interval prog7.out-interval)))

; Test rule for input file prog8.simpl
(rule
  (deps prog8.simpl)
  (target prog8.out-interval)
  (action
    (with-outputs-to %{target} (run mfg --fo --interval-analysis --delay-widening=2 %{deps}))))
(rule
  (alias   runtest)
  (action (diff prog8.expected-interval prog8.out-interval)))

; Test rule for input file prog9.simpl
(rule
  (deps prog9.simpl)
  (target prog9.out-interval)
  (action
    (with-outputs-to %{target} (run mfg --fo --interval-analysis --delay-widening=2 %{deps}))))
(rule
  (alias   runtest)
  (action (diff prog9.expected-interval prog9.out-interval)))

; Test rule for input file prog1.simpl
(rule
  (deps prog1.simpl)
  (target prog1.out-concrete2)
  (action
    (with-outputs-to %{target} (run mfg --fo2 --concrete %{deps}))))
(rule
  (alias   runtest)
  (action (diff prog1.expected-concrete2 prog1.out-concrete2)))

; Test rule for input file prog2.simpl
(rule
  (deps prog2.simpl)
  (target prog2.out-concrete2)
  (action
    (with-outputs-to %{target} (run mfg --fo2 --concrete %{deps}))))
(rule
  (alias   runtest)
  (action (diff prog2.expected-concrete2 prog2.out-concrete2)))

; Test rule for input file prog3.simpl
(rule
  (deps prog3.simpl)
  (target prog3.out-concrete2)
  (action
    (with-outputs-to %{target} (run mfg --fo2 --concrete %{deps}))))
(rule
  (alias   runtest)
  (action (diff prog3.expected-concrete2 prog3.out-concrete2)))

; Test rule for input file prog5.simpl
(rule
  (deps prog5.simpl)
  (target prog5.out-concrete2)
  (action
    (with-outputs-to %{target} (run mfg --fo2 --concrete %{deps}))))
(rule
  (alias   runtest)
  (action (diff prog5.expected-concrete2 prog5.out-concrete2)))

; Test rule for input file prog7.simpl
(rule
  (deps prog7.simpl)
  (target prog7.out-concrete2)
  (action
    (with-outputs-to %{target} (run mfg --fo2 --concrete %{deps}))))
(rule
  (alias   runtest)
  (action (diff prog7.expected-concrete2 prog7.out-concrete2)))

; Test rule for input file prog8.simpl
(rule
  (deps prog8.simpl)
  (target prog8.out-concrete2)
  (action
    (with-outputs-to %{target} (run mfg --fo2 --concrete %{deps}))))
(rule
  (alias   runtest)
  (action (diff prog8.expected-concrete2 prog8.out-concrete2)))

; Test rule for input file prog9.simpl
(rule
  (deps prog9.simpl)
  (target prog9.out-concrete2)
  (action
    (with-outputs-to %{target} (run mfg --fo2 --concrete %{deps}))))
(rule
  (alias   runtest)
  (action (diff prog9.expected-concrete2 prog9.out-concrete2)))

; Test rule for input file prog1.simpl
(rule
  (deps prog1.simpl)
  (target prog1.out-type2)
  (action
    (with-outputs-to %{target} (run mfg --fo2 --type-analysis %{deps}))))
(rule
  (alias   runtest)
  (action (diff prog1.expected-type2 prog1.out-type2)))

; Test rule for input file prog2.simpl
(rule
  (deps prog2.simpl)
  (target prog2.out-type2)
  (action
    (with-outputs-to %{target} (run mfg --fo2 --type-analysis %{deps}))))
(rule
  (alias   runtest)
  (action (diff prog2.expected-type2 prog2.out-type2)))

; Test rule for input file prog3.simpl
(rule
  (deps prog3.simpl)
  (target prog3.out-type2)
  (action
    (with-outputs-to %{target} (run mfg --fo2 --type-analysis %{deps}))))
(rule
  (alias   runtest)
  (action (diff prog3.expected-type2 prog3.out-type2)))

; Test rule for input file prog5.simpl
(rule
  (deps prog5.simpl)
  (target prog5.out-type2)
  (action
    (with-outputs-to %{target} (run mfg --fo2 --type-analysis %{deps}))))
(rule
  (alias   runtest)
  (action (diff prog5.expected-type2 prog5.out-type2)))

; Test rule for input file prog7.simpl
(rule
  (deps prog7.simpl)
  (target prog7.out-type2)
  (action
    (with-outputs-to %{target} (run mfg --fo2 --type-analysis %{deps}))))
(rule
  (alias   runtest)
  (action (diff prog7.expected-type2 prog7.out-type2)))

; Test rule for input file prog8.simpl
(rule
  (deps prog8.simpl)
  (target prog8.out-type2)
  (action
    (with-outputs-to %{target} (run mfg --fo2 --type-analysis %{deps}))))
(rule
  (alias   runtest)
  (action (diff prog8.expected-type2 prog8.out-type2)))

; Test rule for input file prog9.simpl
(rule
  (deps prog9.simpl)
  (target prog9.out-type2)
  (action
    (with-outputs-to %{target} (run mfg --fo2 --type-analysis %{deps}))))
(rule
  (alias   runtest)
  (action (diff prog9.expected-type2 prog9.out-type2)))

; Test rule for input file prog1.simpl
(rule
  (deps prog1.simpl)
  (target prog1.out-const2)
  (action
    (with-outputs-to %{target} (run mfg --fo2 --constant-analysis %{deps}))))
(rule
  (alias   runtest)
  (action (diff prog1.expected-const2 prog1.out-const2)))

; Test rule for input file prog2.simpl
(rule
  (deps prog2.simpl)
  (target prog2.out-const2)
  (action
    (with-outputs-to %{target} (run mfg --fo2 --constant-analysis %{deps}))))
(rule
  (alias   runtest)
  (action (diff prog2.expected-const2 prog2.out-const2)))

; Test rule for input file prog3.simpl
(rule
  (deps prog3.simpl)
  (target prog3.out-const2)
  (action
    (with-outputs-to %{target} (run mfg --fo2 --constant-analysis %{deps}))))
(rule
  (alias   runtest)
  (action (diff prog3.expected-const2 prog3.out-const2)))

; Test rule for input file prog5.simpl
(rule
  (deps prog5.simpl)
  (target prog5.out-const2)
  (action
    (with-outputs-to %{target} (run mfg --fo2 --constant-analysis %{deps}))))
(rule
  (alias   runtest)
  (action (diff prog5.expected-const2 prog5.out-const2)))

; Test rule for input file prog7.simpl
(rule
  (deps prog7.simpl)
  (target prog7.out-const2)
  (action
    (with-outputs-to %{target} (run mfg --fo2 --constant-analysis %{deps}))))
(rule
  (alias   runtest)
  (action (diff prog7.expected-const2 prog7.out-const2)))

; Test rule for input file prog8.simpl
(rule
  (deps prog8.simpl)
  (target prog8.out-const2)
  (action
    (with-outputs-to %{target} (run mfg --fo2 --constant-analysis %{deps}))))
(rule
  (alias   runtest)
  (action (diff prog8.expected-const2 prog8.out-const2)))

; Test rule for input file prog9.simpl
(rule
  (deps prog9.simpl)
  (target prog9.out-const2)
  (action
    (with-outputs-to %{target} (run mfg --fo2 --constant-analysis %{deps}))))
(rule
  (alias   runtest)
  (action (diff prog9.expected-const2 prog9.out-const2)))

; Test rule for input file prog1.simpl
(rule
  (deps prog1.simpl)
  (target prog1.out-interval2)
  (action
    (with-outputs-to %{target} (run mfg --fo2 --interval-analysis --delay-widening=2 %{deps}))))
(rule
  (alias   runtest)
  (action (diff prog1.expected-interval2 prog1.out-interval2)))

; Test rule for input file prog2.simpl
(rule
  (deps prog2.simpl)
  (target prog2.out-interval2)
  (action
    (with-outputs-to %{target} (run mfg --fo2 --interval-analysis --delay-widening=2 %{deps}))))
(rule
  (alias   runtest)
  (action (diff prog2.expected-interval2 prog2.out-interval2)))

; Test rule for input file prog3.simpl
(rule
  (deps prog3.simpl)
  (target prog3.out-interval2)
  (action
    (with-outputs-to %{target} (run mfg --fo2 --interval-analysis --delay-widening=2 %{deps}))))
(rule
  (alias   runtest)
  (action (diff prog3.expected-interval2 prog3.out-interval2)))

; Test rule for input file prog5.simpl
(rule
  (deps prog5.simpl)
  (target prog5.out-interval2)
  (action
    (with-outputs-to %{target} (run mfg --fo2 --interval-analysis --delay-widening=2 %{deps}))))
(rule
  (alias   runtest)
  (action (diff prog5.expected-interval2 prog5.out-interval2)))

; Test rule for input file prog7.simpl
(rule
  (deps prog7.simpl)
  (target prog7.out-interval2)
  (action
    (with-outputs-to %{target} (run mfg --fo2 --interval-analysis --delay-widening=2 %{deps}))))
(rule
  (alias   runtest)
  (action (diff prog7.expected-interval2 prog7.out-interval2)))

; Test rule for input file prog8.simpl
(rule
  (deps prog8.simpl)
  (target prog8.out-interval2)
  (action
    (with-outputs-to %{target} (run mfg --fo2 --interval-analysis --delay-widening=2 %{deps}))))
(rule
  (alias   runtest)
  (action (diff prog8.expected-interval2 prog8.out-interval2)))

; Test rule for input file prog9.simpl
(rule
  (deps prog9.simpl)
  (target prog9.out-interval2)
  (action
    (with-outputs-to %{target} (run mfg --fo2 --interval-analysis --delay-widening=2 %{deps}))))
(rule
  (alias   runtest)
  (action (diff prog9.expected-interval2 prog9.out-interval2)))

