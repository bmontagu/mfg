(*****************************************************************************)

(* configuration for analyzer "FO concrete" *)
module ConfigFOConcrete = struct
  (* extension of the input files *)
  let input_ext = ".simpl"

  (* extension for the output files *)
  let output_ext = ".out-concrete"

  (* extention for the expected output files *)
  let expected_ext = ".expected-concrete"

  (* name of the executable to run for each test *)
  let exe = "mfg"

  (* flags that should be given by default to the executable *)
  let default_flags = [ "--fo"; "--concrete" ]

  (* flags that should be given to specific files *)
  let file_specific_flags = []

  (* files in the following list are excluded *)
  let excludes = []
end

(* generate the rules for the files in the current directory and
   prints them to standard output *)
let () =
  let module M = DuneTestGen.Make (ConfigFOConcrete) in
  M.generate_rules stdout Filename.current_dir_name

(*****************************************************************************)

(* configuration for analyzer "FO type-analysis" *)
module ConfigFOType = struct
  (* extension of the input files *)
  let input_ext = ".simpl"

  (* extension for the output files *)
  let output_ext = ".out-type"

  (* extention for the expected output files *)
  let expected_ext = ".expected-type"

  (* name of the executable to run for each test *)
  let exe = "mfg"

  (* flags that should be given by default to the executable *)
  let default_flags = [ "--fo"; "--type-analysis" ]

  (* flags that should be given to specific files *)
  let file_specific_flags = []

  (* files in the following list are excluded *)
  let excludes = []
end

(* generate the rules for the files in the current directory and
   prints them to standard output *)
let () =
  let module M = DuneTestGen.Make (ConfigFOType) in
  M.generate_rules stdout Filename.current_dir_name

(*****************************************************************************)

(* configuration for analyzer "FO constant-analysis" *)
module ConfigFOConst = struct
  (* extension of the input files *)
  let input_ext = ".simpl"

  (* extension for the output files *)
  let output_ext = ".out-const"

  (* extention for the expected output files *)
  let expected_ext = ".expected-const"

  (* name of the executable to run for each test *)
  let exe = "mfg"

  (* flags that should be given by default to the executable *)
  let default_flags = [ "--fo"; "--constant-analysis" ]

  (* flags that should be given to specific files *)
  let file_specific_flags = []

  (* files in the following list are excluded *)
  let excludes = []
end

(* generate the rules for the files in the current directory and
   prints them to standard output *)
let () =
  let module M = DuneTestGen.Make (ConfigFOConst) in
  M.generate_rules stdout Filename.current_dir_name

(*****************************************************************************)

(* configuration for analyzer "FO interval-analysis" *)
module ConfigFOInterval = struct
  (* extension of the input files *)
  let input_ext = ".simpl"

  (* extension for the output files *)
  let output_ext = ".out-interval"

  (* extention for the expected output files *)
  let expected_ext = ".expected-interval"

  (* name of the executable to run for each test *)
  let exe = "mfg"

  (* flags that should be given by default to the executable *)
  let default_flags = [ "--fo"; "--interval-analysis"; "--delay-widening=2" ]

  (* flags that should be given to specific files *)
  let file_specific_flags = []

  (* files in the following list are excluded *)
  let excludes = []
end

(* generate the rules for the files in the current directory and
   prints them to standard output *)
let () =
  let module M = DuneTestGen.Make (ConfigFOInterval) in
  M.generate_rules stdout Filename.current_dir_name

(*****************************************************************************)

(* configuration for analyzer "FO2 concrete" *)
module ConfigFO2Concrete = struct
  (* extension of the input files *)
  let input_ext = ".simpl"

  (* extension for the output files *)
  let output_ext = ".out-concrete2"

  (* extention for the expected output files *)
  let expected_ext = ".expected-concrete2"

  (* name of the executable to run for each test *)
  let exe = "mfg"

  (* flags that should be given by default to the executable *)
  let default_flags = [ "--fo2"; "--concrete" ]

  (* flags that should be given to specific files *)
  let file_specific_flags = []

  (* files in the following list are excluded *)
  let excludes = []
end

(* generate the rules for the files in the current directory and
   prints them to standard output *)
let () =
  let module M = DuneTestGen.Make (ConfigFO2Concrete) in
  M.generate_rules stdout Filename.current_dir_name

(*****************************************************************************)

(* configuration for analyzer "FO2 type-analysis" *)
module ConfigFO2Type = struct
  (* extension of the input files *)
  let input_ext = ".simpl"

  (* extension for the output files *)
  let output_ext = ".out-type2"

  (* extention for the expected output files *)
  let expected_ext = ".expected-type2"

  (* name of the executable to run for each test *)
  let exe = "mfg"

  (* flags that should be given by default to the executable *)
  let default_flags = [ "--fo2"; "--type-analysis" ]

  (* flags that should be given to specific files *)
  let file_specific_flags = []

  (* files in the following list are excluded *)
  let excludes = []
end

(* generate the rules for the files in the current directory and
   prints them to standard output *)
let () =
  let module M = DuneTestGen.Make (ConfigFO2Type) in
  M.generate_rules stdout Filename.current_dir_name

(*****************************************************************************)

(* configuration for analyzer "FO2 constant-analysis" *)
module ConfigFO2Const = struct
  (* extension of the input files *)
  let input_ext = ".simpl"

  (* extension for the output files *)
  let output_ext = ".out-const2"

  (* extention for the expected output files *)
  let expected_ext = ".expected-const2"

  (* name of the executable to run for each test *)
  let exe = "mfg"

  (* flags that should be given by default to the executable *)
  let default_flags = [ "--fo2"; "--constant-analysis" ]

  (* flags that should be given to specific files *)
  let file_specific_flags = []

  (* files in the following list are excluded *)
  let excludes = []
end

(* generate the rules for the files in the current directory and
   prints them to standard output *)
let () =
  let module M = DuneTestGen.Make (ConfigFO2Const) in
  M.generate_rules stdout Filename.current_dir_name

(*****************************************************************************)

(* configuration for analyzer "FO2 interval-analysis" *)
module ConfigFO2Interval = struct
  (* extension of the input files *)
  let input_ext = ".simpl"

  (* extension for the output files *)
  let output_ext = ".out-interval2"

  (* extention for the expected output files *)
  let expected_ext = ".expected-interval2"

  (* name of the executable to run for each test *)
  let exe = "mfg"

  (* flags that should be given by default to the executable *)
  let default_flags = [ "--fo2"; "--interval-analysis"; "--delay-widening=2" ]

  (* flags that should be given to specific files *)
  let file_specific_flags = []

  (* files in the following list are excluded *)
  let excludes = []
end

(* generate the rules for the files in the current directory and
   prints them to standard output *)
let () =
  let module M = DuneTestGen.Make (ConfigFO2Interval) in
  M.generate_rules stdout Filename.current_dir_name

(*****************************************************************************)
