; Test rule for input file heintze_mcallester.simpl
(rule
  (deps heintze_mcallester.simpl)
  (target heintze_mcallester.out-concrete)
  (action
    (with-outputs-to %{target} (run mfg --ho --concrete %{deps}))))
(rule
  (alias   runtest)
  (action (diff heintze_mcallester.expected-concrete heintze_mcallester.out-concrete)))

; Test rule for input file iter_compose.simpl
(rule
  (deps iter_compose.simpl)
  (target iter_compose.out-concrete)
  (action
    (with-outputs-to %{target} (run mfg --ho --concrete %{deps}))))
(rule
  (alias   runtest)
  (action (diff iter_compose.expected-concrete iter_compose.out-concrete)))

; Test rule for input file iter_compose_bool.simpl
(rule
  (deps iter_compose_bool.simpl)
  (target iter_compose_bool.out-concrete)
  (action
    (with-outputs-to %{target} (run mfg --ho --concrete %{deps}))))
(rule
  (alias   runtest)
  (action (diff iter_compose_bool.expected-concrete iter_compose_bool.out-concrete)))

; Test rule for input file iter_compose_int.simpl
(rule
  (deps iter_compose_int.simpl)
  (target iter_compose_int.out-concrete)
  (action
    (with-outputs-to %{target} (run mfg --ho --concrete %{deps}))))
(rule
  (alias   runtest)
  (action (diff iter_compose_int.expected-concrete iter_compose_int.out-concrete)))

; Test rule for input file prog1.simpl
(rule
  (deps prog1.simpl)
  (target prog1.out-concrete)
  (action
    (with-outputs-to %{target} (run mfg --ho --concrete %{deps}))))
(rule
  (alias   runtest)
  (action (diff prog1.expected-concrete prog1.out-concrete)))

; Test rule for input file prog2.simpl
(rule
  (deps prog2.simpl)
  (target prog2.out-concrete)
  (action
    (with-outputs-to %{target} (run mfg --ho --concrete %{deps}))))
(rule
  (alias   runtest)
  (action (diff prog2.expected-concrete prog2.out-concrete)))

; Test rule for input file prog3.simpl
(rule
  (deps prog3.simpl)
  (target prog3.out-concrete)
  (action
    (with-outputs-to %{target} (run mfg --ho --concrete %{deps}))))
(rule
  (alias   runtest)
  (action (diff prog3.expected-concrete prog3.out-concrete)))

; Test rule for input file prog4.simpl
(rule
  (deps prog4.simpl)
  (target prog4.out-concrete)
  (action
    (with-outputs-to %{target} (run mfg --ho --concrete %{deps}))))
(rule
  (alias   runtest)
  (action (diff prog4.expected-concrete prog4.out-concrete)))

; Test rule for input file prog5.simpl
(rule
  (deps prog5.simpl)
  (target prog5.out-concrete)
  (action
    (with-outputs-to %{target} (run mfg --ho --concrete %{deps}))))
(rule
  (alias   runtest)
  (action (diff prog5.expected-concrete prog5.out-concrete)))

; Test rule for input file prog6.simpl
(rule
  (deps prog6.simpl)
  (target prog6.out-concrete)
  (action
    (with-outputs-to %{target} (run mfg --ho --concrete %{deps}))))
(rule
  (alias   runtest)
  (action (diff prog6.expected-concrete prog6.out-concrete)))

; Test rule for input file prog7.simpl
(rule
  (deps prog7.simpl)
  (target prog7.out-concrete)
  (action
    (with-outputs-to %{target} (run mfg --ho --concrete %{deps}))))
(rule
  (alias   runtest)
  (action (diff prog7.expected-concrete prog7.out-concrete)))

; Test rule for input file prog8.simpl
(rule
  (deps prog8.simpl)
  (target prog8.out-concrete)
  (action
    (with-outputs-to %{target} (run mfg --ho --concrete %{deps}))))
(rule
  (alias   runtest)
  (action (diff prog8.expected-concrete prog8.out-concrete)))

; Test rule for input file prog9.simpl
(rule
  (deps prog9.simpl)
  (target prog9.out-concrete)
  (action
    (with-outputs-to %{target} (run mfg --ho --concrete %{deps}))))
(rule
  (alias   runtest)
  (action (diff prog9.expected-concrete prog9.out-concrete)))

; Test rule for input file heintze_mcallester.simpl
(rule
  (deps heintze_mcallester.simpl)
  (target heintze_mcallester.out-type)
  (action
    (with-outputs-to %{target} (run mfg --ho --type-analysis %{deps}))))
(rule
  (alias   runtest)
  (action (diff heintze_mcallester.expected-type heintze_mcallester.out-type)))

; Test rule for input file iter_compose.simpl
(rule
  (deps iter_compose.simpl)
  (target iter_compose.out-type)
  (action
    (with-outputs-to %{target} (run mfg --ho --type-analysis %{deps}))))
(rule
  (alias   runtest)
  (action (diff iter_compose.expected-type iter_compose.out-type)))

; Test rule for input file iter_compose_bool.simpl
(rule
  (deps iter_compose_bool.simpl)
  (target iter_compose_bool.out-type)
  (action
    (with-outputs-to %{target} (run mfg --ho --type-analysis %{deps}))))
(rule
  (alias   runtest)
  (action (diff iter_compose_bool.expected-type iter_compose_bool.out-type)))

; Test rule for input file iter_compose_int.simpl
(rule
  (deps iter_compose_int.simpl)
  (target iter_compose_int.out-type)
  (action
    (with-outputs-to %{target} (run mfg --ho --type-analysis %{deps}))))
(rule
  (alias   runtest)
  (action (diff iter_compose_int.expected-type iter_compose_int.out-type)))

; Test rule for input file prog1.simpl
(rule
  (deps prog1.simpl)
  (target prog1.out-type)
  (action
    (with-outputs-to %{target} (run mfg --ho --type-analysis %{deps}))))
(rule
  (alias   runtest)
  (action (diff prog1.expected-type prog1.out-type)))

; Test rule for input file prog2.simpl
(rule
  (deps prog2.simpl)
  (target prog2.out-type)
  (action
    (with-outputs-to %{target} (run mfg --ho --type-analysis %{deps}))))
(rule
  (alias   runtest)
  (action (diff prog2.expected-type prog2.out-type)))

; Test rule for input file prog3.simpl
(rule
  (deps prog3.simpl)
  (target prog3.out-type)
  (action
    (with-outputs-to %{target} (run mfg --ho --type-analysis %{deps}))))
(rule
  (alias   runtest)
  (action (diff prog3.expected-type prog3.out-type)))

; Test rule for input file prog4.simpl
(rule
  (deps prog4.simpl)
  (target prog4.out-type)
  (action
    (with-outputs-to %{target} (run mfg --ho --type-analysis %{deps}))))
(rule
  (alias   runtest)
  (action (diff prog4.expected-type prog4.out-type)))

; Test rule for input file prog5.simpl
(rule
  (deps prog5.simpl)
  (target prog5.out-type)
  (action
    (with-outputs-to %{target} (run mfg --ho --type-analysis %{deps}))))
(rule
  (alias   runtest)
  (action (diff prog5.expected-type prog5.out-type)))

; Test rule for input file prog6.simpl
(rule
  (deps prog6.simpl)
  (target prog6.out-type)
  (action
    (with-outputs-to %{target} (run mfg --ho --type-analysis %{deps}))))
(rule
  (alias   runtest)
  (action (diff prog6.expected-type prog6.out-type)))

; Test rule for input file prog7.simpl
(rule
  (deps prog7.simpl)
  (target prog7.out-type)
  (action
    (with-outputs-to %{target} (run mfg --ho --type-analysis %{deps}))))
(rule
  (alias   runtest)
  (action (diff prog7.expected-type prog7.out-type)))

; Test rule for input file prog8.simpl
(rule
  (deps prog8.simpl)
  (target prog8.out-type)
  (action
    (with-outputs-to %{target} (run mfg --ho --type-analysis %{deps}))))
(rule
  (alias   runtest)
  (action (diff prog8.expected-type prog8.out-type)))

; Test rule for input file prog9.simpl
(rule
  (deps prog9.simpl)
  (target prog9.out-type)
  (action
    (with-outputs-to %{target} (run mfg --ho --type-analysis %{deps}))))
(rule
  (alias   runtest)
  (action (diff prog9.expected-type prog9.out-type)))

; Test rule for input file heintze_mcallester.simpl
(rule
  (deps heintze_mcallester.simpl)
  (target heintze_mcallester.out-const)
  (action
    (with-outputs-to %{target} (run mfg --ho --constant-analysis %{deps}))))
(rule
  (alias   runtest)
  (action (diff heintze_mcallester.expected-const heintze_mcallester.out-const)))

; Test rule for input file iter_compose.simpl
(rule
  (deps iter_compose.simpl)
  (target iter_compose.out-const)
  (action
    (with-outputs-to %{target} (run mfg --ho --constant-analysis %{deps}))))
(rule
  (alias   runtest)
  (action (diff iter_compose.expected-const iter_compose.out-const)))

; Test rule for input file iter_compose_bool.simpl
(rule
  (deps iter_compose_bool.simpl)
  (target iter_compose_bool.out-const)
  (action
    (with-outputs-to %{target} (run mfg --ho --constant-analysis %{deps}))))
(rule
  (alias   runtest)
  (action (diff iter_compose_bool.expected-const iter_compose_bool.out-const)))

; Test rule for input file iter_compose_int.simpl
(rule
  (deps iter_compose_int.simpl)
  (target iter_compose_int.out-const)
  (action
    (with-outputs-to %{target} (run mfg --ho --constant-analysis %{deps}))))
(rule
  (alias   runtest)
  (action (diff iter_compose_int.expected-const iter_compose_int.out-const)))

; Test rule for input file prog1.simpl
(rule
  (deps prog1.simpl)
  (target prog1.out-const)
  (action
    (with-outputs-to %{target} (run mfg --ho --constant-analysis %{deps}))))
(rule
  (alias   runtest)
  (action (diff prog1.expected-const prog1.out-const)))

; Test rule for input file prog2.simpl
(rule
  (deps prog2.simpl)
  (target prog2.out-const)
  (action
    (with-outputs-to %{target} (run mfg --ho --constant-analysis %{deps}))))
(rule
  (alias   runtest)
  (action (diff prog2.expected-const prog2.out-const)))

; Test rule for input file prog3.simpl
(rule
  (deps prog3.simpl)
  (target prog3.out-const)
  (action
    (with-outputs-to %{target} (run mfg --ho --constant-analysis %{deps}))))
(rule
  (alias   runtest)
  (action (diff prog3.expected-const prog3.out-const)))

; Test rule for input file prog4.simpl
(rule
  (deps prog4.simpl)
  (target prog4.out-const)
  (action
    (with-outputs-to %{target} (run mfg --ho --constant-analysis %{deps}))))
(rule
  (alias   runtest)
  (action (diff prog4.expected-const prog4.out-const)))

; Test rule for input file prog5.simpl
(rule
  (deps prog5.simpl)
  (target prog5.out-const)
  (action
    (with-outputs-to %{target} (run mfg --ho --constant-analysis %{deps}))))
(rule
  (alias   runtest)
  (action (diff prog5.expected-const prog5.out-const)))

; Test rule for input file prog6.simpl
(rule
  (deps prog6.simpl)
  (target prog6.out-const)
  (action
    (with-outputs-to %{target} (run mfg --ho --constant-analysis %{deps}))))
(rule
  (alias   runtest)
  (action (diff prog6.expected-const prog6.out-const)))

; Test rule for input file prog7.simpl
(rule
  (deps prog7.simpl)
  (target prog7.out-const)
  (action
    (with-outputs-to %{target} (run mfg --ho --constant-analysis %{deps}))))
(rule
  (alias   runtest)
  (action (diff prog7.expected-const prog7.out-const)))

; Test rule for input file prog8.simpl
(rule
  (deps prog8.simpl)
  (target prog8.out-const)
  (action
    (with-outputs-to %{target} (run mfg --ho --constant-analysis %{deps}))))
(rule
  (alias   runtest)
  (action (diff prog8.expected-const prog8.out-const)))

; Test rule for input file prog9.simpl
(rule
  (deps prog9.simpl)
  (target prog9.out-const)
  (action
    (with-outputs-to %{target} (run mfg --ho --constant-analysis %{deps}))))
(rule
  (alias   runtest)
  (action (diff prog9.expected-const prog9.out-const)))

; Test rule for input file heintze_mcallester.simpl
(rule
  (deps heintze_mcallester.simpl)
  (target heintze_mcallester.out-interval)
  (action
    (with-outputs-to %{target} (run mfg --ho --interval-analysis --delay-widening=2 %{deps}))))
(rule
  (alias   runtest)
  (action (diff heintze_mcallester.expected-interval heintze_mcallester.out-interval)))

; Test rule for input file iter_compose.simpl
(rule
  (deps iter_compose.simpl)
  (target iter_compose.out-interval)
  (action
    (with-outputs-to %{target} (run mfg --ho --interval-analysis --delay-widening=2 %{deps}))))
(rule
  (alias   runtest)
  (action (diff iter_compose.expected-interval iter_compose.out-interval)))

; Test rule for input file iter_compose_bool.simpl
(rule
  (deps iter_compose_bool.simpl)
  (target iter_compose_bool.out-interval)
  (action
    (with-outputs-to %{target} (run mfg --ho --interval-analysis --delay-widening=2 %{deps}))))
(rule
  (alias   runtest)
  (action (diff iter_compose_bool.expected-interval iter_compose_bool.out-interval)))

; Test rule for input file iter_compose_int.simpl
(rule
  (deps iter_compose_int.simpl)
  (target iter_compose_int.out-interval)
  (action
    (with-outputs-to %{target} (run mfg --ho --interval-analysis --delay-widening=2 %{deps}))))
(rule
  (alias   runtest)
  (action (diff iter_compose_int.expected-interval iter_compose_int.out-interval)))

; Test rule for input file prog1.simpl
(rule
  (deps prog1.simpl)
  (target prog1.out-interval)
  (action
    (with-outputs-to %{target} (run mfg --ho --interval-analysis --delay-widening=2 %{deps}))))
(rule
  (alias   runtest)
  (action (diff prog1.expected-interval prog1.out-interval)))

; Test rule for input file prog2.simpl
(rule
  (deps prog2.simpl)
  (target prog2.out-interval)
  (action
    (with-outputs-to %{target} (run mfg --ho --interval-analysis --delay-widening=2 %{deps}))))
(rule
  (alias   runtest)
  (action (diff prog2.expected-interval prog2.out-interval)))

; Test rule for input file prog3.simpl
(rule
  (deps prog3.simpl)
  (target prog3.out-interval)
  (action
    (with-outputs-to %{target} (run mfg --ho --interval-analysis --delay-widening=2 %{deps}))))
(rule
  (alias   runtest)
  (action (diff prog3.expected-interval prog3.out-interval)))

; Test rule for input file prog4.simpl
(rule
  (deps prog4.simpl)
  (target prog4.out-interval)
  (action
    (with-outputs-to %{target} (run mfg --ho --interval-analysis --delay-widening=2 %{deps}))))
(rule
  (alias   runtest)
  (action (diff prog4.expected-interval prog4.out-interval)))

; Test rule for input file prog5.simpl
(rule
  (deps prog5.simpl)
  (target prog5.out-interval)
  (action
    (with-outputs-to %{target} (run mfg --ho --interval-analysis --delay-widening=2 %{deps}))))
(rule
  (alias   runtest)
  (action (diff prog5.expected-interval prog5.out-interval)))

; Test rule for input file prog6.simpl
(rule
  (deps prog6.simpl)
  (target prog6.out-interval)
  (action
    (with-outputs-to %{target} (run mfg --ho --interval-analysis --delay-widening=2 %{deps}))))
(rule
  (alias   runtest)
  (action (diff prog6.expected-interval prog6.out-interval)))

; Test rule for input file prog7.simpl
(rule
  (deps prog7.simpl)
  (target prog7.out-interval)
  (action
    (with-outputs-to %{target} (run mfg --ho --interval-analysis --delay-widening=2 %{deps}))))
(rule
  (alias   runtest)
  (action (diff prog7.expected-interval prog7.out-interval)))

; Test rule for input file prog8.simpl
(rule
  (deps prog8.simpl)
  (target prog8.out-interval)
  (action
    (with-outputs-to %{target} (run mfg --ho --interval-analysis --delay-widening=2 %{deps}))))
(rule
  (alias   runtest)
  (action (diff prog8.expected-interval prog8.out-interval)))

; Test rule for input file prog9.simpl
(rule
  (deps prog9.simpl)
  (target prog9.out-interval)
  (action
    (with-outputs-to %{target} (run mfg --ho --interval-analysis --delay-widening=2 %{deps}))))
(rule
  (alias   runtest)
  (action (diff prog9.expected-interval prog9.out-interval)))

; Test rule for input file heintze_mcallester.simpl
(rule
  (deps heintze_mcallester.simpl)
  (target heintze_mcallester.out-concrete2)
  (action
    (with-outputs-to %{target} (run mfg --ho2 --concrete %{deps}))))
(rule
  (alias   runtest)
  (action (diff heintze_mcallester.expected-concrete2 heintze_mcallester.out-concrete2)))

; Test rule for input file iter_compose.simpl
(rule
  (deps iter_compose.simpl)
  (target iter_compose.out-concrete2)
  (action
    (with-outputs-to %{target} (run mfg --ho2 --concrete %{deps}))))
(rule
  (alias   runtest)
  (action (diff iter_compose.expected-concrete2 iter_compose.out-concrete2)))

; Test rule for input file iter_compose_bool.simpl
(rule
  (deps iter_compose_bool.simpl)
  (target iter_compose_bool.out-concrete2)
  (action
    (with-outputs-to %{target} (run mfg --ho2 --concrete %{deps}))))
(rule
  (alias   runtest)
  (action (diff iter_compose_bool.expected-concrete2 iter_compose_bool.out-concrete2)))

; Test rule for input file iter_compose_int.simpl
(rule
  (deps iter_compose_int.simpl)
  (target iter_compose_int.out-concrete2)
  (action
    (with-outputs-to %{target} (run mfg --ho2 --concrete %{deps}))))
(rule
  (alias   runtest)
  (action (diff iter_compose_int.expected-concrete2 iter_compose_int.out-concrete2)))

; Test rule for input file prog1.simpl
(rule
  (deps prog1.simpl)
  (target prog1.out-concrete2)
  (action
    (with-outputs-to %{target} (run mfg --ho2 --concrete %{deps}))))
(rule
  (alias   runtest)
  (action (diff prog1.expected-concrete2 prog1.out-concrete2)))

; Test rule for input file prog2.simpl
(rule
  (deps prog2.simpl)
  (target prog2.out-concrete2)
  (action
    (with-outputs-to %{target} (run mfg --ho2 --concrete %{deps}))))
(rule
  (alias   runtest)
  (action (diff prog2.expected-concrete2 prog2.out-concrete2)))

; Test rule for input file prog3.simpl
(rule
  (deps prog3.simpl)
  (target prog3.out-concrete2)
  (action
    (with-outputs-to %{target} (run mfg --ho2 --concrete %{deps}))))
(rule
  (alias   runtest)
  (action (diff prog3.expected-concrete2 prog3.out-concrete2)))

; Test rule for input file prog4.simpl
(rule
  (deps prog4.simpl)
  (target prog4.out-concrete2)
  (action
    (with-outputs-to %{target} (run mfg --ho2 --concrete %{deps}))))
(rule
  (alias   runtest)
  (action (diff prog4.expected-concrete2 prog4.out-concrete2)))

; Test rule for input file prog5.simpl
(rule
  (deps prog5.simpl)
  (target prog5.out-concrete2)
  (action
    (with-outputs-to %{target} (run mfg --ho2 --concrete %{deps}))))
(rule
  (alias   runtest)
  (action (diff prog5.expected-concrete2 prog5.out-concrete2)))

; Test rule for input file prog6.simpl
(rule
  (deps prog6.simpl)
  (target prog6.out-concrete2)
  (action
    (with-outputs-to %{target} (run mfg --ho2 --concrete %{deps}))))
(rule
  (alias   runtest)
  (action (diff prog6.expected-concrete2 prog6.out-concrete2)))

; Test rule for input file prog7.simpl
(rule
  (deps prog7.simpl)
  (target prog7.out-concrete2)
  (action
    (with-outputs-to %{target} (run mfg --ho2 --concrete %{deps}))))
(rule
  (alias   runtest)
  (action (diff prog7.expected-concrete2 prog7.out-concrete2)))

; Test rule for input file prog8.simpl
(rule
  (deps prog8.simpl)
  (target prog8.out-concrete2)
  (action
    (with-outputs-to %{target} (run mfg --ho2 --concrete %{deps}))))
(rule
  (alias   runtest)
  (action (diff prog8.expected-concrete2 prog8.out-concrete2)))

; Test rule for input file prog9.simpl
(rule
  (deps prog9.simpl)
  (target prog9.out-concrete2)
  (action
    (with-outputs-to %{target} (run mfg --ho2 --concrete %{deps}))))
(rule
  (alias   runtest)
  (action (diff prog9.expected-concrete2 prog9.out-concrete2)))

; Test rule for input file heintze_mcallester.simpl
(rule
  (deps heintze_mcallester.simpl)
  (target heintze_mcallester.out-type2)
  (action
    (with-outputs-to %{target} (run mfg --ho2 --type-analysis %{deps}))))
(rule
  (alias   runtest)
  (action (diff heintze_mcallester.expected-type2 heintze_mcallester.out-type2)))

; Test rule for input file iter_compose.simpl
(rule
  (deps iter_compose.simpl)
  (target iter_compose.out-type2)
  (action
    (with-outputs-to %{target} (run mfg --ho2 --type-analysis %{deps}))))
(rule
  (alias   runtest)
  (action (diff iter_compose.expected-type2 iter_compose.out-type2)))

; Test rule for input file iter_compose_bool.simpl
(rule
  (deps iter_compose_bool.simpl)
  (target iter_compose_bool.out-type2)
  (action
    (with-outputs-to %{target} (run mfg --ho2 --type-analysis %{deps}))))
(rule
  (alias   runtest)
  (action (diff iter_compose_bool.expected-type2 iter_compose_bool.out-type2)))

; Test rule for input file iter_compose_int.simpl
(rule
  (deps iter_compose_int.simpl)
  (target iter_compose_int.out-type2)
  (action
    (with-outputs-to %{target} (run mfg --ho2 --type-analysis %{deps}))))
(rule
  (alias   runtest)
  (action (diff iter_compose_int.expected-type2 iter_compose_int.out-type2)))

; Test rule for input file prog1.simpl
(rule
  (deps prog1.simpl)
  (target prog1.out-type2)
  (action
    (with-outputs-to %{target} (run mfg --ho2 --type-analysis %{deps}))))
(rule
  (alias   runtest)
  (action (diff prog1.expected-type2 prog1.out-type2)))

; Test rule for input file prog2.simpl
(rule
  (deps prog2.simpl)
  (target prog2.out-type2)
  (action
    (with-outputs-to %{target} (run mfg --ho2 --type-analysis %{deps}))))
(rule
  (alias   runtest)
  (action (diff prog2.expected-type2 prog2.out-type2)))

; Test rule for input file prog3.simpl
(rule
  (deps prog3.simpl)
  (target prog3.out-type2)
  (action
    (with-outputs-to %{target} (run mfg --ho2 --type-analysis %{deps}))))
(rule
  (alias   runtest)
  (action (diff prog3.expected-type2 prog3.out-type2)))

; Test rule for input file prog4.simpl
(rule
  (deps prog4.simpl)
  (target prog4.out-type2)
  (action
    (with-outputs-to %{target} (run mfg --ho2 --type-analysis %{deps}))))
(rule
  (alias   runtest)
  (action (diff prog4.expected-type2 prog4.out-type2)))

; Test rule for input file prog5.simpl
(rule
  (deps prog5.simpl)
  (target prog5.out-type2)
  (action
    (with-outputs-to %{target} (run mfg --ho2 --type-analysis %{deps}))))
(rule
  (alias   runtest)
  (action (diff prog5.expected-type2 prog5.out-type2)))

; Test rule for input file prog6.simpl
(rule
  (deps prog6.simpl)
  (target prog6.out-type2)
  (action
    (with-outputs-to %{target} (run mfg --ho2 --type-analysis %{deps}))))
(rule
  (alias   runtest)
  (action (diff prog6.expected-type2 prog6.out-type2)))

; Test rule for input file prog7.simpl
(rule
  (deps prog7.simpl)
  (target prog7.out-type2)
  (action
    (with-outputs-to %{target} (run mfg --ho2 --type-analysis %{deps}))))
(rule
  (alias   runtest)
  (action (diff prog7.expected-type2 prog7.out-type2)))

; Test rule for input file prog8.simpl
(rule
  (deps prog8.simpl)
  (target prog8.out-type2)
  (action
    (with-outputs-to %{target} (run mfg --ho2 --type-analysis %{deps}))))
(rule
  (alias   runtest)
  (action (diff prog8.expected-type2 prog8.out-type2)))

; Test rule for input file prog9.simpl
(rule
  (deps prog9.simpl)
  (target prog9.out-type2)
  (action
    (with-outputs-to %{target} (run mfg --ho2 --type-analysis %{deps}))))
(rule
  (alias   runtest)
  (action (diff prog9.expected-type2 prog9.out-type2)))

; Test rule for input file heintze_mcallester.simpl
(rule
  (deps heintze_mcallester.simpl)
  (target heintze_mcallester.out-const2)
  (action
    (with-outputs-to %{target} (run mfg --ho2 --constant-analysis %{deps}))))
(rule
  (alias   runtest)
  (action (diff heintze_mcallester.expected-const2 heintze_mcallester.out-const2)))

; Test rule for input file iter_compose.simpl
(rule
  (deps iter_compose.simpl)
  (target iter_compose.out-const2)
  (action
    (with-outputs-to %{target} (run mfg --ho2 --constant-analysis %{deps}))))
(rule
  (alias   runtest)
  (action (diff iter_compose.expected-const2 iter_compose.out-const2)))

; Test rule for input file iter_compose_bool.simpl
(rule
  (deps iter_compose_bool.simpl)
  (target iter_compose_bool.out-const2)
  (action
    (with-outputs-to %{target} (run mfg --ho2 --constant-analysis %{deps}))))
(rule
  (alias   runtest)
  (action (diff iter_compose_bool.expected-const2 iter_compose_bool.out-const2)))

; Test rule for input file iter_compose_int.simpl
(rule
  (deps iter_compose_int.simpl)
  (target iter_compose_int.out-const2)
  (action
    (with-outputs-to %{target} (run mfg --ho2 --constant-analysis %{deps}))))
(rule
  (alias   runtest)
  (action (diff iter_compose_int.expected-const2 iter_compose_int.out-const2)))

; Test rule for input file prog1.simpl
(rule
  (deps prog1.simpl)
  (target prog1.out-const2)
  (action
    (with-outputs-to %{target} (run mfg --ho2 --constant-analysis %{deps}))))
(rule
  (alias   runtest)
  (action (diff prog1.expected-const2 prog1.out-const2)))

; Test rule for input file prog2.simpl
(rule
  (deps prog2.simpl)
  (target prog2.out-const2)
  (action
    (with-outputs-to %{target} (run mfg --ho2 --constant-analysis %{deps}))))
(rule
  (alias   runtest)
  (action (diff prog2.expected-const2 prog2.out-const2)))

; Test rule for input file prog3.simpl
(rule
  (deps prog3.simpl)
  (target prog3.out-const2)
  (action
    (with-outputs-to %{target} (run mfg --ho2 --constant-analysis %{deps}))))
(rule
  (alias   runtest)
  (action (diff prog3.expected-const2 prog3.out-const2)))

; Test rule for input file prog4.simpl
(rule
  (deps prog4.simpl)
  (target prog4.out-const2)
  (action
    (with-outputs-to %{target} (run mfg --ho2 --constant-analysis %{deps}))))
(rule
  (alias   runtest)
  (action (diff prog4.expected-const2 prog4.out-const2)))

; Test rule for input file prog5.simpl
(rule
  (deps prog5.simpl)
  (target prog5.out-const2)
  (action
    (with-outputs-to %{target} (run mfg --ho2 --constant-analysis %{deps}))))
(rule
  (alias   runtest)
  (action (diff prog5.expected-const2 prog5.out-const2)))

; Test rule for input file prog6.simpl
(rule
  (deps prog6.simpl)
  (target prog6.out-const2)
  (action
    (with-outputs-to %{target} (run mfg --ho2 --constant-analysis %{deps}))))
(rule
  (alias   runtest)
  (action (diff prog6.expected-const2 prog6.out-const2)))

; Test rule for input file prog7.simpl
(rule
  (deps prog7.simpl)
  (target prog7.out-const2)
  (action
    (with-outputs-to %{target} (run mfg --ho2 --constant-analysis %{deps}))))
(rule
  (alias   runtest)
  (action (diff prog7.expected-const2 prog7.out-const2)))

; Test rule for input file prog8.simpl
(rule
  (deps prog8.simpl)
  (target prog8.out-const2)
  (action
    (with-outputs-to %{target} (run mfg --ho2 --constant-analysis %{deps}))))
(rule
  (alias   runtest)
  (action (diff prog8.expected-const2 prog8.out-const2)))

; Test rule for input file prog9.simpl
(rule
  (deps prog9.simpl)
  (target prog9.out-const2)
  (action
    (with-outputs-to %{target} (run mfg --ho2 --constant-analysis %{deps}))))
(rule
  (alias   runtest)
  (action (diff prog9.expected-const2 prog9.out-const2)))

; Test rule for input file heintze_mcallester.simpl
(rule
  (deps heintze_mcallester.simpl)
  (target heintze_mcallester.out-interval2)
  (action
    (with-outputs-to %{target} (run mfg --ho2 --interval-analysis --delay-widening=2 %{deps}))))
(rule
  (alias   runtest)
  (action (diff heintze_mcallester.expected-interval2 heintze_mcallester.out-interval2)))

; Test rule for input file iter_compose.simpl
(rule
  (deps iter_compose.simpl)
  (target iter_compose.out-interval2)
  (action
    (with-outputs-to %{target} (run mfg --ho2 --interval-analysis --delay-widening=2 %{deps}))))
(rule
  (alias   runtest)
  (action (diff iter_compose.expected-interval2 iter_compose.out-interval2)))

; Test rule for input file iter_compose_bool.simpl
(rule
  (deps iter_compose_bool.simpl)
  (target iter_compose_bool.out-interval2)
  (action
    (with-outputs-to %{target} (run mfg --ho2 --interval-analysis --delay-widening=2 %{deps}))))
(rule
  (alias   runtest)
  (action (diff iter_compose_bool.expected-interval2 iter_compose_bool.out-interval2)))

; Test rule for input file iter_compose_int.simpl
(rule
  (deps iter_compose_int.simpl)
  (target iter_compose_int.out-interval2)
  (action
    (with-outputs-to %{target} (run mfg --ho2 --interval-analysis --delay-widening=2 %{deps}))))
(rule
  (alias   runtest)
  (action (diff iter_compose_int.expected-interval2 iter_compose_int.out-interval2)))

; Test rule for input file prog1.simpl
(rule
  (deps prog1.simpl)
  (target prog1.out-interval2)
  (action
    (with-outputs-to %{target} (run mfg --ho2 --interval-analysis --delay-widening=2 %{deps}))))
(rule
  (alias   runtest)
  (action (diff prog1.expected-interval2 prog1.out-interval2)))

; Test rule for input file prog2.simpl
(rule
  (deps prog2.simpl)
  (target prog2.out-interval2)
  (action
    (with-outputs-to %{target} (run mfg --ho2 --interval-analysis --delay-widening=2 %{deps}))))
(rule
  (alias   runtest)
  (action (diff prog2.expected-interval2 prog2.out-interval2)))

; Test rule for input file prog3.simpl
(rule
  (deps prog3.simpl)
  (target prog3.out-interval2)
  (action
    (with-outputs-to %{target} (run mfg --ho2 --interval-analysis --delay-widening=2 %{deps}))))
(rule
  (alias   runtest)
  (action (diff prog3.expected-interval2 prog3.out-interval2)))

; Test rule for input file prog4.simpl
(rule
  (deps prog4.simpl)
  (target prog4.out-interval2)
  (action
    (with-outputs-to %{target} (run mfg --ho2 --interval-analysis --delay-widening=2 %{deps}))))
(rule
  (alias   runtest)
  (action (diff prog4.expected-interval2 prog4.out-interval2)))

; Test rule for input file prog5.simpl
(rule
  (deps prog5.simpl)
  (target prog5.out-interval2)
  (action
    (with-outputs-to %{target} (run mfg --ho2 --interval-analysis --delay-widening=2 %{deps}))))
(rule
  (alias   runtest)
  (action (diff prog5.expected-interval2 prog5.out-interval2)))

; Test rule for input file prog6.simpl
(rule
  (deps prog6.simpl)
  (target prog6.out-interval2)
  (action
    (with-outputs-to %{target} (run mfg --ho2 --interval-analysis --delay-widening=2 %{deps}))))
(rule
  (alias   runtest)
  (action (diff prog6.expected-interval2 prog6.out-interval2)))

; Test rule for input file prog7.simpl
(rule
  (deps prog7.simpl)
  (target prog7.out-interval2)
  (action
    (with-outputs-to %{target} (run mfg --ho2 --interval-analysis --delay-widening=2 %{deps}))))
(rule
  (alias   runtest)
  (action (diff prog7.expected-interval2 prog7.out-interval2)))

; Test rule for input file prog8.simpl
(rule
  (deps prog8.simpl)
  (target prog8.out-interval2)
  (action
    (with-outputs-to %{target} (run mfg --ho2 --interval-analysis --delay-widening=2 %{deps}))))
(rule
  (alias   runtest)
  (action (diff prog8.expected-interval2 prog8.out-interval2)))

; Test rule for input file prog9.simpl
(rule
  (deps prog9.simpl)
  (target prog9.out-interval2)
  (action
    (with-outputs-to %{target} (run mfg --ho2 --interval-analysis --delay-widening=2 %{deps}))))
(rule
  (alias   runtest)
  (action (diff prog9.expected-interval2 prog9.out-interval2)))

