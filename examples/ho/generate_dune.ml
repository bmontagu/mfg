(*****************************************************************************)

(* configuration for analyzer "HO concrete" *)
module ConfigHOConcrete = struct
  (* extension of the input files *)
  let input_ext = ".simpl"

  (* extension for the output files *)
  let output_ext = ".out-concrete"

  (* extention for the expected output files *)
  let expected_ext = ".expected-concrete"

  (* name of the executable to run for each test *)
  let exe = "mfg"

  (* flags that should be given by default to the executable *)
  let default_flags = [ "--ho"; "--concrete" ]

  (* flags that should be given to specific files *)
  let file_specific_flags = []

  (* files in the following list are excluded *)
  let excludes = []
end

(* generate the rules for the files in the current directory and
   prints them to standard output *)
let () =
  let module M = DuneTestGen.Make (ConfigHOConcrete) in
  M.generate_rules stdout Filename.current_dir_name

(*****************************************************************************)

(* configuration for analyzer "HO type-analysis" *)
module ConfigHOType = struct
  (* extension of the input files *)
  let input_ext = ".simpl"

  (* extension for the output files *)
  let output_ext = ".out-type"

  (* extention for the expected output files *)
  let expected_ext = ".expected-type"

  (* name of the executable to run for each test *)
  let exe = "mfg"

  (* flags that should be given by default to the executable *)
  let default_flags = [ "--ho"; "--type-analysis" ]

  (* flags that should be given to specific files *)
  let file_specific_flags = []

  (* files in the following list are excluded *)
  let excludes = []
end

(* generate the rules for the files in the current directory and
   prints them to standard output *)
let () =
  let module M = DuneTestGen.Make (ConfigHOType) in
  M.generate_rules stdout Filename.current_dir_name

(*****************************************************************************)

(* configuration for analyzer "HO constant-analysis" *)
module ConfigHOConst = struct
  (* extension of the input files *)
  let input_ext = ".simpl"

  (* extension for the output files *)
  let output_ext = ".out-const"

  (* extention for the expected output files *)
  let expected_ext = ".expected-const"

  (* name of the executable to run for each test *)
  let exe = "mfg"

  (* flags that should be given by default to the executable *)
  let default_flags = [ "--ho"; "--constant-analysis" ]

  (* flags that should be given to specific files *)
  let file_specific_flags = []

  (* files in the following list are excluded *)
  let excludes = []
end

(* generate the rules for the files in the current directory and
   prints them to standard output *)
let () =
  let module M = DuneTestGen.Make (ConfigHOConst) in
  M.generate_rules stdout Filename.current_dir_name

(*****************************************************************************)

(* configuration for analyzer "HO interval-analysis" *)
module ConfigHOInterval = struct
  (* extension of the input files *)
  let input_ext = ".simpl"

  (* extension for the output files *)
  let output_ext = ".out-interval"

  (* extention for the expected output files *)
  let expected_ext = ".expected-interval"

  (* name of the executable to run for each test *)
  let exe = "mfg"

  (* flags that should be given by default to the executable *)
  let default_flags = [ "--ho"; "--interval-analysis"; "--delay-widening=2" ]

  (* flags that should be given to specific files *)
  let file_specific_flags = []

  (* files in the following list are excluded *)
  let excludes = []
end

(* generate the rules for the files in the current directory and
   prints them to standard output *)
let () =
  let module M = DuneTestGen.Make (ConfigHOInterval) in
  M.generate_rules stdout Filename.current_dir_name

(*****************************************************************************)

(* configuration for analyzer "HO2 concrete" *)
module ConfigHO2Concrete = struct
  (* extension of the input files *)
  let input_ext = ".simpl"

  (* extension for the output files *)
  let output_ext = ".out-concrete2"

  (* extention for the expected output files *)
  let expected_ext = ".expected-concrete2"

  (* name of the executable to run for each test *)
  let exe = "mfg"

  (* flags that should be given by default to the executable *)
  let default_flags = [ "--ho2"; "--concrete" ]

  (* flags that should be given to specific files *)
  let file_specific_flags = []

  (* files in the following list are excluded *)
  let excludes = []
end

(* generate the rules for the files in the current directory and
   prints them to standard output *)
let () =
  let module M = DuneTestGen.Make (ConfigHO2Concrete) in
  M.generate_rules stdout Filename.current_dir_name

(*****************************************************************************)

(* configuration for analyzer "HO2 type-analysis" *)
module ConfigHO2Type = struct
  (* extension of the input files *)
  let input_ext = ".simpl"

  (* extension for the output files *)
  let output_ext = ".out-type2"

  (* extention for the expected output files *)
  let expected_ext = ".expected-type2"

  (* name of the executable to run for each test *)
  let exe = "mfg"

  (* flags that should be given by default to the executable *)
  let default_flags = [ "--ho2"; "--type-analysis" ]

  (* flags that should be given to specific files *)
  let file_specific_flags = []

  (* files in the following list are excluded *)
  let excludes = []
end

(* generate the rules for the files in the current directory and
   prints them to standard output *)
let () =
  let module M = DuneTestGen.Make (ConfigHO2Type) in
  M.generate_rules stdout Filename.current_dir_name

(*****************************************************************************)

(* configuration for analyzer "HO2 constant-analysis" *)
module ConfigHO2Const = struct
  (* extension of the input files *)
  let input_ext = ".simpl"

  (* extension for the output files *)
  let output_ext = ".out-const2"

  (* extention for the expected output files *)
  let expected_ext = ".expected-const2"

  (* name of the executable to run for each test *)
  let exe = "mfg"

  (* flags that should be given by default to the executable *)
  let default_flags = [ "--ho2"; "--constant-analysis" ]

  (* flags that should be given to specific files *)
  let file_specific_flags = []

  (* files in the following list are excluded *)
  let excludes = []
end

(* generate the rules for the files in the current directory and
   prints them to standard output *)
let () =
  let module M = DuneTestGen.Make (ConfigHO2Const) in
  M.generate_rules stdout Filename.current_dir_name

(*****************************************************************************)

(* configuration for analyzer "HO2 interval-analysis" *)
module ConfigHO2Interval = struct
  (* extension of the input files *)
  let input_ext = ".simpl"

  (* extension for the output files *)
  let output_ext = ".out-interval2"

  (* extention for the expected output files *)
  let expected_ext = ".expected-interval2"

  (* name of the executable to run for each test *)
  let exe = "mfg"

  (* flags that should be given by default to the executable *)
  let default_flags = [ "--ho2"; "--interval-analysis"; "--delay-widening=2" ]

  (* flags that should be given to specific files *)
  let file_specific_flags = []

  (* files in the following list are excluded *)
  let excludes = []
end

(* generate the rules for the files in the current directory and
   prints them to standard output *)
let () =
  let module M = DuneTestGen.Make (ConfigHO2Interval) in
  M.generate_rules stdout Filename.current_dir_name

(*****************************************************************************)
